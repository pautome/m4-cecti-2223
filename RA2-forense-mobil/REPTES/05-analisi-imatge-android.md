# Repte: Anàlisi d'imatge Android  

## Temps
5 hores ?

## Material i programari

- Imatge Android per analitzar - https://drive.google.com/drive/folders/14gL_V0GlPym-VbbGwIDpgDNK33DIegnO?usp=sharing
- Chrome o browser web compatible amb XML
- DB Browser for SQLite https://sqlitebrowser.org/dl/
- Autopsy

---

## Objectiu

Utilitzar les eines d'anàlisi per a trobar evidències a una imatge d'Android.

---

## Repte

1. Baixa i descomprimeix la imatge del mòbil Android.
2. Analitza la imatge per a donar resposta a les preguntes de revisió.
3. Comprova les respostes a https://ctf.iescarlesvallbona.cat/challenges?category=andoid-image

---

## Preguntes de revisió

Redacteu un informe sobre com s'han trobat les evidències per a resoldre les preguntes.

1. Quina era l'hora del dispositiu en UTC al moment de l'adquisició? (hh:mm:ss)
2. Quina és l'adreça de correu electrònic de Zoe Washburne?
3. A quina hora en UTC es va baixar el navegador TOR? (hh:mm:ss)
4. A quina hora es va fer la càrrega del telèfon al 100% després de l'últim reinici? (hh:mm:ss)
5. Quina és la contrasenya del punt d'accés WIFI més recentment connectat?
6. En quina aplicació es va centrar l'usuari a les 14:13:27?
7. Quant de temps va estar mirant Youtube el sospitós el 2021-05-20? (hh:mm:ss)

8. Indica les hores reals dedicades al repte.
