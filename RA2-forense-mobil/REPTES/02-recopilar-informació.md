# Repte: Recopilar informació sobre els mòbils de pràctiques

## Temps
3 hores ?

## Material i programari

Els dispositius a analitzar són dispositius de gamma baixa i preu reduït:
- XIAOMI REDMI 9A

---

## Objectiu

Recopilar informació sobre els mòbils de que disposem (i opcionalment del propi mòbil particular Android).
Obtenir eines bàsiques per a començar l'anàlisi del dispositiu posteriorment.

---

## Repte

1. Busqueu informació tècnica del dispositiu a la web del fabricant.

2. Busqueu informació sobre la versió d'Android que porta instal·lada (funcionalitats, possibles vulnerabilitats, etc.). Podeu fer servir CVE Details - https://www.cvedetails.com/

2. Cerqueu i descarregueu la imatge oficial del sistema del dispositiu. Això es farà per tenir una possible còpia de restauració.

3. Descriviu el procés a seguir per a extreure la imatge del sistema operatiu del mòbil a un dispositiu extern, si és possible. En el cas de no haver trobat la imatge oficial, pot servir per restaurar el mòbil. Així tindrem una còpia pel cas que es bloqui o no arrenqui després d'alguna operació fallida.

4. Descriviu el procés a seguir per a actualitzar la imatge del sistema operatiu oficial. Si cal alguna aplicació del fabricant, busqueu on obtenir-la i descarregueu-la.

5. Descriviu el procés a seguir per a aconseguir root al dispositiu.

6. Descriviu el procés per a poder connectar al dispositiu i depurar remotament amb adb.

---

## Preguntes de revisió

1. Redacteu un informe amb la informació obtinguda.
2. Indica les hores reals dedicades al repte.
