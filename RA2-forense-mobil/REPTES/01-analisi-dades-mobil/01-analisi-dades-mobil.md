# Repte: Anàlisi de fitxers d'Android

## Temps
1 hora ?

## Material i programari

- Chrome o browser web compatible amb XML
- DB Browser for SQLite https://sqlitebrowser.org/dl/

- https://www.tutorialspoint.com/android/android_sqlite_database.htm

---

## Objectiu

Moltes aplicacions mòbils guarden la informació en arxius "plans" o bases de dades SQLite. Les apps de dispositius no "rootejats" usen sandboxes i tancades al seu propi directori, fent fàcil exportar i analitzar les dades. Recuperant i analitzant aquests fitxers podem identificar comunicacions i transaccions entre aplicacions.

Tenim 3 directoris/fitxers que hem d'examinar:
    1. Packages.xml
    2. AndroidManifest.xml
    3. Fitxers d'Apps (.apk)

---

## Repte

### 1 - Examinar Packages.xml d'un telèfon Android

El fitxer s'ubica a /Root/system/packages.xml. Conté una llista d'aplicacions instal·lades al dispositiu i els seus permisos associats.

Ex: la Calculadora pot tenir permisos per llegir l'emmagatzematge extern, però no per la càmera o el GPS.


1. Usa Chrome o un altre browser per veure el fitxer packages.xml.
2. Busca:
   - Permisos associats a com.roidapp.photogrid
   - Quins permisos hauria de tenir una App si accedeix a com.google.android.calendar.uid.shared (ID usuari 10055)?
   - Que es pot dir dels permisos de com.surpax.ledflashlight.panel?

---

### 2 - Examinar una base de dades SQLite

1. Busca la definició de SQLite (http://sqlite.com/about.html) i explica les seves característiques.

2. Obre la BD master.db

3. Desplega les taules que conté.

4. A la taula “FileTransfers”:
  - Quants fitxers s'han enviat?
  - Quins són els noms dels fitxers?
  - Quin és l'ID de l'usuari a qui s'han enviat?

5. A la taula “Profile”:
  - Quin és el PIN d'aquest usuari?

6. A la taula “Users”:
  - Quin és el nom de l'usuari amb ID 10?

7. Quina taula s'utilitza per a veure els missatges de text? Quan es va enviar el missatge “Round 3 reply”?

---

8. Indiqueu el temps REAL que heu dedicat a fer el repte.
