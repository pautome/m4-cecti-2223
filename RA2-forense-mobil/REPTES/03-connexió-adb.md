# Repte: Connexió a mòbil amb ADB i Android Studio

## Temps
3 hores ?

## Material i programari

- Els dispositius a analitzar són dispositius de gamma baixa i preu reduït - XIAOMI REDMI 9A

- Manual SQLITE - https://www.sqlite.org/cli.html

- Usar ADB (connexió i comandes) - https://developer.android.com/studio/command-line/adb

- Android Studio executar apps en emulador i disp. físic - https://developer.android.com/studio/run/device

- Còpia de seguretat amb ADB - https://www.androidjefe.com/respaldo-android-desde-pc/

- A Script to... https://github.com/newbit1/rootAVD
  - **root your Android Studio Virtual Device (AVD)**, with Magisk (Stable, Canary or Alpha)
  - patch its fstab
  - download and install the USB HOST Permissions Module for Magisk
  - install custom build Kernel and its Modules
  - download and install AOSP prebuilt Kernel and its Modules

---

## Objectiu

Aplicar la informació recopilada per a connectar al dispositiu mòbil i obtenir una còpia de les dades contingudes al dispositiu físic o en el seu defecte, al dispositiu simulat amb Android Studio.

---

## Repte

1. Prepara l'entorn per a poder connectar via ADB al dispositiu físic i al simulador.
- Cal instal·lar l'Android Software Development Kit (SDK). Aquest conté l'IDE de desenvolupament d'apps Android, documentació, simuladors d'Android i altres eines útils per a la investigació. Si tenim prou espai farem servir aquesta opció (a Windows o millor a Linux, ja que Android fa servir Linux). https://developer.android.com/studio/index.html
- Alternativament es pot instal·lar només les "Platform tools", que formen part del SDK per ocupar menys espai. Inclouen per exemple ADB. https:/​/developer.android.com/studio/releases/platform-​tools.

2. Crea un dispositiu virtual que faci servir la mateixa versió d'Android dels nostres dispositius físics.

3. Obre una consola shell amb ADB al dispositiu físic (si en disposes) i una altra al simulador. La ruta a les utilitats és: /home/user/Android/Sdk/platform-tools/adb

4. Navega pels directoris i recull tota la informació del dispositiu que puguis, tant des de la consola com l'IDE d'Android SDK.
- Arbre de fitxers.
- Fitxers de la memòria SD.
- Dades del sistema.
- Sistemes d'arxius muntats i disponibles.
- Informació de processos.
- Informació d'APK's instal·lades.
- Captures de pantalla.
- Informació dels serveis.
- Número IMEI i informació relacionada.
- etc.

5. Navega per les carpetes dels dispositius virtual i físic des de l'entorn del SDK d'Android i extreu algun arxiu amb la interfície GUI.

6. OPCIONAL: Prova a realitzar una extracció de dades amb Magnet Axiom https://www.magnetforensics.com/.

7. OPCIONAL: Feu el procés de Root per al dispositiu virtual a l'Android Studio.

---

## Preguntes de revisió

1. Redacteu un informe amb la informació obtinguda.
2. Indica les hores reals dedicades al repte.
