# Repte: Triage Android amb android_triage i Tsurugi  

## Temps
1 hora

## Material i programari

- Els dispositius a analitzar són dispositius de gamma baixa i preu reduït - XIAOMI REDMI 9A

- Tsurugi Linux - Distro forense amb la utilitat android_triage
- Android Triage - https://github.com/RealityNet/android_triage
  - Android Triage explicat - https://blog.digital-forensics.it/2021/03/triaging-modern-android-devices-aka.html
  - Vídeo explicatiu de l'aplicació: Android logical acquisition and triage with android_triage - https://www.youtube.com/watch?v=jRRH2YWSnhE&t=6s

Si es fa des de windows amb un bash cal afegir al path la ruta al directori de adb (canviar amb la ruta correcta):
~~~
export PATH=$PATH:"/C/Program Files (x86)/apache-maven-3.3.3/bin"
~~~

---

## Objectiu

Utilitzar una eina automatitzada per a fer un triatge amb adquisició lògica d'un dispositiu Android.
Aquesta aplicació ens serveix de guia per a fer l'adquisició mitjançant adb.

---

## Repte

1. Arrenca Tsurugi Lab per a poder usar l'eina android_triage o bé instal·la des del repositori l'script amb les seves dependències.
2. Connecta el telèfon físic i fes servir un dispositiu emulat amb Android SDK. En el dispositiu emulat, cal poder ser Root. Es pot fer amb una imatge Android que no tingui el Google Play. Busca entre les imatges disponibles una que no tingui el logo de Google Play. Aquesta imatge porta la comanda su que permet iniciar Android amb root.
3. Observa que el menú de l'aplicació segueix l'ordre de volatilitat. Executa la seqüència del menú i observa quines dades ha adquirit.
4. Determina quines comandes adb fa servir l'script per a realitzar cada una de les operacions d'adquisició i quina utilitat te l'operació.
5. Compara la informació que es descarrega en mode Root i en mode normal.

---

## Preguntes de revisió

1. Redacteu un informe sobre la utilitat.
2. Indica les hores reals dedicades al repte.
