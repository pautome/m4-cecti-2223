2. Realitza anàlisis forenses en dispositius mòbils, aplicant metodologies establertes, actualitzades i reconegudes.

- Criteris d'avaluació:
  1. Realitza el procés de presa de evidències en un dispositiu mòbil.
  2. Extreu, descodifica i analitza les proves conservant la cadena de custòdia.
  3. Genera informes de dades mòbils, complint amb els requisits de la indústria forense de telefonia mòbil.
  4. Presenta i exposa les conclusions de l'anàlisi forense realitzat a qui procedeixi.

  **Continguts**

  2. Realització d'anàlisis forenses en dispositius mòbils:
      1. Mètodes per a l'extracció d'evidències.
      2. Eines de mercat més comuns.
