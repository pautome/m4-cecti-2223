# Jerarquia d'arxius a Android

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Jerarquia d'arxius a Android](#jerarquia-darxius-a-android)
	- [Directoris a l'arrel](#directoris-a-larrel)
		- [/boot:](#boot)
		- [/system](#system)
		- [*/recovery](#recovery)
		- [/data](#data)
		- [*/cache](#cache)
		- [*/misc](#misc)
		- [/sdcard](#sdcard)
	- [Sistemes de fitxers a Android](#sistemes-de-fitxers-a-android)
		- [Memòries flash](#memries-flash)
			- [Extended File Allocation Table (ExFAT)](#extended-file-allocation-table-exfat)
			- [Flash Friendly File System (F2FS)](#flash-friendly-file-system-f2fs)
			- [Yet Another Flash File System 2 (YAFFS2)](#yet-another-flash-file-system-2-yaffs2)
			- [Robust File System (RFS)](#robust-file-system-rfs)
		- [Basats en el mitjà](#basats-en-el-mitj)
			- [Extended File System (EXT)](#extended-file-system-ext)
			- [Virtual File Allocation Table (VFAT)](#virtual-file-allocation-table-vfat)
		- [Pseudo sistemes d'arxius](#pseudo-sistemes-darxius)
			- [Sistema de fitxers arrel (rootfs)](#sistema-de-fitxers-arrel-rootfs)
			- [Sistema de fitxers sysfs](#sistema-de-fitxers-sysfs)
			- [Sistema de fitxers devpts](#sistema-de-fitxers-devpts)
			- [Sistema de fitxers del cgroup](#sistema-de-fitxers-del-cgroup)
			- [Sistema de fitxers proc](#sistema-de-fitxers-proc)
			- [Sistema de fitxers tmpfs](#sistema-de-fitxers-tmpfs)

<!-- /TOC -->

---

## Directoris a l'arrel

Com que Android utilitza Linux com a base, la jerarquia de directoris és molt semblant.
- Cada fabricant pot introduir canvis poc importants a aquesta jerarquia de directoris.
- Alguns directoris només els pot accedir l'usuari root i per tant es necessitarà fer root al dispositiu per a poder accedir a aquests arxius.

![](imgs/03-jerarquia-directoris-3f1a9f47.png)

L'estructura de directoris es pot veure amb la comanda "ls", tot i que en un dispositiu on no s'és root no es pot accedir a molts directoris:

~~~
U3A_PLUS_4G:/ $ ls -la
ls: ./ueventd.rc: Permission denied
ls: ./tctpersist: Permission denied
ls: ./init.zygote32.rc: Permission denied
ls: ./init.usb.rc: Permission denied
ls: ./init.usb.configfs.rc: Permission denied
ls: ./init.rc: Permission denied
ls: ./init.environ.rc: Permission denied
ls: ./init: Permission denied
ls: ./fstab.enableswap: Permission denied
ls: ./boot.ver: Permission denied
total 24
drwxr-xr-x  19 root   root      0 2022-03-01 10:47 .
drwxr-xr-x  19 root   root      0 2022-03-01 10:47 ..
dr-xr-xr-x   3 root   root      0 2022-03-01 10:47 acct
lrwxrwxrwx   1 root   root     50 1970-01-01 01:00 bugreports -> /data/user_de/0/com.android.shell/files/bugreports
drwxrwx---   6 system cache  4096 2018-01-01 01:03 cache
lrwxrwxrwx   1 root   root     13 1970-01-01 01:00 charger -> /sbin/charger
drwxr-xr-x   4 root   root      0 1970-01-01 01:00 config
drwxr-xr-x   2 root   root      0 2022-03-01 10:47 custom
lrwxrwxrwx   1 root   root     17 1970-01-01 01:00 d -> /sys/kernel/debug
drwxrwx--x  56 system system 4096 2022-03-01 10:48 data
lrwxrwxrwx   1 root   root     23 1970-01-01 01:00 default.prop -> system/etc/prop.default
drwxr-xr-x  18 root   root   4020 2022-03-01 10:47 dev
lrwxrwxrwx   1 root   root     11 1970-01-01 01:00 etc -> /system/etc
drwxr-xr-x   3 root   root      0 1970-01-01 01:00 lib
drwxr-xr-x  11 root   system  240 2022-03-01 10:47 mnt
drwxr-xr-x   2 root   root      0 1970-01-01 01:00 oem
dr-xr-xr-x 419 root   root      0 1970-01-01 01:00 proc
drwx------   2 root   root      0 2021-07-30 14:19 root
drwxr-x---   2 root   root      0 1970-01-01 01:00 sbin
lrwxrwxrwx   1 root   root     21 1970-01-01 01:00 sdcard -> /storage/self/primary
drwxr-xr-x   4 root   root    100 2022-03-01 11:20 storage
dr-xr-xr-x  16 root   root      0 2022-03-01 10:47 sys
drwxr-xr-x  17 root   root   4096 1970-01-01 01:00 system
drwxr-xr-x  20 root   root   4096 1970-01-01 01:00 vendor

~~~

La jeraquia de directoris sol incloure:

### /boot:

Informació i fitxers requerits per l'arrencada del telèfon. Conté el kernel i el disc RAM. Sense aquesta partició, el telèfon no pot iniciar els seus processos. Les dades que resideixen a la RAM són importants i han de ser capturades durant l'adquisició forense.

### /system

Aquesta partició conté altres fitxers relacionats amb el sistema. Mai no s'ha d'esborrar per que llavors no es podria iniciar el telèfon.

~~~
U3A_PLUS_4G:/system $ ls -la
total 1412
drwxr-xr-x 17 root root     4096 1970-01-01 01:00 .
drwxr-xr-x 19 root root        0 2022-03-01 10:47 ..
drwxr-xr-x 52 root root     4096 2021-08-25 10:21 app
drwxr-xr-x  3 root shell    8192 2008-12-31 17:00 bin
-rw-------  1 root root    10103 2021-08-25 10:23 build.prop
-rw-r--r--  1 root root    74612 2008-12-31 17:00 compatibility_matrix.xml
drwxr-xr-x  3 root root     4096 2021-08-25 10:21 custpack
drwxr-xr-x 15 root root     4096 2021-08-25 10:11 etc
drwxr-xr-x  2 root root     4096 2008-12-31 17:00 fake-libs
drwxr-xr-x  2 root root     8192 2008-12-31 17:00 fonts
drwxr-xr-x  6 root root     4096 2008-12-31 17:00 framework
drwxr-xr-x  6 root root    16384 2008-12-31 17:00 lib
drwx------  2 root root     4096 1970-01-01 01:00 lost+found
-rw-r--r--  1 root root     2946 2008-12-31 17:00 manifest.xml
drwxr-xr-x  3 root root     4096 2021-08-25 10:11 media
drwxr-xr-x  3 root root     4096 2021-08-25 10:23 overlay
drwxr-xr-x 53 root root     4096 2021-08-25 10:11 priv-app
-rw-r--r--  1 root root  1259103 2008-12-31 17:00 recovery-from-boot.p
-rw-r--r--  1 root root       12 2021-08-25 10:21 system.ver
drwxr-xr-x  2 root root     4096 2021-08-25 10:11 usermanual
drwxr-xr-x  8 root root     4096 2008-12-31 17:00 usr
lrw-r--r--  1 root root        7 2008-12-31 17:00 vendor -> /vendor
drwxr-xr-x  2 root shell    4096 2008-12-31 17:00 xbin
~~~

### */recovery

Dissenyat per a fer backups permet iniciar el dispositiu en mode recuperació (recovery mode). En aquest mode hi trobem eines per a reparar la instal·lació del telèfon.

~~~
U3A_PLUS_4G:/ $ find . -name "recovery" 2> /dev/null                                      
./dev/block/platform/bootdevice/by-name/recovery
~~~

### /data

Conté les dades de cada aplicació. Conté moltes dades de l'usuari, com ara contactes, SMS, números marcats, etc. Aquesta carpeta és molt important per al forense, però no s'hi pot accedir si no s'és root.

### */cache

Emmagatzema dades accedides freqüentment i alguns logs que necessiten ser accedits ràpidament. Aquesta carpeta també és molt important pel forense ja que pot contenir dades que ja no existeixen a la carpeta /data.

~~~
U3A_PLUS_4G:/ $ find / -name "cache" 2> /dev/null                                           
/vendor/cache
/cache
/dev/block/platform/bootdevice/by-name/cache
~~~

### */misc

Conté informació sobre configuracions miscelànies. En general defineixen l'estat del dispositiu on/off, informació sobre configuració del hardware, configuració USB, etc.

Exemple de carpetes misc
~~~
U3A_PLUS_4G:/ $ find . -name "misc" 2> /dev/null                                            
./vendor/data/misc
./sys/devices/virtual/misc
./sys/devices/platform/10010000.kp/misc
./sys/class/misc
~~~

### /sdcard

Aquesta és la partició que conté la informació de la memòria Secure Digital (SD). Pot contenir informacions diverses com ara fotografies, vídeos, fitxers, documents...

~~~
U3A_PLUS_4G:/ $ ls -la sdcard/                                                              
total 39
drwxrwx--x 13 root sdcard_rw 3488 2022-03-01 11:01 .
drwx--x--x  4 root sdcard_rw 3488 2018-01-01 01:07 ..
drwxrwx--x  2 root sdcard_rw 3488 2018-01-01 01:07 Alarms
drwxrwx--x  3 root sdcard_rw 3488 2018-01-01 01:07 Android
drwxrwx--x  4 root sdcard_rw 3488 2022-03-01 12:03 DCIM
drwxrwx--x  2 root sdcard_rw 3488 2018-01-01 01:07 Download
drwxrwx--x  2 root sdcard_rw 3488 2018-01-01 01:07 Movies
drwxrwx--x  2 root sdcard_rw 3488 2018-01-01 01:07 Music
drwxrwx--x  2 root sdcard_rw 3488 2018-01-01 01:07 Notifications
drwxrwx--x  2 root sdcard_rw 3488 2018-01-01 01:07 Pictures
drwxrwx--x  2 root sdcard_rw 3488 2018-01-01 01:07 Podcasts
drwxrwx--x  2 root sdcard_rw 3488 2018-01-01 01:07 Ringtones
drwxrwx--x  3 root sdcard_rw 3488 2022-03-01 11:01 mtklog
~~~

---

## Sistemes de fitxers a Android

Es pot comprovar quins sistemes d'arxius suporta el kernel d'Android comprovant "cat /proc/filesystems". Si apareix la paraula "nodev" és que no hi ha un sistema d'arxius d'aquest tipus muntat.

~~~
U3A_PLUS_4G:/ $ cat /proc/filesystems                                                         
nodev	sysfs
nodev	rootfs
nodev	ramfs
nodev	bdev
nodev	proc
nodev	cpuset
nodev	cgroup
nodev	tmpfs
nodev	configfs
nodev	debugfs
nodev	tracefs
nodev	sockfs
nodev	pipefs
nodev	devpts
	ext3
	ext2
	ext4
	vfat
	msdos
	iso9660
nodev	sdcardfs
	fuseblk
nodev	fuse
nodev	fusectl
	f2fs
nodev	pstore
nodev	selinuxfs
nodev	functionfs
~~~

Si volem veure els dispositius muntats ho podrem fer amb la comanda mount.

~~~
U3A_PLUS_4G:/ $ mount
rootfs on / type rootfs (ro,seclabel)
tmpfs on /dev type tmpfs (rw,seclabel,nosuid,relatime,mode=755)
devpts on /dev/pts type devpts (rw,seclabel,relatime,mode=600)
proc on /proc type proc (rw,relatime,gid=3009,hidepid=2)
sysfs on /sys type sysfs (rw,seclabel,relatime)
selinuxfs on /sys/fs/selinux type selinuxfs (rw,relatime)
/dev/block/mmcblk0p36 on /system type ext4 (ro,seclabel,relatime,data=ordered)
/dev/block/mmcblk0p35 on /vendor type ext4 (ro,seclabel,relatime,data=ordered)
none on /acct type cgroup (rw,relatime,cpuacct)
none on /dev/memcg type cgroup (rw,relatime,memory)
debugfs on /sys/kernel/debug type debugfs (rw,seclabel,relatime)
none on /dev/stune type cgroup (rw,relatime,schedtune)
tmpfs on /mnt type tmpfs (rw,seclabel,relatime,mode=755,gid=1000)
none on /config type configfs (rw,relatime)
none on /dev/cpuctl type cgroup (rw,relatime,cpu)
none on /dev/cpuset type cgroup (rw,relatime,cpuset,noprefix,release_agent=/sbin/cpuset_release_agent)
pstore on /sys/fs/pstore type pstore (rw,seclabel,relatime)
tracefs on /sys/kernel/debug/tracing type tracefs (rw,seclabel,relatime)
/dev/block/mmcblk0p37 on /cache type ext4 (rw,seclabel,nosuid,nodev,noatime,discard,noauto_da_alloc,data=ordered)
/dev/block/mmcblk0p14 on /vendor/protect_f type ext4 (rw,seclabel,nosuid,nodev,noatime,nodelalloc,noauto_da_alloc,commit=1,data=ordered)
/dev/block/mmcblk0p15 on /vendor/protect_s type ext4 (rw,seclabel,nosuid,nodev,noatime,nodelalloc,noauto_da_alloc,commit=1,data=ordered)
/dev/block/mmcblk0p9 on /vendor/nvdata type ext4 (rw,seclabel,nosuid,nodev,noatime,discard,noauto_da_alloc,data=ordered)
/dev/block/mmcblk0p8 on /vendor/nvcfg type ext4 (rw,seclabel,nosuid,nodev,noatime,nodelalloc,noauto_da_alloc,commit=1,data=ordered)
/dev/block/mmcblk0p17 on /vendor/persist type ext4 (rw,seclabel,nosuid,nodev,noatime,nodelalloc,noauto_da_alloc,commit=1,data=ordered)
/dev/block/mmcblk0p12 on /tctpersist type ext4 (rw,seclabel,nosuid,nodev,noatime,nodelalloc,noauto_da_alloc,commit=1,data=ordered)
tmpfs on /storage type tmpfs (rw,seclabel,relatime,mode=755,gid=1000)
adb on /dev/usb-ffs/adb type functionfs (rw,relatime)
/dev/block/dm-0 on /data type f2fs (rw,lazytime,seclabel,nosuid,nodev,noatime,background_gc=on,discard,no_heap,user_xattr,inline_xattr,acl,inline_data,inline_dentry,extent_cache,mode=adaptive,active_logs=6)
/data/media on /mnt/runtime/default/emulated type sdcardfs (rw,nosuid,nodev,noexec,noatime,fsuid=1023,fsgid=1023,gid=1015,multiuser,mask=6,derive_gid)
/data/media on /storage/emulated type sdcardfs (rw,nosuid,nodev,noexec,noatime,fsuid=1023,fsgid=1023,gid=1015,multiuser,mask=6,derive_gid)
/data/media on /mnt/runtime/read/emulated type sdcardfs (rw,nosuid,nodev,noexec,noatime,fsuid=1023,fsgid=1023,gid=9997,multiuser,mask=23,derive_gid)
/data/media on /mnt/runtime/write/emulated type sdcardfs (rw,nosuid,nodev,noexec,noatime,fsuid=1023,fsgid=1023,gid=9997,multiuser,mask=7,derive_gid)
~~~

Els sistemes d'arxius poden ser organitzats en 3 categories:
- Memòries flash  
- Multimèdia
- Pseudo-sistemes d'arxius

### Memòries flash

La memòria flash és una memòria no volàtil (NVM) que pot ser esborrada i reprogramada en unitats anomenades blocs i manté la informació quan se li treu l'alimentació elèctrica. Els sistemes d'arxius més habituals són:

#### Extended File Allocation Table (ExFAT)

Sistema propietari de Microsoft creat per a pendrives USB i tergetes SD. Requereixen una llicència d'ús i per tant no forma part del kernel Linux estàndard. Tot i això molts fabricants hi donen suport.

#### Flash Friendly File System (F2FS)

Sistema que suporta dispositius Samsung amb kernel Linux 3.8 kernel. F2FS es basa en mètodes estructurats en log que òptimitzen la memòria flash NAND. Encara és transitori i s'està actualitzant

#### Yet Another Flash File System 2 (YAFFS2)

És un sistema d'arxius de codi obert, d'un sol fil de CPU llançat el 2002. Està dissenyat principalment per ser ràpid quan tracta amb flash NAND. YAFFS2 utilitza Out-of-band (OOB), i sovint no es captura ni es descodifica correctament durant l'adquisició forense, la qual cosa dificulta l'anàlisi.

YAFFS2 és un sistema de fitxers amb transaccions. La integritat de les dades està garantida, fins i tot en el cas d'una esgotament sobtat de la bateria.

Actualment, el YAFFS2 no està suportat per les versions més noves del nucli, però alguns fabricants de mòbils encara podrien continuar donant-li suport. Ara s'utilitza EXT4.

#### Robust File System (RFS)

Suporta la memòria flash NAND en dispositius Samsung. RFS és un sistema de fitxers FAT16 o FAT32, amb registre de transaccions habilitat. És conegut per ser molt lent.

---

### Multimèdia

#### Extended File System (EXT)

Va ser introduït el 1992 específicament per al nucli de Linux. Va ser un dels primers sistemes de fitxers, i va utilitzar un sistema de fitxers virtual. EXT2, EXT3, i EXT4 són les versions posteriors.

El registre de transaccions és l'avantatge principal de EXT3 sobre EXT2. Amb EXT3, si hi ha un tancament inesperat, no hi ha necessitat de verificar el sistema de fitxers.

El sistema de fitxers EXT4 ha guanyat importància amb dispositius mòbils que implementen processadors de doble nucli. El sistema de fitxers YAFFS2 té un coll d'ampolla en sistemes de doble nucli i amb la versió "Gingerbread" d'Android, el sistema de fitxers YAFFS es va canviar per EXT4.

#### FAT32 i Virtual File Allocation Table (VFAT)

És una extensió dels sistemes de fitxers FAT16 i FAT32. El sistema de fitxers FAT32 de Microsoft és compatible amb la majoria dels dispositius Android.

És recolzat per gairebé tots els principals sistemes operatius, incloent Windows, Linux i macOS. Això permet a aquests sistemes llegir, modificar i suprimir fàcilment els fitxers de la partició FAT32 del dispositiu Android.

La majoria de les targetes SD externes estan formatades utilitzant el sistema de fitxers FAT32.

---

### Pseudo sistemes d'arxius

Els pseudo-sistemes de fitxers, no són fitxers reals, sinó un agrupament lògic d'arxius. Els següents són alguns dels pseudo-sistemes de fitxers importants d'Android:

#### Sistema de fitxers arrel (rootfs)

És un dels components principals d'Android i conté tota la informació necessària per arrencar el dispositiu. Aquest sistema de fitxers està muntat a / (carpeta arrel). Si aquest sistema d'arxius és corrupte, el dispositiu no es pot arrencar.

#### Sistema de fitxers sysfs

Munta la carpeta /sys, que conté informació sobre la configuració del dispositiu.

~~~
U3A_PLUS_4G:/sys $ ls -la                                                                     
total 0
dr-xr-xr-x  16 root root 0 2022-03-01 10:47 .
drwxr-xr-x  19 root root 0 2022-03-01 10:47 ..
drwxr-xr-x   2 root root 0 2022-03-01 11:22 block
drwxr-xr-x   2 root root 0 2022-03-01 12:09 board_properties
drwxr-xr-x   2 root root 0 2022-03-01 12:09 bootinfo
drwxr-xr-x  19 root root 0 2022-03-01 11:59 bus
drwxr-xr-x  77 root root 0 2022-03-01 11:22 class
drwxr-xr-x   4 root root 0 2022-03-01 12:09 dev
drwxr-xr-x   9 root root 0 2022-03-01 10:47 devices
drwxr-xr-x   3 root root 0 2022-03-01 11:58 firmware
drwxr-xr-x   8 root root 0 2022-03-01 10:47 fs
drwxr-xr-x  10 root root 0 2022-03-01 10:47 kernel
drwxr-xr-x 116 root root 0 2022-03-01 11:54 module
drwxr-xr-x   2 root root 0 2022-03-01 12:09 mtk_rgu
drwxr-xr-x   2 root root 0 2022-03-01 12:09 mtuart
drwxr-xr-x   5 root root 0 2022-03-01 10:47 power
~~~

Aquestes carpetes estan majoritàriament relacionades amb la configuració, i solen ser de gran importància per a un investigador forense. Pot haver-hi algunes circumstàncies en les quals podríem voler comprovar si s'ha habilitat una configuració en particular pel telèfon. Cada carpeta conté un gran nombre de fitxers.

La captura d'aquestes dades a través de l'adquisició forense és el millor mètode per assegurar que les dades no es canvien durant un examen.

#### Sistema de fitxers devpts

Presenta les interfícies a sessions de Terminal en un dispositiu Android. Està muntat a /dev/pts. Sempre que s'estableix una connexió a Terminal (per exemple, quan un shell d'adb està connectat a un dispositiu Android) es crea un nou node sota /dev/pts.

~~~
shell@Android:/ $ ls -l /dev/pts
crw------- shell shell 136, 0 2013-10-26 16:56 0
~~~

#### Sistema de fitxers del cgroup

Cgroup significa "grups de control". Els dispositius Android utilitzen aquest sistema de fitxers per fer el seguiment de la feina. Aquestes dades generalment no són gaire útils durant l'anàlisi forense.

#### Sistema de fitxers proc

Conté informació sobre les estructures de dades del nucli, processos i altres dades relacionades amb el sistema al directori /proc. Per exemple, el directori /sys conté fitxers relacionats amb els paràmetres del nucli.
~~~
U3A_PLUS_4G:/proc/sys $ ls -la /proc/sys                                                      
total 0
dr-xr-xr-x   1 root root 0 2022-03-01 12:02 .
dr-xr-xr-x 413 root root 0 1970-01-01 01:00 ..
dr-xr-xr-x   1 root root 0 2022-03-01 12:13 debug
dr-xr-xr-x   1 root root 0 2022-03-01 12:13 dev
dr-xr-xr-x   1 root root 0 2022-03-01 12:13 fs
dr-xr-xr-x   1 root root 0 2022-03-01 12:13 kernel
dr-xr-xr-x   1 root root 0 2022-03-01 12:13 net
dr-xr-xr-x   1 root root 0 2022-03-01 12:02 vm
~~~

La següent ordre mostra tota la informació sobre la CPU del dispositiu:
~~~
U3A_PLUS_4G:/ $ cat /proc/cpuinfo                                                             
processor	: 0
Processor	: ARMv7 Processor rev 4 (v7l)
model name	: ARMv7 Processor rev 4 (v7l)
BogoMIPS	: 9.51
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm aes pmull sha1 sha2 crc32
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd03
CPU revision	: 4

processor	: 1
Processor	: ARMv7 Processor rev 4 (v7l)
model name	: ARMv7 Processor rev 4 (v7l)
BogoMIPS	: 9.51
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm aes pmull sha1 sha2 crc32
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd03
CPU revision	: 4

processor	: 2
Processor	: ARMv7 Processor rev 4 (v7l)
model name	: ARMv7 Processor rev 4 (v7l)
BogoMIPS	: 9.51
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm aes pmull sha1 sha2 crc32
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd03
CPU revision	: 4

processor	: 3
Processor	: ARMv7 Processor rev 4 (v7l)
model name	: ARMv7 Processor rev 4 (v7l)
BogoMIPS	: 9.51
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm aes pmull sha1 sha2 crc32
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd03
CPU revision	: 4

Hardware	: MT6739WA
Revision	: 0000
Serial		: 0000000000000000
~~~

De la mateixa manera, /proc/filesystems mostra la llista de sistemes de fitxers disponibles al dispositiu. Hi ha molts altres fitxers útils que proporcionen informació valuosa quan s'extreuen.

#### Sistema de fitxers tmpfs

És un dispositiu temporal d'emmagatzematge a la RAM (memòria volàtil). L'avantatge principal de l'ús de la RAM és un accés i recuperació més ràpid. Però una vegada que el dispositiu es reinicia o s'apaga, aquestes dades ja no seran accessibles. Per tant, és important que un investigador forense examini les dades a la RAM abans que es produeixi un reinici del dispositiu, o que extregui les dades a través de mètodes d'adquisició de la RAM.

Les eines forenses d'avui poden muntar fàcilment aquests sistemes de fitxers i mostrar el contingut en una GUI, permetent navegar i analitzar fàcilment els fitxers.
