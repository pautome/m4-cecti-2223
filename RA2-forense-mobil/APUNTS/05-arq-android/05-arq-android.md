# Arquitectura d'Android

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Arquitectura d'Android](#arquitectura-dandroid)
	- [1. Capa de Kernel de Linux](#1-capa-de-kernel-de-linux)
	- [2. Capa HAL (Hardware Abstraction Layer)](#2-capa-hal-hardware-abstraction-layer)
	- [3. Capa de biblioteques ("Libraries")](#3-capa-de-biblioteques-libraries)
		- [3.1. Android Runtime (ART)](#31-android-runtime-art)
		- [3.2. Biblioteques C/C++ natives](#32-biblioteques-cc-natives)
	- [4. Java API Framework](#4-java-api-framework)
	- [5. Apps del sistema](#5-apps-del-sistema)
- [Seguretat a Android](#seguretat-a-android)
	- [1. Kernel segur](#1-kernel-segur)
		- [1.1. Model de permisos](#11-model-de-permisos)
		- [1.2. Sandbox d'aplicacions](#12-sandbox-daplicacions)
		- [1.3. Interprocess Communication (IPC) Segur](#13-interprocess-communication-ipc-segur)
	- [2. Aplicacions signades](#2-aplicacions-signades)
	- [3. Security-Enhanced Linux (SELinux)](#3-security-enhanced-linux-selinux)
	- [4. Xifrat a Android (FDE) i (FBE)](#4-xifrat-a-android-fde-i-fbe)
		- [4.1. Full Disk Encryption (FDE)](#41-full-disk-encryption-fde)
		- [4.2. File-based Encryption (FBE)](#42-file-based-encryption-fbe)
	- [5. Android Keystore (magatzem de claus)](#5-android-keystore-magatzem-de-claus)
	- [6. Trusted Execution Environment (TEE)](#6-trusted-execution-environment-tee)
	- [7. Arrencada verificada](#7-arrencada-verificada)

<!-- /TOC -->

---

L'arquitectura d'Android, com es veu a la figura anterior, consisteix en una pila de capes de programari que s'executen una sobre l'altra. La capa superior utilitza serveis de la capa inferior per a donar serveis més elaborats a la seva superior.


<img src="imgs/forense-android-290aa6ad.png" width="600" align="center" />

Fig: Pila de capes de programari d'Android https://developer.android.com/guide/platform


<img src="imgs/02-arq-android-9ed63d08.png" width="800" align="center" />

Arquitectura Android - https://jarroba.com/arquitectura-android-art-vs-dalvik/


---

## 1. Capa de Kernel de Linux

La capa de Kernel de Linux és la **base de la plataforma Android**. Per exemple, l'Android Runtime (ART) confia en la capa de Kernel Linux i les seves funcionalitats per implementar fils i gestió de memòria a baix nivell.

El kernel Linux permet que els fabricants puguin desenvolupar drivers de maquinari en un **kernel ben conegut**. També permet fer ús de les característiques de seguretat de Linux.

<img src="imgs/02-arq-android-da08b664.png" width="600" align="center" />

---

## 2. Capa HAL (Hardware Abstraction Layer)

Aquesta capa ofereix unes interfícies estàndard que permeten a la capa superior, API Java, **accedir al maquinari**.
- La **HAL** consisteix en molts mòduls de biblioteques que implementen una interfície per a cada component de maquinari, com ara la càmera o el mòdul Bluetooth.
- Quan un element de l'API Java fa una crida per accedir a un dispositiu de maquinari, el sistema Android carrega el mòdul de biblioteca per aquell component.

<img src="imgs/02-arq-android-cb8210b9.png" width="600" align="center" />

---

## 3. Capa de biblioteques ("Libraries")

### 3.1. Android Runtime (ART)

L'ART permet **executar múltiples màquines virtuals** en dispositius amb poca memòria mitjançant l'execució de fitxers "bytecode" DEX (Dalvik Executable).

- **"Bytecode"** és un tipus de codi de programa que s'executa en sistemes anomenats VM (màquines virtuals). Els programadors poden usar el bytecode en la seva forma original en qualsevol plataforma en la qual operi la VM, per la qual cosa el codi és independent de la plataforma.

- **DEX** és un format de "bytecode" dissenyat especialment per a Android i optimitzat per un consum mínim de memòria. Les eines de compilació com "d8", compilen fonts Java a bytecode DEX, que es poden executar a la plataforma Android.

- **Dalvik**: Abans de la versió 5.0 d'Android (API nivell 21), el runtime d'execució d'Android era anomenat **Dalvik**. Si l'aplicació funciona bé amb ART, llavors també hauria de funcionar amb Dalvik per compatibilitat, però al contrari pot no ser veritat.

A partir de la versió 5.0 d'Android (API nivell 21) o superior, **cada aplicació** s'executa en el seu propi procés (VM) i **amb la seva pròpia instància de l'Android Runtime (ART)**.

Durant l'execució del programa cal traduir cada instrucció del "bytecode" a instruccions natives del processador, i això es pot fer traduint tot abans (AOT) o traduint a mida que s'executa (JIT).

#### Característiques principals de l'ART

- Compilació de codi anticipada en el moment de la instal·lació (**AOT=Ahead-of-time**) i/o compilació en temps real mentre s'executa l'aplicació (**JIT=Just in Time**). Són eines per convertir "bytecode" en codi màquina del maquinari en un moment o l'altre.
- **Recol·lecció de brossa optimitzada** (GC=Garbage Collection) GC és un procés que s'executa en segon pla i allibera de forma automàtica la memòria que ja no s'està fent servir al programa. En altres llenguatges de programació, ho ha de fer el propi programador.
- **Millor suport de depuració**, incloent un "profiler" dedicat de mostreig (un profiler permet veure com utilitza l'aplicació els recursos del telèfon com ara CPU, memòria, bateria i xarxa), excepcions de diagnòstic detallades i informes de fallades d'aplicació ("crash"), i la capacitat d'establir "watchpoints" per supervisar variables específiques.
- Amb Android 9 (nivell d'API 28) i superior, els paquets d'aplicacions de fitxers de format executable **Dalvik (DEX) es converteixen a un codi màquina més compacte**.

Android també inclou un conjunt de **biblioteques bàsiques (core libraries)** en temps d'execució (runtime) que proporcionen la major part de la funcionalitat del llenguatge de programació Java, incloent algunes característiques de llenguatge Java 8 que utilitza el framework Java API.

<img src="imgs/02-arq-android-60c5ad2b.png" width="300" align="center" />

---

- Android ART/Dalvik - https://source.android.com/devices/tech/dalvik
- Compilador d8 - https://developer.android.com/studio/command-line/d8
- DEX - https://source.android.com/devices/tech/dalvik/dex-format

---

### 3.2. Biblioteques C/C++ natives   

Molts components i serveis bàsics del sistema Android, com ART i HAL, es construeixen a partir de codi natiu que requereix biblioteques natives escrites en C i C++. La plataforma Android **proporciona frameworks API de Java per donar la funcionalitat d'algunes d'aquestes biblioteques natives a les aplicacions**. Per exemple, es pot accedir a OpenGL ES a través de l'API Java OpenGL del framework d'Android per afegir suport per dibuixar i manipular gràfics 2D i 3D a l'aplicació.

Si es desenvolupa una aplicació que requereix un codi C o C++, es pot utilitzar l'Android NDK que permet accedir a les biblioteques escrites en C/C++ des del codi Java.


<img src="imgs/02-arq-android-521a25f9.png" width="400" align="center" />

---

- Android NDK (per incloure codi C/C++) - https://developer.android.com/ndk

---

## 4. Java API Framework

Tot el conjunt de funcionalitats del sistema operatiu Android està disponible a través d'**APIs escrites en llenguatge Java**. Una API és un conjunt de protocols i eines utilitzat per desenvolupadors.

Aquestes API són els blocs de construcció que es necessiten per crear aplicacions d'Android simplificant la reutilització de components i serveis del sistema del nucli i mòduls, que inclouen el següent:

- Un **sistema de visualització** ric i extensible que es pot utilitzar per construir la interfície d'usuari d'una aplicació, incloent llistes, graelles, quadres de text, botons, i fins i tot un navegador web incrustable.
- Un **gestor de recursos**, que proporciona accés a recursos que no pertanyen al codi com cadenes localitzades, gràfics i fitxers de layout.
- Un **gestor de notificacions** que permet a totes les aplicacions mostrar alertes personalitzades a la barra d'estat.
- Un **gestor d'activitats** que gestiona el cicle de vida de les aplicacions i proporciona un entorn de navegació comuna.
- **Proveïdors de contingut** ("Content providers") que permeten a les aplicacions accedir a dades d'altres aplicacions, com l'app Contactes, o compartir les seves pròpies dades.

Els desenvolupadors tenen accés complet al mateix framework API que utilitzen les aplicacions del sistema Android.

<img src="imgs/02-arq-android-ae5ceaa1.png" width="600" align="center" />

---

## 5. Apps del sistema

Android proporciona un **conjunt d'aplicacions bàsiques** per:
- correu electrònic
- missatgeria de SMS
- calendaris
- navegació per Internet
- contactes
- altres...

Les aplicacions incloses a la plataforma no tenen un estatus especial entre les aplicacions que l'usuari decideix instal·lar. Així que **una aplicació de tercers pot convertir-se en l'aplicació per defecte**. Per exemple en el navegador web per defecte de l'usuari, el missatger de SMS, o fins i tot el teclat per defecte (hi ha algunes excepcions, com ara l'aplicació de configuració del sistema).

Les aplicacions del sistema funcionen com a aplicacions per als usuaris però també per proporcionar funcions que els desenvolupadors poden accedir des de la seva pròpia aplicació. Per exemple, si l'aplicació vol enviar un missatge de SMS, no cal construir aquesta funcionalitat; en el seu lloc es pot invocar qualsevol aplicació de SMS que ja estigui instal·lada per lliurar un missatge a qualsevol destinatari.

![](imgs/02-arq-android-936121ec.png)

---

# Seguretat a Android

**Android va ser dissenyat amb un enfocament específic en la seguretat**.

Ofereix i força certes característiques que salvaguarden les dades d'usuari presents al mòbil en un model de seguretat multicapa. Hi ha paràmetres segurs per defecte que protegiran l'usuari, i d'altres que els ofereixen els desenvolupadors per crear aplicacions segures.

Com a **investigador forense**, entendre el funcionament intern de la seguretat d'Android és crucial, ja que ajuda a identificar les millors tècniques a aplicar en una situació determinada, les limitacions tècniques de determinades metodologies, etc.

---

- Visió general de la seguretat Android - https://source.android.com/security

---

## 1. Kernel segur

El kernel Linux serveix com a base sòlida per Android, ja que Linux és una plataforma de confiança demostrada al llarg dels anys. Amb cada versió d'Android, la versió del kernel també ha canviat.

El nucli de Linux porta automàticament **algunes de les seves característiques de seguretat inherents** a la plataforma Android:
- Un model de **permisos basat en l'usuari**.
- **Aïllament dels processos** en execució (sandbox d'aplicació)
- **Comunicació segura entre processos** (IPC)

---

### 1.1. Model de permisos

Cada aplicació Android ha de tenir **permisos concedits per l'usuari per accedir a funcionalitats sensibles**, com ara accés a Internet, trucades de telèfon, etc.

D'aquesta manera **l'usuari pot conèixer amb antelació quines funcionalitats en el dispositiu accedirà l'App**.
- Així es prevenen atacs, però l'usuari que no entengui la implicació dels permisos que dona quedarà compromès.
- El **punt feble del sistema és l'usuari**, que pot permetre instal·lar malware.

**Fins Android 6.0** els usuaris havien de donar permisos en el moment de la instal·lació. Però **s'havia d'acceptar tots el permisos de cop o no instal·lar l'aplicació**.
- **A partir de la versió 6.0**, l'usuari **concedeix permisos durant l'execució de la aplicació**.
- D'aquesta manera, **l'usuari te més control de la funcionalitat de l'app** i pot permetre selectivament els permisos. Per exemple, es pot permetre a una determinada App l'accés a Internet però no a la ubicació GPS.
- L'usuari **podrà revocar permisos a l'opció Settings**. Des de la perspectiva forense, la informació que es podrà extreure del dispositiu dependrà no només del dispositiu i les Apps instal·lades si no també dels permisos configurats per l'usuari.  

---

### 1.2. Sandbox d'aplicacions

- https://source.android.com/security/app-sandbox?hl=en

En Linux **cada usuari té assignat un UID (User Identifier) únic**. Els usuaris no poden accedir a les dades dels altres usuaris (ni memòria, ni processos, ni arxius...). Les aplicacions que executa un determinat usuari s'executen amb el mateix nivell de privilegis.

A **Android, cada aplicació té un únic usuari amb el seu UID propi de manera que s'executa com un procés separat**. Si una aplicació/usuari intenta accedir als recursos d'una altra aplicació/usuari, el kernel no ho permet per que no te els privilegis necessaris.

En definitiva, l'usuari/aplicació A:
- No pot llegir els fitxers de B
- No pot esgotar la memòria de B
- No pot esgotar els recursos de CPU de B
- No pot esgotar els dispositius de B (ex: telefonia, GPS, Bluetooth)

---


### 1.3. Interprocess Communication (IPC) Segur

Android ofereix **comunicació entre processos (IPC) segur**, que permet a una activitat dintre d'una aplicació enviar missatges a una altra activitat dintre de la mateixa aplicació o una altra. Les aplicacions forenses també fan servir aquesta qualitat per a accedir a dades del dispositiu.  

---

- IPC - https://source.android.com/security/overview/app-security#interprocess-communication

---

## 2. Aplicacions signades

Totes les **aplicacions instal·lades han d'estar signades digitalment** de forma obligatòria. Només es poden posar Apps al Play Store de Google si s'han signat.

La **clau privada amb la que es signa és propietat del desenvolupador**. Amb aquesta clau privada el desenvolupador pot actualitzar, compartir dades entre aplicacions, etc.

Les aplicacions sense signar són rebutjades quan s'intenta instal·lar-les per Google Play o l'instal·lador de paquets del dispositiu.

A Android 8.0 i superiors, **els usuaris han de permetre instal·lar aplicacions desconegudes si volen instal·lar aplicacions no signades**.


<img src="imgs/02-arq-android-9f10b3b7.png" width="800" align="center" />

Creació d'APK Android https://jarroba.com/arquitectura-android-art-vs-dalvik/

---

- APK signat - https://source.android.com/security/apksigning?hl=en
- Comprovar signatura d'APK - https://source.android.com/security/features/apk-verity?hl=en

---

## 3. Security-Enhanced Linux (SELinux)

SELinux es va introduir amb l'Android 4.3 i va ser forçat a la versió 5.0.

El sistema tradicional de seguretat en Linux és el **"Discretionary Access Control" (DAC)**.
- Per exemple, els drets d'accés al sistema d'arxius funciona amb aquest model.
- En aquest sistema, el propietari d'un objecte controla la seva llista de control d'accés (ACL) i té control total.

SELinux utilitza **Mandatory Access Control (MAC)**, que consulta una autoritat central cada cop que s'intenta accedir a un objecte per part d'una aplicació per assegurar-se que pot accedir a l'objecte, inclús si l'usuari és root.
- Assegura que les aplicacions treballen en entorns aïllats. Així, tot i que un usuari instal·li una App maliciosa, el malware no podrà accedir al sistema operatiu i corrompre el dispositiu.
- **SELinux força MAC a tots els processos**, incloent els que s'executen amb privilegis de root. En SELinux, **tot el que no està explícitament permès es denega per defecte**.

Pot operar en dos modes:
- **Permissiu**: que guarda als logs (dmesg i logcat) les denegacions de permisos però no les força. Aquest mode es fa servir per a depurar les aplicacions mentre es fa desenvolupament.
- **Forçat**: fa les entrades als logs i força les denegacions. Aquest és el mode que utilitza Android per defecte.

SELinux en Android - https://source.android.com/security/selinux

---

## 4. Xifrat a Android (FDE) i (FBE)

Android adopta dues estratègies de xifrat de les dades.

### 4.1. Full Disk Encryption (FDE)

**FDE encripta totes les dades del disc de l'usuari usant una única clau secreta**.
- Aquesta clau és xifrada al seu torn usant un PIN, un patró o un password configurat per l'usuari. D'aquesta manera no cal canviar la clau i tornar a xifrar tot si canviem el PIN o password, només xifrar la clau secreta.
- A partir d'aquell moment, **totes les dades creades són xifrades abans de ser escrites al disc**, i totes les lectures desxifren les dades abans de retornar-les al procés que les ha sol·licitat.
- Amb Android 6.0 Marshmallow, Google va fer obligatori FDE per a la majoria de dispositius, sempre i quan el hardware compleixi uns estàndards mínims.

**FDE d'Android funciona només amb "Embedded Multimedia Card" (eMMC) i dispositius flash similars** que es presenten a si mateixes com a dispositius de blocs (discos).

Com que l'accés a les dades està protegit darrere d'una única credencial d'usuari, **hi ha funcions com les alarmes que no poden operar**, els serveis d'accessibilitat no estan disponibles, i els telèfons no poden rebre trucades si el mòbil està blocat.

---

### 4.2. File-based Encryption (FBE)

Des de l'Android 7.x, Google ha canviat el seu model de xifrat des de FDE a **"file-based encryption" (FBE)**.
- En aquest nou model, **cada fitxer és xifrat independentment amb diferents claus** que poden ser desblocades independentment.
- D'aquesta manera es pot accedir als fitxers de forma independent sense la necessitat de desxifrar la partició completa.
- Tot això **permet mostrar notificacions obertes o accedir a fitxers de boot** sense necessitat d'esperar que l'usuari desbloqui el telèfon.

---

- Xifrat a Android - https://source.android.com/security/encryption?hl=en

---

## 5. Android Keystore (magatzem de claus)

Android Keystore és un **magatzem de claus criptogràfiques** que evita un accés no autoritzat a aquestes.
- Les claus guardades a un Keystore poden ser usades per a realitzar operacions criptogràfiques però mai es poden extreure del Keystore.
- Aquestes claus donen seguretat a nivell de hardware de manera que inclús si el sistema operatiu és compromès, les claus del mòdul hardware seguiran segures.

---

- Keystore - https://source.android.com/security/keystore

---

## 6. Trusted Execution Environment (TEE)


Un **TEE és una àrea aïllada** (típicament un altre processador) per a guardar dades confidencials i executar codi de forma segura. El processador principal, que és considerat insegur, delega al TEE les operacions que requereixen ús de dades secretes.

A Android es fa servir **Trusty que és un sistema operatiu segur per tenir un TEE**. Trusty funciona sobre el mateix processador que Android, però Trusty està aïllat de la resta dels sistema per hardware i software.

Android no està limitat a Trusty, també pot fer ús d'altres TEE.

---

- Trusty TEE - https://source.android.com/security/trusty

---

## 7. Arrencada verificada

**El codi que s'arrenca en un Android és verificat criptogràficament mitjançant hashs signats** per l'autoritat arrel del sistema (pertany a Android) i així comprovar que prové d'un lloc confiable i no d'un atacant.

Es comprova:
- el kernel carregat de la partició boot
- l'arbre de hardware del dispositiu no detectable automàticament carregat de la partició dtbo
- la partició del sistema
- la partició del fabricant, etc.

---

![](imgs/02-arq-android-1b22388a.png)

---

S'estableix una **cadena de confiança completa** i evita una càrrega fraudulenta d'un altre sistema operatiu.
- Cada etapa de l'arrencada comprova la integritat i autenticitat del codi de la següent etapa abans de continuar.
- A més també es comproven les dades usades (hashs en temps real que posen el dispositiu en mode "corrupte" si no hi ha integritat) i s'impedeix la instal·lació d'una versió més antiga d'Android, per aprofitar alguna possible vulnerabilitat (**Rollback protection**).

---

## Referències

- Arrencada verificada - https://source.android.com/security/verifiedboot
- Roolback protection - https://source.android.com/security/verifiedboot/verified-boot#rollback-protection

---
