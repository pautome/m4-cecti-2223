# Códigos secretos comunes para la mayoría de los teléfonos con Android

https://www.malavida.com/es/guias-trucos/lista-completa-con-los-codigos-secretos-de-android-017216

- Ver el IMEI del teléfono: *#06 #
- Menú de información: *#0*#
- Menú de información general: *#*#4636#*#*
- Información de cámara: *#*#34971539#*#*
- Versión de software TLC: *#*#1111#*#*
- Versión de software PDA: *#*#1234#*#*
- Información de software y hardware: *#12580*369#
- Ver el estado de bloqueo del dispositivo: *#7465625#
- Ver la dirección MAC del dispositivo: *#*#232338#*#*
- Ver la versión de pantalla táctil: *#*#2663#*#*
- Ver la versión de RAM: *#*#3264#*#*
- Ver la dirección Bluetooth del teléfono: *#*#232337#*#
- Ver el estado de Google Talk: *#*#8255#*#*
- Ver la información de PDA y hardware: *#*#4986*2650468#*#*
- Ver información de FTA: *#*#2222#*#*
- Ver información de firmware y registro de cambios: *#*#44336#*#*
- Realiza una copia de seguridad de las carpetas de media: *#*#273282*255*663282*#*#*
- Modo de prueba: *#*#197328640#*#*
- Prueba del adaptador wifi: *#*#232339#*#*
- Prueba de brillo y vibración: *#*#0842#*#*
- Prueba de pantalla táctil: *#*#2664#*#*
- Prueba de Bluetooth: *#*#232331#*#*
- Prueba de campo: *#*#7262626#*#*
- Prueba rápido de GPS: *#*#1472365#*#*
- Prueba completa de GPS: *#*#1575#*#*
- Prueba de Loopback: *#*#0283#*#*
- Prueba de LCD: *#*#0*#*#*
- Prueba de audio: *#*#0289#*#*
- Prueba de sensor de aproximación: *#*#0588#*#*
- Configuración de diagnóstico: *#9090#
- Configuración de HSDPA y HSUPA: *#301279#
- Configuración de entrada de USB: *#872564#
- System Dump: *#9900#
- Mostrar el menú EPST: ##778 y botón de llamada

## Códigos secretos para dispositivos Samsung

- Mostrar el firmware actual: *#1234#
- Modo de diagnóstico y configuración general: #*#4636#*#*
- Restablecimiento por software de fábrica: *#*#7780#*#*
- Muestra el código de producto: *2767*4387264636#
- Menú principal del modo de servicio: *#*#197328640#*#*
- Información de software y hardware: *#12580*369#
- Dirección de adaptador Bluetooth: *#232337#
- Lectura de ADC: *#0228#
- Selección de banda RF: *#2263#
- Información de cifrado: *#32489#
- Configuración de diagnóstico: *#9090#
- Modo de prueba para la conexión WLAN: *#232339#
- Prueba de motor de vibración: *#0842#
- Dirección MAC del WLAN: *#232338#
- Control de modo USB I2C: *#7284#
- Modo de prueba de audio: *#0673#
- Modo de prueba general: *#0*#
- Configuración de GCF: *#4238378#
- Prueba de reloj en tiempo real: *#0782#
- Control de bucle invertido de audio: *#0283#
- Modo de prueba LBS: *#3214789650#
- Menú de control GPS: *#1575#
- Menú RIL Dump: *#745#
- Menú de creación de datos: *#273283*255*3282*#
- Modo de prueba del sensor de proximidad: *#0588#
- Actualización del firmware de la cámara: *#34971539#
- Modo de prueba del sensor de luz: *#0589#
- Modo de ingeniería del WLAN: *#526#
- Menú de volcado de depuración: *#746#
- Número de serie del flash NAND: *#03#
- Versión del software Información: *#44336#
- Modo de volcado del sistema: *#9900#
- Modo de creación de datos en la tarjeta SD: *#273283*255*663282*#
- Uso de datos: *#3282*727336*#
- Actualización de firmware TSP / TSK: *#2663#
- Reasignar el apagado para finalizar la llamada TSK: *#7594#
- Menú de firmware de la cámara: *#7412365# y *#*#34971539#*#*
- Modo de ingeniería WLAN: *#528#
- Modo de prueba de melodía: *#0289#
- Menú de actualización de OTA: *#8736364#
- Historial de pruebas: *#07#
- Menú de control HSDPA/HSUPA: *#301279#
- Menú de prueba rápida: *#7353#
- Ver el estado del bloqueo del teléfono: *#7465625#
- Selección automática de respuestas: *#272886#

## Códigos secretos para móviles Huawei
- Información del teléfono, uso y batería: *#*#4636#*#*
- Restablecimiento de fábrica: ##258741
- Modo de pruebas: ##147852
- Ajustes NAM: #8746846549
- Tarjeta RUIM (información personal sobre el usuario): #8746846549
- Prueba de hardware: ##5674165485
- Entrar en el menú de servicio: *#0*#
- Información detallada de la cámara: *#*#34971539#*#*
- Copia de seguridad de todos los archivos multimedia: *#*#273282*255*663282*#*#*
- Prueba de la red inalámbrica: *#*#232339#*#*
- Habilitar el modo de prueba para el servicio: *#*#197328640#*#*
- Prueba de retroiluminación y vibración: *#*#0842#*#*
- Pruebe la pantalla táctil: *#*#2664#*#*
- Versión del software FTA: *#*#1111#*#*
- Información completa de software y hardware: *#12580*369#
- Configuración de diagnóstico: *#9090#
- Control de registro USB: *#872564 #
- Modo de volcado del sistema: *#9900#
- Menú de control HSDPA/HSUPA: *#301279#
- Ver el estado del bloqueo del teléfono: *#7465625#
- Muestra la dirección MAC de WiFi: *#*#232338#*#*
- Prueba de GPS: *#*#1575#*#*

## Códigos secretos para los dispositivos de Sony
- Información sobre el sistema operativo del teléfono y la operadora: *#*#7378423#*#*
- Prueba de sonido: *#*#0673#*#* o *#*#0289#*#*
- Información sobre la batería: *#*#4636#*#*
- Restablecer de fábrica: *2767*3855#
- Copia de seguridad de los ficheros multimedia: *#*#273283*255*663282*#*#*
- Ver la dirección MAC de los dispositivos Bluetooth: *#*#232337#*#*

## Códigos secretos para móviles HTC
- Versión de RAM: ##3264##
- Mostrar información sobre el teléfono, la batería y las estadísticas de uso: *#*#4636#*#*
- Información de hardware y software: *#12580 *369 #
- Muestra la hora de compilación y el número de lista de cambios: *#*#44336#*#*
- Muestra la dirección MAC del Wifi: *#*#232338#*#*
- Muestra la versión de pantalla táctil del dispositivo: *#*#2663#*#*
- Versión de la memoria RAM: *#*#3264#*#*
- Muestra la dirección del dispositivo Bluetooth: *#*#232337#*#
- Información de firmware de PDA y dispositivo: *#*#1234#*#*
- Versión del software FTA: *#*#1111#*#*
- Muestra información completa sobre la cámara del dispositivo: *#*#34971539#*#*
- Versión FTA del hardware: *#*#2222#*#*

## Códigos secretos para teléfonos Xiaomi

- Acceder a la configuración de Google Partner: ##759##
- Prueba del sensor de proximidad: ##0588 ##
- Apagar y desactivar: ###61
- Estado de bloqueo del teléfono: #7465625 #
- Control de registro USB: *#872564 #
- Cambiar el comportamiento del botón de encendido: habilita el apagado directo una vez que pruebe este código: *#*#7594#*#*
- Estado de bloqueo de red: *#7465625 #
- Prueba GPS avanzada: ##1575##
- Calendario de eventos: ##225##
- Modo de volcado del sistema: *#9900 #
- Calendario: *#*#225#*#*
- Prueba de control de calidad: *#*#64663#*#*
- Formatee su teléfono inteligente: *2767*3855 #
- Restablecer dispositivo: *#*#7780#*#*

## Códigos secretos para teléfonos OPPO

- Verificar la información de la cámara: *#*#34971539#*#*
- Apagar con un solo toque del botón de encendido: *#*#7594#*#*
- Copia de seguridad de sus archivos multimedia: *#*#273283*255*663282*#*#*
- Comprobar la actividad de servicio: *#*#197328640#*#*
- Mostrar la dirección del dispositivo Bluetooth: *#*#232337#* #
- Supervisar el servicio Google Talk: *#*#8255#*#*
- Pruebas de LAN inalámbrica: *#*#232339#*#* O *#*#526#*#*
- Dirección MAC del Wifi: *#*#232338#*#*
- Comprobar la pantalla LCD: *#*#0*#*#*
- Otras pruebas GPS: *#*#1575#*#*
- Pruebas de bucle invertido de paquetes: *#*#0283#*#*
- Información de firmware de PDA, teléfono, hardware, fecha de llamada de RF: *#*#4986*2650468#*#*
- Para encontrar la versión de FTA Software: *#*#1111#*#*
- Prueba de audio: *#*#0673#*#* O *#*#0289#*#*
- Pruebas de vibración y retroiluminación: *#*#0842#*#*
- Comprobar el funcionamiento del sensor de proximidad: *#*#0588#*#*
- Verificar la versión de la pantalla táctil: *#*#2663#*#*
- Comprobar la pantalla táctil: *#*#2664#*#*
- Ver la versión de RAM: *#*#3264#*#*
