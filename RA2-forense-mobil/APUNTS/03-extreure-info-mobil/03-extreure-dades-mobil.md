# Extreure informació del mòbil

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Extreure informació del mòbil](#extreure-informaci-del-mbil)
	- [Mode Recovery vs Fastboot](#mode-recovery-vs-fastboot)
		- [1. Mode Recovery](#1-mode-recovery)
			- [Accés al mode Recovery](#accs-al-mode-recovery)
		- [2. Fastboot](#2-fastboot)
			- [Programa de l'Android SDK "fastboot.exe"](#programa-de-landroid-sdk-fastbootexe)
			- [Accès a Fastboot en el mòbil](#accs-a-fastboot-en-el-mbil)
		- [Comandes de Fastboot](#comandes-de-fastboot)
	- [Desblocar i Entrar al mòbil Android](#desblocar-i-entrar-al-mbil-android)
		- [Usar Android Device manager per desblocar el mòbil (Android < v.5)](#usar-android-device-manager-per-desblocar-el-mbil-android-v5)
	- [Saltar el bloqueig de pantalla](#saltar-el-bloqueig-de-pantalla)
		- [Usar ADB per mirar de saltar la protecció](#usar-adb-per-mirar-de-saltar-la-protecci)
			- [Esborrar gesture.key](#esborrar-gesturekey)
			- [Actualitzar Settings.db](#actualitzar-settingsdb)
			- [Esborrar arxius de bloqueig](#esborrar-arxius-de-bloqueig)
			- [Versions superiors a Android 6.0](#versions-superiors-a-android-60)
		- [Smudge Atack](#smudge-atack)
		- [Password o patró oblidat](#password-o-patr-oblidat)
		- [Saltar bloqueig d'aplicacions de tercers](#saltar-bloqueig-daplicacions-de-tercers)
	- [Codis de control dels mòbils Android](#codis-de-control-dels-mbils-android)
	- [IMEI](#imei)
		- [Obtenir l'IMEI](#obtenir-limei)
		- [Utilitat de l'IMEI](#utilitat-de-limei)
		- [Buscar informació a Imei.info](#buscar-informaci-a-imeiinfo)
	- [Obtenir informació a partir de la targeta SIM](#obtenir-informaci-a-partir-de-la-targeta-sim)
	- [Extracció de dades lògica](#extracci-de-dades-lgica)
		- [1. Extracció amb ADB pull](#1-extracci-amb-adb-pull)
		- [2. Extracció amb ADB backup](#2-extracci-amb-adb-backup)
		- [3. Extracció amb Android Studio](#3-extracci-amb-android-studio)
		- [4. Extracció ADB dumpsys (extracció de dades en viu)](#4-extracci-adb-dumpsys-extracci-de-dades-en-viu)
		- [5. Usar Cellebrite & AXIOM](#5-usar-cellebrite-axiom)
	- [Root Android](#root-android)
		- [ROM, stock ROM, custom ROM](#rom-stock-rom-custom-rom)
		- [Procés de ROOT](#procs-de-root)
		- [Exemple d'utilitat per a fer Root: Magisk](#exemple-dutilitat-per-a-fer-root-magisk)
		- [Altres mètodes per a fer Root](#altres-mtodes-per-a-fer-root)
		- [Unroot](#unroot)
		- [Netejar Bloatware (sense root)](#netejar-bloatware-sense-root)
	- [Referències](#referncies)

<!-- /TOC -->
---

## Mode Recovery vs Fastboot

Els dispositius Android tenen **3 particions molt importants**:
- **Boot loader: /boot**: La primera partició en carregar, decideix quina serà la següent partició a iniciar-se, que normalment és la ROM d'Android. Conté el kernel i la ramdisk (memòria virtual que executa alguns processos inicials). S'utilitza també quan es fa un **reset de fàbrica** del dispositiu i per instal·lar actualitzacions.
- **Recovery: /recovery**: Partició de recuperació que conté programari de recuperació. El menú de Recovery té **menys opcions que el Fastboot** i per això hi ha programari de recuperació fet a mida (custom recovery).
- **Android ROM: /system**: Sistema operatiu Android i aplicacions.

Hi ha algunes particions més, interessants pel forense:
- **Dades /data**: Conté les dades dels usuaris. Aplicacions que l'usuari instal·la. És la memòria interna del telèfon. Aquesta partició es reinicialitza en fer el procés de paràmetres de fàbrica.
- **/cache:** Conté dades i arxius accedits sovint per part de l'usuari i el sistema. Serveix per millorar el rendiment del telèfon.
- **/misc:** Guarda configuracions de l'operador i maquinari del telèfon. Important pel funcionament correcte del telèfon.

Tenim **dos modes d'arrencada d'emergència** del dispositiu, a part del normal que arrenca Android.

![](imgs/05-extreure-info-mobil-2-b0dbd262.png)

https://tektab.com/2015/10/31/android-bootloaderfastboot-mode-and-recovery-mode-explained/

---

### 1. Mode Recovery

**Recovery** és un petit sistema operatiu resident a la partició /recovery, amb el que es pot fer un reset de fàbrica o actualitzar el sistema operatiu ("firmware") amb imatges de sistema del fabricant (que es guarden a una memòria SD, d'on es llegeix).

És un **mode ideal per a realitzar modificacions en el sistema**, como flashejar ROM o modificar apps del sistema (per exemple, instal·lar les aplicacions de Google en un mòbil que no les inclou).

Les dues funcions principals són:
- esborrar totes les dades de l'usuari.
- instal·lar actualitzacions.

Per exemple, quan es fa un **reinici de fàbrica s'inicia el mode recovery** i esborra totes les dades, inclosa la SD. També passa quan s'actualitza el dispositiu.

Cada telèfon hauria de tenir el recovery estàndard que proporciona Google, però **molts fabricants el substitueixen** per una eina pròpia per vàries raons:
- El Recovery original és molt potent tot i que té opcions limitades per disseny. Es substitueix per **evitar que es faci malbé el mòbil per accident**.
- També es fa per **millorar la interfície de l'aplicació i per afegir funcions extra** com:
  - restauració de dades
  - backup
  - esborrar la caché del sistema
  - el·liminar la partició de dades
  - actualitzar des d'adb o targeta SD, etc.

Com a exemples de **recovery creats per la comunitat** tenim **CWM** (que ja no s'actualitza) o **TWRP** (Team Win Recovery Project ) https://twrp.me/

---

#### Accés al mode Recovery

Es pot fer:
- amb la comanda **"adb reboot recovery"**.
- També es pot fer amb una **combinació de tecles com ara baixar volum + power**.

Si en mode recovery es pot accedir amb "adb" al dispositiu, llavors el recovery no és l'original si no un fet a mida.  

---

### 2. Fastboot

És un mode de recuperació que es pot utilitzar **quan la partició recovery està corrupta**.
- Requereix de la **connexió a un PC via USB i de l'enviament de comandes no interactives des del PC**.

El nom **Fastboot indica tres coses diferents**:
1. Un **protocol** per a comunicar el pc amb el dispositiu, via cable USB.
2. **Programari** que s'executa al dispositiu quan arrenca en mode Fastboot.
3. Fitxer de **programa executable (fastboot.exe en windows)** per a poder enviar comandes al programa del dispositiu via cable USB.

#### Programa de l'Android SDK "fastboot.exe"

- Permet **connectar a un dispositiu des d'un pc via un cable USB** i poder enviar actualitzacions de les particions del dispositiu o re-flashejar particions del dispositiu.
- Per exemple, permet **restaurar una partició** des de la partició de Recovery, o canviar la ROM.
- Com que el mode **fastboot s'inicia abans que Android** inclús si l'Android no està instal·lat al dispositiu, es pot fer servir com una alternativa al mode Recovery en cas que la partició del mode Recovery estigui corrupta.

El mode Fastboot és més útil en principi que el mode Recovery ja que es pot fer servir com un mètode de recuperació (recovery) quan no existeix un altre.

En el cas de Samsung, es substitueix Fastboot per la **utilitat Odín** per al pc.

---

#### Accès a Fastboot en el mòbil

La forma d'**accés al mode Fastboot** varia d'un fabricant a un altre.

- Per exemple, en mòbils Xiaomi es pot activar el fastboot prement simultàniament el **botó power + baixar volum**. Per sortir, podem mantenir premut el botó "power" (potser cal fins a 15 segons).
Si s'activen opcions de desenvolupador, el mode Fastboot és el propi de Xiaomi (en lloc de l'estàndard).

**NOTA**: Les tecles que es fan servir pel mode Fastboot i Recovery s'assemblen molt (potser una volum amunt i l'altra volum avall + power).

- o bé amb **adb reboot fastboot**, si el mòbil ho permet i te el mode debug activat.


---

### Comandes de Fastboot

Fastboot és una comanda del pc **semblant a adb però sense shell interactiu**. Les comandes més útils poden ser:

1. **fastboot devices**: llistat de dispositius connectats i arrencats amb fastboot.

2. **fastboot oem unlock**: Utilitzat per molts fabricants per a alliberar el bootloader i poder instal·lar una recovery o fer root del mòbil. **Alguns fabricants el bloquen**.
   - Aquesta comanda ens mostrarà una sèrie de passos (demana contrasenya de desblocat) per procedir.

3. **fastboot flash**: Permet **flashejar un arxiu .img** descarregat per al nostre dispositiu.

Exemples:
- **fastboot flash recovery recovery.img**: Flasheja i instal·la el recovery al nostre dispositiu a /recovery.
- **fastboot flash boot boot.img**: Cambia el sistema d'arrencada (/boot) del nostre dispositiu, necessari per a poder carregar el recovery en molts casos.
- **fastboot flash radio radio.img**: Instal·la els nous controladors de ràdio.
- **fastboot flash system system.img**: Instal·la un nou sistema complet a /system.

4. **fastboot flashall**: Permet flashejar automàticament tots els arxius .img que tinguem descarregats i copiats al directori de adb i fastboot. Molt útil quan cal **flashejar diversos arxius** en un dispositiu, per exemple, quan ha quedat en mode "soft brick".

5. **fastboot boot "imatge"**: permet **carregar «en mode live» una imatge** del sistema operatiu per provar-la sense escriure les dades a la memòria interna. La fan servir els desenvolupadors per provar un nou kernel o recovery, tot i que de cara a l'usuari comú te poca utilitat.

Aquesta darrera opció permetria carregar una imatge Android en mode "live" per a fer-hi modificacions sense canviar la partició de recovery.

---

Altres comandes per obtenir informació del mòbil:
~~~
fastboot oem device-info
fastboot oem get-bootinfo
fastboot getvar all
~~~

---

## Desblocar i Entrar al mòbil Android

Molts usuaris bloquen el seu dispositiu per mirar de protegir-ho. Si volem fer l'extracció de dades d'un mòbil, serà necessari accedir-hi. Veurem algunes **tècniques per a saltar el bloqueig del dispositiu**.

De tota manera, cal recordar alguns punts per evitar haver d'aplicar aquestes tècniques. Cal comprovar si el dispositiu està desblocat en aquell moment i mirar de donar millor accés al dispositiu per l'adquisició:
1. Activar si és possible la **depuració per USB**.
2. **Desactivar les opcions de suspensió del mòbil** per que no es bloqui automàticament i s'aturi l'adquisició (o activar opcions "Stay Awake").
3. **Augmentar el temps de suspensió** de la pantalla.

També cal recordar el tema d'**aïllar correctament el dispositiu** per evitar accés remot, esborrat remot, etc.
- Les opcions de Google permeten esborrar les dades o blocar el mòbil.
- Existeixen aplicacions de tercers que fan el mateix.
- Podem activar el **mode Avió** si el mòbil no està blocat, mantenint botó d'apagat.

---

### Usar Android Device manager per desblocar el mòbil (Android < v.5)

Molts mòbils Android porten el servei de gestió d'Android que permet localitzar el mòbil perdut. També es pot fer servir per a desblocar el dispositiu.
- Cal conèixer les **credencials de compte de Google**.
- Anar a http://google.com/android/devicemanager i iniciar sessió.
- Escollir opció "Lock".
- Entrar un password temporal i picar de nou Lock.
- Entrar el password temporal en el dispositiu per desblocar.

En mòbils Samsung, també es pot fer servir el servei "Find my mobile".

https://toolbox.iskysoft.com/android-unlock/android-device-manager-unlock.html

---

## Saltar el bloqueig de pantalla

Hi ha tres mètodes de bloqueig:
1. **Per patró**: Un dibuix que es fa unint punts a pantalla.
2. **Per PIN**: Codi de 4 dígits
3. **Per password**: Paraula de pas alfanumèrica.

Veurem alguns mètodes per a saltar el bloqueig del mòbil. Aquests poden provocar canvis en els dispositius i no hi ha garantia que funcionin.
- Caldria **provar abans en dispositius de prova**.
- **Cal tenir autorització** per a fer-ho

---

### Usar ADB per mirar de saltar la protecció

Connectarem el mòbil per USB i mitjançant ADB podem fer alguns intents.

#### Esborrar gesture.key

Per Android anterior a la versió 6.0 "Marshmallow", es pot esborrar el fitxer gesture.key per esborrar el patró del dispositiu. NOTA: Només es pot fer si el dispositiu està "Rootejat"

1. Connectar via usb
2. Executar:
~~~
adb shell
cd /data/system
rm gesture.key
~~~

3. Reiniciar el dispositiu. Si encara apareix el bloqueig de patró, podem dibuixar un aleatòriament i desblocar.

---

#### Actualitzar Settings.db

Es pot buscar la BD settings.db, en format SQLite, i canviar els valors de bloqueig a 0.

~~~
adb shell
cd /data/data/com.android.providers.settings/databases
sqlite3 settings.db
update system set value=0 where name='lock_pattern_autolock';
update system set value=0 where name='lock_screen.lockedoutpermanently';
.quit
~~~

#### Esborrar arxius de bloqueig

Es pot intentar esborrar els arxius que configuren el bloqueig.

~~~
adb shell su rm /data/system/locksettings.db rm /data/system/locksettings.db-wal
rm /data/system/locksettings.db-shm
~~~

---

#### Versions superiors a Android 6.0

Des de l'Android 6.0 "Marshmallow", ja no es guarda la informació sobre el mètode de bloqueig de pantalla al fitxer gesture.key ni password.key. En el seu lloc es guarda en altres fitxers que crea el gatekeeper-service. Cal esborrar els fitxers:

~~~
adb shell rm /data/system/gatekeeper.password.key
adb shell rm /data/system/gatekeeper.pattern.key
adb shell rm /data/system/locksettings.db
~~~

A partir de l'Android 4.2 es va introduir els múltiples usuaris, de manera que **les dades es segmenten per a cada usuari "humà"**. Per tant les dades es troben a /data/user/0, /data/user/1,/data/user/2, ...

Per retrocompatibilitat la carpeta /data/data té un enllaç simbòlic a /data/user/0.

La BD de dades de configuració es troba a /data/user/0/com.android.providers.settings/databases

---

### Smudge Atack

Smudge: Es tracta de les marques que es deixen a la pantalla en tocar-la amb els dits bruts o humits. Amb un llum adequat es pot veure el rastre deixat pels dits, com es pot feure a la foto següent.
- Es pot arribar a **deduir el patró**.
- Es pot arribar a deduir el PIN del dispositiu (sabent on estan situats els números).


<img src="imgs/05-extreure-info-mobil-2-e26ea21b.png" width="200" align="center" />

Pantalla amb marques dels dits (smudge)

---

### Password o patró oblidat

Fins Android 4.4 funcionava l'opció de recuperar password o patró a la pantalla de desbloqueig.

---

### Saltar bloqueig d'aplicacions de tercers

En el cas que el bloqueig es faci amb aplicacions de tercers, és possible que es pugui saltar arrencant el dispositiu en mode segur. Aquest mode no carrega cap de les aplicacions que s'han instal·lat per l'usuari, de manera que es podrà desactivar l'opció que s'arrenqui i bloquegi.

Pot ser que funcioni **mantenir el botó de power fins que aparegui el missatge de reiniciar**. Llavors **mantenir el botó power un altre cop fins que aparegui l'opció de reiniciar en mode segur**.

En **Xiaomi per exemple**, s'ha d'apagar el mòbil, tornar a encendre i quan aparegui el logo de MIUI mantenir el botó de baixar volum fins que arrenqui. Apareixerà una missatge a la part inferior esquerra de la pantalla.

---

## Codis de control dels mòbils Android

Podem obtenir molta informació a partir dels **codis de control** que es poden teclejar com un número de telèfon. Més informació a: https://www.malavida.com/es/guias-trucos/lista-completa-con-los-codigos-secretos-de-android-017216    


## IMEI

Una font molt important d'informació del telèfon mòbil és el seu número **IMEI (International Mobile Equipment Identity)**.

El número **IMEI (International Mobile Equipment Identity)** és un codi únic de 15 dígits que identifica amb precisió cada dispositiu que usi una targeta SIM a tot el món.
- Es va crear perquè **el número de la targeta SIM no pot ser un identificador permanent del dispositiu**.
- La targeta SIM està associada amb l'usuari i es pot transferir fàcilment d'un telèfon a l'altre.

Així, l'IMEI actua com un "fingerprint" del mòbil:
- És un número de 15 digits únic per a cada dispositiu al mon.
- Els primers 14 dígits els defineix l'Associació GSM.
- Els **primers 6 dígits del IMEI són el TAC (Type Approval Code)** i serveix per a trobar el model exacte del dispositiu.
- L'últim dígit és generat per un algorisme anomenat fórmula de Luhn i és un caràcter de control. https://es.planetcalc.com/2464/

Els operadors i fabricants de mòbils fan servir l'IMEI per poder **fer un seguiment dels "smartphones" que han sigut robats o compromesos**.

**NOTA: No s'ha de confondre el número IMEI amb el número de sèrie del dispositiu**. Els fabricants usen el número de sèrie a nivell intern, mentre que l'IMEI és un estàndard a tota la indústria.

Si un número IMEI està blocat o en una llista negra, serà difícil fer-hi res.
- La **llista negra de IMEI** es comparteix entre els proveïdors de telefonia i no és fàcil desblocar l'IMEI.
- Si un telèfon és blocat per una xarxa determinada, no funcionarà fins que aquesta el desbloqui.
- L'única opció és demanar al proveïdor que desbloqui el dispositiu.

---

### Obtenir l'IMEI

1. La forma més fàcil de verificar el IMEI en qualsevol telèfon és usar la **seqüència * # 06 #.**
2. Es pot mirar també a **Settings->About phone->Status->IMEI** (o una ruta similar depenent del mòbil).
3. **Sota la bateria** si aquesta és extraïble es pot trobar l'IMEI imprès en una enganxina.
4. A la **caixa del dispositiu** si en disposem.
5. Amb ADB, executant la comanda **"service call iphonesubinfo 4"**

![](imgs/04-extreure-info-mobil-03208706.png)


Segon IMEI (Android 9)
~~~
service call iphonesubinfo 3 i32 1 | grep -oE '[0-9a-f]{8} ' | while read hex; do echo -ne "\u${hex:4:4}\u${hex:0:4}"; done; echo
service call iphonesubinfo 3 i32 2 | grep -oE '[0-9a-f]{8} ' | while read hex; do echo -ne "\u${hex:4:4}\u${hex:0:4}"; done; echo
~~~

Font: https://www.preguntandroid.com/pregunta/33006/como-obtener-el-segundo-numero-imei-usando-adb

---

### Utilitat de l'IMEI

El número IMEI és útil en les situacions següents:
1. Per **conèixer la informació tècnica oficial**: quantitat de memòria, CPU, placa base, aspecte físic, sistema operatiu, etc.
  - Pot ser que no es correspongui al fabricant que pensàvem que era d'entrada.
  - Pot ser que el sistema Android instal·lat no sigui la versió que hauria de tenir segons les característiques tècniques.
2. Per obtenir **informació "secreta"** sobre el producte que a vegades permet localitzar el telèfon intel·ligent (a partir dels 6 primers dígits).
3. Quan es vol **enviar el dispositiu al servei tècnic**, per a completar els formularis de garantia.
4. Per **denunciar un telèfon robat o perdut** a la policia o a l'operador de xarxa.
5. Per **bloquejar el telèfon i deshabilitar el dispositiu**, independentment de si es canvia o es treu la targeta SIM.
6. Per **veure si el dispositiu és vàlid i original (si és de segona ma)**.

---

### Buscar informació a Imei.info

Una de les característiques més importants del cercador d'Imei.info són els verificadors en línia, que ens proporcionen:
- la xarxa i el país d'on prové originalment el seu dispositiu
- informació de garantia
- l'estat de bloqueig del senyal
- informe de llistes negres.
- data de compra
- informació de l'operador
- versió del sistema
- especificacions del dispositiu i més informació detallada.

També es poden obtenir alguns serveis avançats (que estan disponibles per a alguns fabricants):
- Consultar l'Estat del iPhone
- Estat de iCloud
- Estat de llista negra
- Informació del venedor
- comprovar la Xarxa
- comprovar SIMLock
- llista negra de telèfons
- Verificar Operadors
- Desbloquejar SIMLock

---

## Obtenir informació a partir de la targeta SIM

Si tenim la **targeta SIM del telèfon**, podem obtenir informació de diferents maneres:
1. Es pot fer servir la pàgina https://informacion-telefonos.com/ i posar el numero de mòbil per veure proveïdor i si està en alguna llista negra.
2. Es pot demanar el llistat de trucades al proveïdor (només per via judicial).
3. Es pot buscar el número a https://listaspam.com per a veure si està en alguna llista de spam.
4. Es pot utilitzar la pàgina https://tellows.es per posar el número i saber a qui pertany (fer una cerca inversa a partir del telèfon).
5. Es pot donar d'alta a l'agenda d'un mòbil i consultar a WhatsApp el perfil del número de telèfon.

---

## Extracció de dades lògica

Aconseguir accés de **Root no és sempre necessari per obtenir una adquisició lògica**. Però pot influir en la quantitat i el tipus de dades que es pot extreure.
- Fer el **Root del dispositiu pot alterar el dispositiu** i s'ha de fer només si tenim autorització legal per a fer-ho.

Hi ha vàries tècniques d'extracció lògica.

---

### 1. Extracció amb ADB pull

Permet copiar fitxers/carpetes del dispositiu.

~~~
adb pull ruta-remota ruta-local
~~~

---

### 2. Extracció amb ADB backup

Permet fer un **backup del dispositiu Android**. No cal ser root, però el dispositiu ha de poder desblocar-se, ja que en començar ho demana. Cal executar la comanda:

~~~
// Backup complet
adb backup -apk -shared -all -system -f fitxerbackupAndroid.ab

// Per una sola aplicació
adb backup -noapk com.your.packagename

// Si la aplicació és depurable, podem copiar les seves dades sense ser root
adb shell
run-as com.your.packagename
cp /data/data/com.your.packagename/
~~~

NOTA: Aquesta comanda està obsoleta i pot ser eliminada en versions posteriors.

---

### 3. Extracció amb Android Studio

També es pot extreure fitxers dels dispositius amb Android Studio >= 3.0 :
- **View > Tool Windows > Device File Explorer**.
- Expandir /data/data/[nom-paquet]

Només es pot fer en paquets que funcionen en mode debug per a dispositius no-Root.

---

### 4. Extracció ADB dumpsys (extracció de dades en viu)

Aquesta eina dona informació sobre els serveis del dispositiu Android.

1. Llista de serveis: adb shell dumpsys -l
2. Informació de tots els serveis: adb shell dumpsys | more
3. Informació de xarxa (usar servei netstats):

~~~
U3A_PLUS_4G:/ $ dumpsys netstats

Active interfaces:
  iface=wlan0 ident=[{type=WIFI, subType=COMBINED, networkId="Republica2", metered=false}]
Active UID interfaces:
  iface=wlan0 ident=[{type=WIFI, subType=COMBINED, networkId="Republica2", metered=false}]
Dev stats:
  Pending bytes: 1011464
  History since boot:
  ident=[{type=WIFI, subType=COMBINED, networkId="Republica2", metered=false}] uid=-1 set=ALL tag=0x0
    NetworkStatsHistory: bucketDuration=3600
      st=1646128800 rb=560931 rp=1339 tb=290113 tp=1150 op=0
      st=1646132400 rb=90556 rp=323 tb=69864 tp=252 op=0
Xt stats:
  Pending bytes: 962918
  History since boot:
  ident=[{type=WIFI, subType=COMBINED, networkId="Republica2", metered=false}] uid=-1 set=ALL tag=0x0
    NetworkStatsHistory: bucketDuration=3600
      st=1646128800 rb=541004 rp=1304 tb=273224 tp=1121 op=0
      st=1646132400 rb=83609 rp=259 tb=65081 tp=208 op=0

adb shell dumpsys netstats detail
...
~~~

---

### 5. Usar Cellebrite & AXIOM

Són eines comercials de pagament i per tant no les veurem.

**Cellebrite UFED** - Accés a dispositius mòbils de molts tipus. Accedeix a proves digitals crítiques de forma legal a partir de funcions i telèfons intel·ligents, drons, targetes SIM, targetes SD, dispositius GPS i més.
- https://cellebrite.com/en/ufed/

Magnet Axiom Mobile - Adquisició Android i iOS i anàlisi (timeline, Connexions, explorador de mitjans, etc.) - https://www.magnetforensics.com/products/magnet-axiom/mobile/

Amb versió de prova.

---

## Root Android

**"Rootejar"** el dispositiu Android permet **obtenir tots els drets de l'administrador**.
- Pot anular la garantia del dispositiu ja que l'obre a vulnerabilitats.
- Pot ser l'única opció per obtenir les dades del dispositiu si no tenim accés a les credencials.

### ROM, stock ROM, custom ROM

Cal conèixer alguns conceptes:

1. "ROM" (Read-Only-Memory): Es refereix actualment a una imatge de sistema Android. Instal·lar o flashejar la ROM es refereix a esborrar o sobrescriure la imatge d'Android original del mòbil per una que modifiqui el comportament del dispositiu.

2. "Stock ROM": Imatge dels sistema que porta el mòbil quan surt de fàbrica. A vegades es pot descarregar del lloc web del fabricant

3. "Custom ROM": Una modificació d'una dostribució "pura" d'Android feta pels fabricants, empreses de telefonia o altres desenvolupadors. Els canvis que incorporen poden incloure:
  - millora de rendiment
  - canvis de pantalles d'inici o pantalles de fons
  - canvis de programari, etc.  

Cal fer "root" del dispositiu per instal·lar "custom ROMS". Cal desblocar el "bootloader" per poder fer root.

### Procés de ROOT

El procés per a fer-ho varia entre dispositius. En general, **implica explotar un bug de seguretat del firmware per acabar copiant la comanda "su" (/system/xbin/su) i donant-li permís d'execució amb la comanda "chmod"**.

Abans de fer-ho, comprovem primer que no estigui ja fet. Algunes formes de comprovar-ho són:
- Fer servir la utilitat "Lookout", que és una aplicació d'Android que et diu si tens el mòbil amb root, si existeix alguna aplicació vulnerada, etc.
- Usar l'aplicació "Root Checker".
- Usar l'aplicació "SuperSu".
- Connectar via ADB (si és possible) i executar "su". Si apareix una finestra emergent, podem validar el root.

Molts processos per desprotegir el mòbil **comencen flashejant la partició de Recovery** per posar un de modificat. Després es fa una **actualització des de aquest Recovery**, potser des de la SD.

Però amb Android 7.x es va començar a forçar la comprovació estricta de l'arrencada. Així, **cada etapa de l'arrencada comprova la integritat i autenticitat de la següent abans d'executar-la**. Llavors, en cas de no integritat/autenticitat, no es permet l'arrencada.

Hi ha quatre passes fonamentals:
1. Desblocar OEM – Cal activar l'opció que permeti executar les comandes “fastboot” per desblocar el bootloader.
  - Els fabricants ho bloquen per evitar canvis accidentals fets pels usuaris. Cal anar a "opcions de desenvolupador" i "Unlock OEM". 
  - Alguns fabricants no ho posen, només bloquen el bootloader.
2. Desblocar el Bootloader, que permet instal·lar un "custom recovery" al dispositiu.
3. Insta·lar un Custom Recovery (com ara TWRP). Això permetrà flashejar una imatge d'Android modificada, cosa que un recovery per defecte no permetria (per exemple Magisk, Viper4android, etc.).
4. Flashejar Magisk, un conjunt d'eines open-source per personalitzar parts fonamentals d'Android que inclouen el “root”.

### Exemple d'utilitat per a fer Root: Magisk

Permet fer root a qualsevol Android que permeti instal·lar un recovery modificat.
- Això implica que ha de tenir el **bootloader alliberat**.
- Magisk **no toca la partició de sistema ni la de boot**.
- Llocs de descàrrega i documentació:
  - Oficial - https://github.com/topjohnwu/Magisk/releases
  - No oficial - https://magisk.me/

### Altres mètodes per a fer Root

- "Rootejar" Android - https://andro4all.com/otros-android/como-rootear-android

- One click Root - https://oneclickroot.com/

### Unroot

Pot ser interessant retornar a l'estat root el mòbil.

https://www.xda-developers.com/how-to-unroot-android-phone/#unroot-by-flashing-the-original-boot-image

### Netejar Bloatware (sense root)

Netejar programari de fàbrica sense ser root

https://www.xda-developers.com/uninstall-carrier-oem-bloatware-without-root-access/

---

## Referències

Mètodes per desblocar els mòbils - https://toolbox.iskysoft.com/android-unlock/

Hacking móvil - Entrar como usuario "root" en Android - https://www.youtube.com/watch?v=ZKlqqdbQl9o

Android recovery - https://www.androidcentral.com/what-recovery-android-z

Fastboot - https://www.androidcentral.com/android-z-what-fastboot

Custom recovery - https://www.xatakandroid.com/roms-android/que-twrp-como-se-instala-sirve-este-custom-recovery-para-android

- https://itoolab.com/unlock-android/how-to-crack-a-pattern-lock-on-android-phone/
- https://hackersonlineclub.com/bypass-android-pattern-lock/
- https://forensics.spreitzenbarth.de/2012/02/28/cracking-the-pattern-lock-on-android/
- https://en.droidwiki.org/wiki/Forgot_Pattern
- https://stackoverflow.com/questions/51466177/where-is-the-sqlite-database-that-holds-the-global-settings-stored-on-nougat
- https://www.xatakandroid.com/tutoriales/como-saber-bootloader-mi-movil-android-esta-desbloqueado-no
- https://www.xatakandroid.com/sistema-operativo/modo-recovery-modo-descarga-android-que-sirven

- Comprovar root - https://andro4all.com/otros-android/como-saber-movil-esta-rooteado
- Universal ADB Drivers - https://adb.clockworkmod.com/
- Inkwire Screen Share + Assist - https://www.inkwire.io/
- Gadget Hacks Android - https://android.gadgethacks.com
- Saber qui et truca - https://androidguias.com/quien-me-llama/
- Com va Loockout - https://www.androidsis.com/como-funciona-lookout/
- Desempaquetar backups d'Android - https://nelenkov.blogspot.com/2012/06/unpacking-android-backups.html
- Backup complet d'Android - https://technastic.com/take-full-backup-android-device/
- Pentesting amb ADB - https://y000o.medium.com/introducci%C3%B3n-al-pentesting-con-adb-17a8cda3d3b3
- Accedir a les dades de l'Android - https://stackoverflow.com/questions/13006315/how-to-access-data-data-folder-in-android-device
