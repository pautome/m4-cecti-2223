# Forense de mòbils

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Forense de mòbils](#forense-de-mbils)
	- [Reptes en forense de mòbil](#reptes-en-forense-de-mbil)
	- [Passes per a investigar:](#passes-per-a-investigar)
		- [1. Entrada del dispositiu](#1-entrada-del-dispositiu)
		- [2. Identificació](#2-identificaci)
		- [3. Preparació](#3-preparaci)
		- [4. Aïllament](#4-allament)
		- [5. Processat](#5-processat)
		- [6. Verificació](#6-verificaci)
		- [7. 8. informe i documentació](#7-8-informe-i-documentaci)
		- [9. Arxivat](#9-arxivat)
	- [Aproximacions pràctiques al forense de mòbils](#aproximacions-prctiques-al-forense-de-mbils)
		- [1. Comprendre els diferents sistemes operatius de mòbils](#1-comprendre-els-diferents-sistemes-operatius-de-mbils)
			- [Android](#android)
		- [2. Nivells d'aplicació de les eines d'adquisició](#2-nivells-daplicaci-de-les-eines-dadquisici)
			- [Extracció manual](#extracci-manual)
			- [Extracció lògica](#extracci-lgica)
			- [Extracció de bolcat binari (Hex Dump)](#extracci-de-bolcat-binari-hex-dump)
			- [Extracció del chip (Chip-off)](#extracci-del-chip-chip-off)
			- [Micro lectura](#micro-lectura)
	- [Evidències potencials als dispositius mòbils](#evidncies-potencials-als-dispositius-mbils)

<!-- /TOC -->

---

## Reptes en forense de mòbil

En el forense de mòbil hi ha una sèrie de problemes i reptes a solucionar que fan que sigui molt més complex el forense d'equips:

1. **Diferències de Maquinari**: Hi ha un a gran varietat de dispositius amb maquinari diferent.

2. **Sistemes operatius de mòbils**: Els sistemes són variats: Android, Apple iOS i amb moltes versions.

3. **Característiques de seguretat de plataformes mòbils**: S'aplica xifrat per defecte i protecció de dades dels usuaris amb ACL's.

4. **Adquisició sense modificar dades**: En els mòbils no basta amb apagar-ho perquè molts processos s'executen en segon pla. Per exemple l'aplicació d'alarmes del sistema. A vegades apagar pot modificar l'estat del mòbil.

5. **Tècniques antiforense**: En els mòbils s'apliquen tècniques "antiforense". Per exemple, s'oculta la informació, s'ofusca, s'esborra les dades de forma segura, etc.

6. **Recuperar passwords**: Si el dispositiu està protegit per contrasenya, cal accedir-hi per recuperar-la i saltar el bloqueig de pantalla. Hi ha mètodes que no funcionen a totes les versions.

7. **Falta de recursos**: Es necessiten moltes eines per que hi ha moltes casuístiques diferents. Per exemple, accessoris com cables USB, bateries, carregadors de diferents mòbils, etc.

8. **Natura dinàmica de les evidències digitals**: És molt fàcil que es modifiquin per exemple navegant amb un browser. Cal anotar tot el que es fa investigant per justificar els possibles canvis.

9. **Alteració del dispositiu**: És possible que s'hagi modificat les dades d'una aplicació movent-les de lloc o canviant el nom dels arxius del sistema. Cal tenir en compte el nivell d'expertesa del sospitós.

10. **Blindatge de les comunicacions**: El mòbils fan servir xarxes celul·lars, Wifi, Bluetooth i infraroges per la comunicació (vía radio i senyal infraroig) i per tant cal aïllar el dispositiu en bosses de Faraday o tapant sensors.

11. **Falta de disponibilitat d'eines**: Cal una combinació de moltes eines ja que només una pot no suportar tots els models o tenir totes les funcions necessàries. Escollir les eines adequades es complica.

12. **Programes maliciosos**: El dispositiu pot tenir algun malware, com virus o troians. Aquest malware es pot propagar a d'altres dispositius via wifi, cable, etc.

13. **Problemes legals**: En ser un dispositiu mòbil, pot ser que el crim s'hagi produït en diferents zones geogràfiques amb legislació diferent.

---

## Passes per a investigar:

El procès d'investigació tindria les següents passes: Developing Process for Mobile Device Forensics (Det. Cynthia A. Murphy): http://www.mobileforensicscentral.com/mfc/documents/Mobile%20Device%20Forensic%20Process%20v3.0.pdf

1. Entrada del dispositiu
2. Identificació
3. Preparació
4. Aïllament
5. Processat
6. Verificació
7. documentació
8. informe
9. Arxivat

---

### 1. Entrada del dispositiu

Quan es rep el dispositiu cal documentar el propietari i determinar els objectius de la investigació.
- Generar el documents adients (entrada de proves, cadena de custòdia).
- Cal respectar la llei.
- Podem intentar desactivar el password o el bloqueig si el mòbil està desblocat en aquell moment.

---

### 2. Identificació

Cal determinar en aquesta fase:
1. **Autoritats legals**: Quina autoritat legal es té per a fer el forense.
2. **Determinar les dades que cal extreure**: Així escollir les eines adequades.
3. **Informació d'identificació del dispositiu (model, versió, etc)**: Recollir dades del dispositiu i estat físic (ratllades, components addicionals, color, etc)
4. **Dispositius d'emmagatzematge addicionals**: targetes de memòria addicional que es poden extreure per a fer una adquisició forense normal (imatge).
5. **Altres fonts d'evidències**: empremtes dactilars, etc. Cal portar guants en manipular els dispositius.

---

### 3. Preparació

Cal buscar informació sobre:
- El model de telèfon.
- Els mètodes a utilitzar per aquest model, per exemple per a fer root.
- Eines d'adquisició adients.

---

### 4. Aïllament

Donat que els dispositius fan servir xarxes via ràdio:
- Connectats a xarxes, es pot modificar la informació que contenen (trucades, missatges, dades d'aplicacions, etc).
- També és possible destruir les dades remotament.

Llavors cal aïllar el dispositiu bàsicament de dues maneres:
- Avtivar **Mode avió**: Cal vigilar per que encara pot rebre senyals ja que als avions es permet wifi. No possible si el dispositiu està blocat.
- Ficar-ho a una **Bossa o caixa de Faraday**: També hi ha tendes i habitacions de Faraday per treballar a gust. Bloquen la radiació electromagnètica.

<img src="imgs/01-forense-mobil-97dbcd37.png" width="400" align="center" />

Bossa de Faraday

<img src="imgs/01-forense-mobil-4888cdf5.png" width="400" align="center" />

"Tenda" Faraday

https://www.teeltech.com/mobile-device-forensics-equipment/black-hole-data-bag-vector-kit/

---

### 5. Processat

**Cal identificar eines a usar**: preu, facilitat d'ús, aplicabilitat, etc.

El programari de forense és molt car i normalment es requereixen vàries eines per accedir a les dades.
- Cal assegurar-se que l'eina permet assegurar la integritat.
- Cal usar un mètode provat i reproduïble així com a "forensicament" segur.

#### 1. **L'adquisició física (imatge forense)**

És el mètode preferit. Extreu una imatge binària de la memòria.
- Normalment el mòbil està apagat.
- Aquest mètode normalment provoca pocs canvis en el dispositiu.

#### 2. **L'adquisició lògica**

Si no és possible o falla el mètode anterior, és la següent alternativa. És aconsellable fer-la igualment tot i que tinguem la física.
- Es copia el sistema d'arxius del dispositiu.
- S'hauria de fer sempre ja que conté informació que permet guiar l'anàlisi de la imatge física binària.

---

### 6. Verificació

Cal comprovar l'exactitud de les dades adquirides:
- **Comparant les dades extretes amb les del dispositiu**. Atenció per que això pot provocar canvis al dispositiu.
- **Usar múltiples eines i comparar els resultats**. Seria ideal.
- **Usar funcions de hash**, ja que totes les imatges cal que en tinguin un. En el cas d'imatges lògiques, es calcula un hash per a cada fitxer.

Per a qualsevol discrepància entre el hash original i el de la còpia, cal tenir una justificació. Per exemple si s'ha tornat a iniciar el dispositiu i tornat a adquirir una imatge, el hash pot canviar.

---

### 7. 8. informe i documentació

Cal **documentar tot el procès**, mentre es porta a terme. Cal incloure:
- Data inici i final.
- Estat físic del dispositiu (ratllades, color, etc.)
- Fotos del telèfon i components.
- Estat del telèfon (on-off)
- Model i versió del telèfon i sistema
- Eines usades per l'adquisició
- Eines usades per examinar
- Dades trobades en examinar
- Notes dels altres investigadors   

S'ha de fer una **presentació clara de la informació** per si ho ha de llegir un altre investigador o per un judici.
- Les troballes han de ser autoexplicatives i clares, reproduïbles (que algú altre pugui reproduir el procés seguint la documentació).
- Les línies de temps i els analitzadors d'enllaços poden ajudar a explicar les troballes relacionant diferents fonts i dispositius interconnectats.  

---

### 9. Arxivat

En molts casos cal **guardar les evidències** per intervals de temps més o menys llargs.
- Cal tenir present els sistemes d'arxivat preservant les proves.
- En un futur poden aparèixer noves tecnologies per analitzar les imatges binàries i revisitar el cas.

---

## Aproximacions pràctiques al forense de mòbils

### 1. Comprendre els diferents sistemes operatius de mòbils

Hi ha diferents sistemes operatius per a mòbils com ara:
- iOS
- Windows Phone (desaparegut avui dia)
- Android (el que treballarem)

#### Android

Sistema operatiu basat en **Linux** i que és la plataforma open-source de Google. Als fabricants de mòbils, en ser open-source els hi surt gratis.
- És el més amplament usat en mòbils al mon (iOS és el segon).
- La seva natura oberta fa que existeixin moltíssimes aplicacions que normalment es pugen a Google Play (market d'apps de Google).

---

### 2. Nivells d'aplicació de les eines d'adquisició

L'adquisició de les dades es pot fer aplicant tècniques a diferents nivells. Cada tècnica te els seus propis avantatges i inconvenients.

<img src="imgs/depuracio-adb-6abf506b.png" width="600" align="center" />

Cellular phone tool leveling pyramid (Sam Brothers, 2009)

---

Podem categoritzar les eines segons el mètode d'extracció que s'apliqui. Si pugem **des de la base de la piràmide cap amunt augmenta**:
- el nivell tècnic requerit del personal
- la sofisticació de les eines
- el temps necessari per a portar-les a terme
- el risc de destrucció de dades i dispositiu.

Si s'escull el mètode erroni, podem destruir totes les dades o el mateix dispositiu. Es requereix un bon entrenament en la tècnica utilitzada per evitar errors.

---

#### 2.1. Extracció manual

Aquest mètode s'ha de fer servir si:
- no tenim cap altre opció
- tenim pocs coneixements
- ha fallat una extracció física (binària) o lògica (de sistemes d'arxius, arxius, etc.).

Simplement farem observació de les dades que puguem veure de **forma visual**.
- Remenarem **directament la interfície** (teclat, pantalla tàctil) per buscar informació de forma directa.
- Documentarem les proves fent **fotografies**.
- Aquest procés és **ràpid i fàcil** i funciona en pràcticament tots els dispositius.
- Pot ser que **cometem errors** en fer-ho si no estem familiaritzats amb la interfície.
- No permet recuperar informació esborrada.
- Podem **destruir dades o simplement canviar-les sense adonar-nos** (per exemple, mirar un SMS el marca com a llegit).

Existeixen eines com "Project-A-Phone" per guiar l'examinador a documentar el procés. http://www.mobileforensicscentral.com/mfc/products/projecta.asp?pg=d&prid=267&pid=

---

#### 2.2. Extracció lògica

Es tracta de **connectar els dispositiu via cable USB, RJ45, infrarojos o Bluetooth**.
- Un cop connectat s'envien **comandes al dispositiu** per que els interpreti i torni les dades demanades a l'estació forense.
- Més tard es revisen les dades.
- **La gran majoria d'eines** estan a aquest nivell.
- És un procés ràpid i fàcil d'usar i requereix poc entrenament.
- Es pot escriure sense voler al dispositiu i canviar la seva integritat.
- Normalment **tampoc podem recuperar informació esborrada**.

---

#### 2.3. Extracció de bolcat binari (Hex Dump)

Es tracta d'una **extracció que es coneix com a física del contingut de la memòria en cru**.
- Es connecta el dispositiu a l'estació forense i s'insereix al dispositiu un **codi d'arrencada no signat** o bé un **bootloader**.
- Amb aquestes eines es fa el bolcat de memòria.
- Requereix **expertesa tècnica per analitzar la imatge binària**.
- El procès és poc costós, permet obtenir més informació i permet recuperar dades esborrades de l'espai no assignat.

---

#### 2.4. Extracció del chip (Chip-off)

Es tracta d'**extreure la informació directament del chip del dispositiu**.
- S'**extreu físicament la memòria** per posar-la a un lector de memòries o un altre telèfon.
- El procès és car, requereix coneixements del hardware i utilitzar eines per dessoldar i escalfar el xip.
- Cal un bon entrenament per a fer-ho correctament.
- Una mala execució pot fer malbé el xip de memòria.
- Només **es recomana si les opcions anteriors han fallat**, ja que és un procés destructiu.
- La informació que s'obté està en format cru i calen eines per decodificar-la i interpretar-la  
- Aquest mètode es prefereix en el cas que sigui molt important preservar l'estat de la memòria de forma exacta.
- És l'única alternativa si el dispositiu no funciona però el xip està intacte.  

També es pot fer servir el mètode **Joint Test Action Group (JTAG)** que permet llegir la memòria sense extreure-la.
- Es tracta de connectar als **Test Access Ports (TAPs)** del dispositiu per a forçar la transferència de dades en cru emmagatzemades al dispositiu.
- S'utilitza en casos en que **el dispositiu funciona** però no és accessible pels mètodes anteriors.

Aquestes dues tècniques també s'apliquen **quan el dispositiu té la pantalla en bloqueig**.

---

#### 2.5. Micro lectura

Es tracta de **llegir els senyals electròniques generades per les potes del chip**.
- És un procés laboriós, costós, que necessita molt de temps i molts coneixements tècnics.
- Només per a casos de molta importància, a nivell de seguretat nacional.
- Totes les eines són comercials.

---

## Evidències potencials als dispositius mòbils

Es poden trobar **evidències a diferents localitzacions**:
- targetes **SIM**
- targetes de memòria externa (**SD, microSD**)
- memòria **RAM**
- memòria **FLASH interna**
- **proveïdor de servei telefònic**
- **núvol**

En general es poden obtenir els següents artefactes, la majoria amb marques de temps:
- **Llibre d'adreces (Address book)**: Noms de contacte, número de telèfon, emails...
- **Històric de trucades (Call history)**: Trucades fetes, rebudes i perdudes així com la seva durada.
- **SMS**: Missatges de text enviats i rebuts.
- **MMS**: Arxius com fotos enviades i rebudes o fotos per MMS.
- **E-mail**: emails enviats, en esborrany i rebuts.
- **Històric del navegador Web**: pàgines web visitades.
- **Fotos**: Fetes pel mòbil, descarregades d'internet o transferides d'un altre dispositiu.
- **Vídeos**: Capturats amb la càmera, descarregats d'Internet, transferits d'altres dispositius.
- **Música**: Fitxers de música d'Internet o altres dispositius.
- **Documents**: Creats amb aplicacions del dispositiu, baixats d'Internet, o transferits.
- **Calendari**: Cites i tasques.
- **Comunicacions de xarxes**: Ubicacions GPS.
- **Mapes**: Llocs visitats, adreces que s'han cercat, mapes descarregats...
- **Dades de xarxes Socials**: Dades guardades per aplicacions com Facebook, Twitter, LinkedIn, WhatsApp, etc.
- **Dades esborrades**: informació esborrada del telèfon.

---

## Referències

- Curs SQLite forensics - https://www.teeltech.com/mobile-device-forensics-training/sqlite-forensics/

- Mobile Forensics Central - http://www.mobileforensicscentral.com/mfc/products/projecta.asp?pg=d&prid=267&pid=

- The mobile forensics process: steps and types - https://resources.infosecinstitute.com/topic/mobile-forensics-process-steps-types/
