# ADB: Android Debug Bridge


**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [ADB: Android Debug Bridge](#adb-android-debug-bridge)
	- [Instal·lar i utilitzar ADB](#installar-i-utilitzar-adb)
	- [Comandes ADB](#comandes-adb)
		- [Connexió i control](#connexi-i-control)
		- [Informació del sistema](#informaci-del-sistema)
		- [Enviar i rebre fitxers](#enviar-i-rebre-fitxers)
		- [Capturar pantalla](#capturar-pantalla)
		- [Simular botons](#simular-botons)
		- [APK's](#apks)
		- [Activar dades i localització](#activar-dades-i-localitzaci)
	- [Directoris d'Android amb dades](#directoris-dandroid-amb-dades)
	- [Mostrar la pantalla del mòbil al PC en viu](#mostrar-la-pantalla-del-mbil-al-pc-en-viu)

<!-- /TOC -->
---

- **Android Debug Bridge (adb)** és una eina de línia de comandes versàtil que permet comunicar-se amb un dispositiu Android. La comanda adb permet realitzar una gran varietat d'accions en el dispositiu, com instal·lar i depurar apps, i proporciona accés a un shell d'Unix que es pot usar per a executar diferents comandes en un dispositiu. https://developer.android.com/studio/command-line/adb?hl=es-419

També es pot executar adb des d'un navegador Chrome amb Webadb - https://webadb.com/

És un programa client-servidor que inclou tres components:

1. Un client, que envia comandes i s'executa a la màquina de desenvolupament (comanda adb).
2. Un daemon (adbd), que executa comandes al dispositiu Android.
3. Un servidor, que administra la comunicació entre el client i el daemon, que s'executa a la màquina de desenvolupament .

---

## Instal·lar i utilitzar ADB

1. Cal instal·lar l'Android Software Development Kit (SDK). Aquest conté l'IDE de desenvolupament d'apps Android, documentació, simuladors d'Android i altres eines útils per a la investigació. Si tenim prou espai farem servir aquesta opció (a Windows o millor a Linux, ja que Android fa servir Linux). https://developer.android.com/studio/index.html

- Alternativament es pot instal·lar només les "Platform tools", que formen part del SDK per ocupar menys espai. Inclouen per exemple ADB. https:/​/developer.android.com/studio/releases/platform-​tools

2. Si es vol connectar a un mòbil físic, cal activar abans:
- les opcions de desenvolupador (Configuración > Acerca del dispositivo > Número de compilación i picar a sobre 7 vegades)
- la depuració per USB Configuración > Sistema > Avanzado > Opciones para desarrolladores > Depuración por USB)

3. Caldrà també instal·lar els drivers USB pel mòbil utilitzat. Universal ADB Drivers - https://adb.clockworkmod.com/

4. Connectem el cable USB o configurem la connexió Wifi (Android 11 i superiors).

Detalls al manual de ADB: https://developer.android.com/studio/debug/dev-options?hl=es-419#enable

---

## Comandes ADB

Tenim control del mòbil des del pc. Podem executar moltes comandes, etre elles:

### Connexió i control

- `adb devices -l`: Per veure dispositius connectats

- `adb kill-server`: Detenir el servidor adb en cas de problemes (qualsevol comanda adb el torna a posar en marxa)

- `adb -s [NUMERO-DISP] shell comanda`: per executar comanda i sortir. Si no es posa comanda, es fa un shell interactiu.

- `adb shell`: Obtenir shell interactiva del dispositiu. Es podran escriure comandes del Linux de l'Android

~~~
$ adb shell
shell@ $
~~~

- `adb reboot`: Reiniciar dispositiu

- `adb reboot-bootloader`: Reiniciar dispositiu en mode fastboot

- `adb reboot recovery`: Reiniciar dispositiu en mode recuperació

### Informació del sistema

- `adb logcat` : Logs del sistema Android (seguiment de pila, errors del sistema i missatges que s'envien des d'una app amb la clase Log)

- `adb shell dumpsys`: Bolcar dades del sistema

- `adb shell dumpsys | grep "DUMP OF SERVICE servei"`: Bolcar dades del sistema a pantalla centrat en un servei

- `adb shell dumpsys [servei]`: Bolcar dades d'un servei específic

- `adb shell cat/proc/cpuinfo`: Mostrar informació sobre CPU

### Enviar i rebre fitxers

- `adb push [RUTA-LOCAL] [RUTA-dispositiu]`: Copiar un arxiu al dispositiu

- `adb pull [RUTA-dispositiu] [RUTA-LOCAL]`: Descarregar un arxiu des del dispositiu
  - Ex: `adb pull /data/misc/wifi/wpa_supplicant.conf ./info`  ja que conté les wifis i passwords de les wifis accedides

### Capturar pantalla

- `adb shell screencap [RUTA-dispositiu]`: Captura de pantalla

Ex: Capturar pantalles sense tocar la pantalla del mòbil (També es poden gravar vídeos de la pantalla):
~~~
$ adb shell
shell@ $ screencap /sdcard/screen.png
shell@ $ exit
$ adb pull /sdcard/screen.png
~~~

- `adb shell screenrecord -time-limit [SEGONS] [RUTA-dispositiu]`: Gravar pantalla del dispositiu

### Simular botons

- `adb shell input keyevent 26`: Emular botó encendre

- `adb shell input keyevent 82`: Emular pantalla de desbloqueig

### APK's

- `adb install [APK]`: Instal·lar APK

- `adb -r install [APK]`: Reinstal·lar APK (no esborra dades)

- `adb uninstall [NOMBRE-PAQUETE-APLICACION]`: Desinstal·lar APK

- `adb shell pm path [NOM-PAQUET]`: Extreure APK

Exemple d'ús: "Downgrade" d'aplicació instal·lada prèviament, mantenint les dades que contenia. La pactica de downgrade permet accedir a vulnerabilitats de versions anteriors de programari que actualment estiguin arreglades, per exemple, whatsapp. Així podrem extreure les dades.

https://www.xda-developers.com/downgrade-an-app-android-no-root/

~~~
adb push app.apk /sdcard/app.apk
// -r retenir dades -d downgrade
adb shell pm install -r -d /sdcard/app.apk
~~~

- `adb shell pm list packages`: Llistar paquets instal·lats

Ex: Llistar paquets instal·lats pel fabricant oneplus
~~~
pm list packages | grep 'oneplus'
~~~

- `adb shell pm uninstall -k --user 0 NameOfPackage`: Esborrar aplicació, útil per eliminar "Bloatware" (programari preinstal·lat pel fabricant).

~~~
adb shell pm uninstall -k --user 0 com.mi.android.globalFileexplorer  
~~~

https://www.xda-developers.com/uninstall-carrier-oem-bloatware-without-root-access/

- `adb shell am start -n [PAQUETE-APLICACION]/.[ACTIVITY]` `adb shell am start -n [PAQUET-APLICACIO]/[ACTIVITY]`: Iniciar una Activity (apk).

### Activar dades i localització

- `adb shell svc data enable`: Habilitar dades mòbils

- `adb shell svc data disable`: Deshabilitar dades mòbils

- `adb shell location`:  Localitzar dispositiu

---

## Directoris d'Android amb dades

Algunes carpetes del sistema Android amb informació interessant:

- **/data/data/com.android.email/*.db** que són arxius que es poden veure amb "sqlitebrowser" ja que és una BD SQLite.
- **/data/data/com.android.phone** (info del telèfon)
- **/data/data/com.android.providers.contact**
- **/data/data/com.whatsapp** (codificat amb db12 o crypt12). La clau estarà en /data/misc/keychain
- **/data/misc/wifi/wpa_supplicant.conf**: Dades de wifi
- **/data/data/com.android.providers.settings**: Dades sobre providers estàndar Android (entre ells wifi)

---

## Mostrar la pantalla del mòbil al PC en viu

Per poder veure la pantalla del nostre mòbil Android físic al monitor, per fer alguna demostració, podem utilitzar entre d'altres les aplicacions:

1. Scrcpy - via USB o sobre TCP/IP i Wifi (molt ràpida). No requereix Root - https://github.com/Genymobile/scrcpy
2. Screenstream
3. LetsView
4. ApowerMirror
5. Reflector 3
6. MirrorGO
7. TeamViewer
