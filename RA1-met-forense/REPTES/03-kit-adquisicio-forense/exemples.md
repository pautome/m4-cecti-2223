Forensic image acquisition tools
1. Tools for memory acquisition
  - Magnet RAM Capture
  - Belkasoft Live RAM Capturer
  - Linux Memory Extractor (LiME)
  - Acquire Volatile Memory for Linux (AVML) - https://github.com/microsoft/avml

2. Disk acquisition
  - Forensic Toolkit® (FTK) Imager
  - USB acquisition - https://www.osforensics.com/tools/write-usb-images.html

3. Network package acquisition
  - Wireshark

4. Artifact collectors
  - Kroll Artifact Parser and Extractor (KAPE): This tool collects different artifacts from a target system and processes the information using different integrated tools. You can run this tool from a USB drive because it doesn't need any installation:- https://www.kroll.com/en/services/cyber-risk/incident-response-litigation-support/kroll-artifact-parser-extractor-kape .
  - MAGNET Web Page Saver (WPS) - Magnet WPS supports Windows 7 or higher. You can download this tool from here: https://www.magnetforensics.com/resources/web-page-saver/
  - DFIR-O365RC This tool for Office 365 log collection works using PowerShell and PowerShell Core. You can get data from Azure Active Directory (AD) sign-in and audit logs and from Office 365 unified audit logs
