# Repte: Preparar kit d'intervenció Forense

## Temps
60 minuts

## Referències

- Getting Started with Bento Digital Forensics Toolkit - https://www.youtube.com/watch?v=PHZxF90sHuM
- Documentation BENTO toolkit - https://tsurugi-linux.org/documentation_bento_toolkit.php#
- Llistat d'eines - https://tsurugi-linux.org/documentation_bento_toolkit_tools_listing.php#

## Material i programari

- Pendrive USB
- OVA Windows 10 (imatge mínima) - https://www.scivision.dev/free-windows-virtual-machine-images/

## Objectiu

Preparar distribució per a l'adquisició. Necessitem una sèrie d'eines per a l'actuació forense.

Aquestes eines permeten obtenir imatges de:
  1. RAM
  2. Discos
  3. Xarxa
  4. Artefactes

## Exercici

**ATENCIÓ**: Pensat per a investigació en mode live i triatge, no per l'anàlisi o un anàlisi mínim per determinar què recollir.
Algunes utilitats es poden detectar com a nocives per alguns AntiVirus.

1. Descarregar Bento https://tsurugi-linux.org/bento.php
2. Formatar el disc USB amb FAT32 o preferiblement NTFS.
3. Copia els fitxers de Bento al disc USB.
4. Prepara les utilitats que no estiguin descarregades a Bento per motius de llicències. Segueix el vídeo.

## Preguntes de revisió

1. Quines utiltitats has hagut de descarregar i configurar per tenir el kit operatiu?
2. Pots iniciar-les des del menú de Bento?
3. Quines eines permeten fer adquisició d'evidències de dispositius d'emmagatzemament i quines de memòria RAM usant Linux i Windows?

\ | Linux  | Windows
--|---|--
RAM  |   |  
Disc  |   |   
