
# Repte: Anàlisi forense BSides Amman 2021

## Temps

2 a 3 hores (?)

## Material i programari

- Reptes de cyber5w - https://academy.cyber5w.com/courses/take/bsides-amman-2021-windows-forensics-workshop/quizzes/25867347-workshop-questions
- Imatge forense: https://archive.org/details/BSidesAmman21.E01

1. Eric Zimmerman's tools - https://ericzimmerman.github.io/#!index.md
   - Registry Explorer - https://ericzimmerman.github.io/#!index.md
2. Arsenal Recon - Digital Forensics Tools by Digital Forensics Experts - https://arsenalrecon.com/downloads/
3. 010 Editor: World's Best Hex Editor - https://www.sweetscape.com/010editor/
4. HxD - Freeware Hex Editor and Disk Editor - https://mh-nexus.de/en/hxd/
5. Nirsoft WinPrefetchView - https://www.nirsoft.net/utils/win_prefetch_view.html
6. Microsoft Virtual Machines - https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/
7. Windows Forensics Workshop @ BSides Amman 2021 - https://github.com/ashemery/WindowsDFIR
8. RegRipper - https://github.com/keydet89/RegRipper3.0
9. Autopsy - https://www.autopsy.com/download/
10. DCode™ – Timestamp Decoder - https://www.digital-detective.net/dcode/

---

## Objectiu

Farem servir diverses eines per analitzar una imatge de Windows i poder així respondre les preguntes guiades del cas.

---

## Repte

1. Registreu-vos a https://academy.cyber5w.com/. Aquí trobareu cursos sobre forense digital, gratuïts i de pagament.
2. La imatge de disc que ens proporcionen pertany a un equip que s'ha utilitzat per alguna activitat il·legal. Algun usuari ha accedit a arxius als que en teoria no hauria d'haver accedit. El sistema te dos usuaris que són els principals sospitosos. Cal obtenir les respostes a les preguntes guiades i proporcionar les evidències detallades i amb captures adequades. Accediu a: https://academy.cyber5w.com/courses/take/bsides-amman-2021-windows-forensics-workshop/quizzes/25867347-workshop-questions

---

## Preguntes de revisió (entrega)

1. Afegir l'informe forense del cas. Tingueu present omplir el full d'adquisicions.
2. Captura les respostes que introdueixes al lloc web del cas.
3. Indiqueu el temps REAL que heu dedicat a fer el repte.

---

- Vídeo explicatiu: https://www.youtube.com/watch?v=9DafPi5fFUQ
