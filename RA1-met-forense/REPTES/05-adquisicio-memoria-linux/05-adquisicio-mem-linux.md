# Repte: Adquisició d'imatge de memòria en Linux amb Lime

## Temps
60 minuts

## Referències

- Repositori LiME (ajuda al README i la carpeta doc) - https://github.com/504ensicsLabs/LiME.git
- https://medium.com/@ozan.unal/linux-forensics-series-chapter-1-memory-forensics-372b9dba49d7

## Material i programari

- Màquina virtual Ubuntu Desktop 20.04
- Màquina forense, per exemple un clon de l'Ubuntu Desktop

## Objectius

Seguint l'ordre de volatilitat, procedirem a adquirir la memòria RAM de l'equip Linux amb LiME (Linux Memory Extractor). Més endavant farem l'anàlisi de la imatge obtinguda amb Volatility Framework.
Aquesta eina també permet extreure imatges de memòria de sistemes Android, que són també Linux.

## Repte

**NOTA**: Cal tenir en compte que aquesta adquisició modifica l'estat de la memòria (cal instal·lar un mòdul del Kernel i executar comandes) i per tant **cal documentar-ho adequadament**, així com les comandes que executem a l'equip destí, **per descartar les operacions en temps d'anàlisi**.

1. Comprovar amb "uname -r" i "cat /etc/os-release" la distribució i versió de Linux de la màquina destí o víctima.

2. En un equip de laboratori amb la distribució i versió de Linux de la màquina destí (comprovar amb "uname -r" i "/etc/os-release"), clonar el repositori de LiME, entrar al directori src i executar la comanda make. Crearem el mòdul de LiME que copiarem al volum forense extern. Potser cal instal·lar el compilador i el make. A efectes de prova, ho farem a un equip clon de la màquina destí o víctima.

3. Carregarem el volum forense a la màquina destí o víctima i instal·larem el mòdul LiME al Kernel. Començarà a fer-se el bolcat de RAM al dispositiu. Consulteu opcions al manual de LiME (https://github.com/504ensicsLabs/LiME#usage). L'ús bàsic és aquest (ruta del fitxer destí, format de la imatge i hash):

~~~
$ sudo insmod lime-<kernel>.ko "path=ruta-dispositiu-extern/ram.lime format=lime digest=md5"
~~~

4. Comproveu si el mòdul encara hi és (lsmod) i si cal treure'l (rmmod).

5. Imaginem que el dispositiu de destí no és prou gran pel bolcat de RAM. Llavors podem enviar-ho a l'estació forense posant "tcp:port" a la ruta del path (això crearà un servidor escoltant a l'equip víctima). També podem calcular i enviar el hash.

Caldrà des de l'equip forense connectar-se amb netcat a la ip:port de la màquina destí o víctima dues vegades: La primera per rebre la imatge (enviar la sortida a un fitxer ram.lime) i la segona per rebre el hash (enviar la sortida a un fitxer ram.lime).  

6. Per estalviar temps en l'enviament de la imatge podem utilitzar l'opció de compressió del LiME. Aquesta opció comprimeix la imatge, tenint en compte que el hash es calcula sobre la imatge sense comprimir, si posem l'opció hash. Cal comprovar que el fitxer s'ha rebut correctament i el hash és correcte.
~~~
...a l'equip forense (rebre i descomprimir)
$ nc ip 4444 | unpigz > ram.lime
~~~

7. Feu que l'equip físic sigui l'estació forense i envieu-li la imatge de memòria RAM. Comproveu el hash.
