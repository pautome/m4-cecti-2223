# Repte: Usar FTK Imager

## Temps
30 minuts

## Material i programari

- OVA Windows 10 (imatge mínima) - https://www.scivision.dev/free-windows-virtual-machine-images/
- FTK Imager Windows
  - Amb registre - https://accessdata.com/product-download/ftk-imager-version-4-5
  - Directe - https://go.exterro.com/l/43312/a-FTK-Imager-4-5-0-28x6429-exe/f7zhh8
  - Linux Ubuntu Debian línia de comandes - https://accessdata.com/product-download/debian-and-ubuntu-x64-3-1-1

- USB petit amb fitxers i fitxers esborrats

## Objectiu

Fer una còpia bit a bit de tots els sectors d'un disc USB amb una eina com FTK Imager.

**NOTA**: FTK Imager no garanteix que no s'escrigui al disc mentre s'està fent la imatge. Per aquesta raó, cal fer servir un blocador d'escriptura en casos reals. En aquesta activitat assumirem que tenim un blocador d'escriptura USB.

## Exercici

1. Engegar FTK imager i inserir el disc USB.
2. Crear una imatge del disc USB en format cru (Raw dd) i guardar la còpia a una carpeta compartida amb la màquina física.
3. Explorar altres característiques de FTK Imager.

## Preguntes de revisió

1. Després de la còpia, quins fitxers ha creat FTK Imager? Selecciona totes les opcions correctes.
- Fitxer Imatge amb extensió .001
- Fitxers individuals com al disc original
- Fitxer de text amb el sumari de la imatge
- Fitxer Imatge amb extensió .ex001

2. Quants algoritmes hash usa FTK imager per verificar que la imatge no ha sigut alterada?
- Un algoritmes hash
- Dos algoritmes hash
- Tres algoritmes hash
- Quatre algoritmes hash

3. L'opció "Verify images after they are created" està activada per defecte i el seu efecte és:
- FTK imager calcula el hash de la imatge
- FTK imager calcula el hash del disc USB
- FTK imager calcula el hash MD5 i SHA1 del disc USB i de la imatge i comprova que coincideixen.
- FTK imager calcula el hash MD5 del disc USB i el hash MD5 de la imatge, i comprova que coincideixen

4. Un cop carregada la imatge dd en FTK Imager, es veuen els arxius esborrats?
- Si
- No

5. FTK Imager pot fer captures de memòria RAM?
- Cert
- Fals

6. Quins altres formats d'imatge de disc es poden generar amb FTK IMager? Descriu quines diferències hi ha amb la còpia dd "crua" (RAW).
