# Repte:  Crear hash en sistemes *nix

## Temps
30 minuts

## Referències

La comanda "stat fitxer" ens dona informació de marques de temps de fitxers *nix. Són 4.

~~~
Time fields meaning:

ctime: file change time.
atime: file access time.
mtime: file modification time.
crtime: file creation time.

Fitxer: prova
     Mida: 292       	Blocs: 8          Bloc d’E/S: 4096   fitxer ordinari
Dispositiu: 833h/2099d	Node‐i: 5274828     Enllaços: 1
Accés: (0664/-rw-rw-r--)  UID: ( 1000/     pep)   GID: ( 1000/     pep)
Accés: 2004-01-01 16:21:42.000000000 +0100
Modificació: 2004-01-01 16:21:42.000000000 +0100
    Canvi: 2022-09-09 12:56:08.763623260 +0200
Naixement: -
~~~

## Material i programari

- Windows: hashcalc (www.slavasoft.com/hashcalc/)
- Linux/Unix: md5sum i shasum (sha256sum, sha1sum, sha512sum...) / gtkhash en entorn gràfic

## Objectius

- Fer operacions de hash en arxius i comprovar com afecten els canvis als hashs de fitxers.

## Exercici

1. Crea o selecciona un fitxer de text que contingui informació.
2. Calcula i registra el hash del fitxer (eines "xxxsum" com per exemple md5sum) i els valors de "stat" per comparar després.
3. Busca i explica quina diferència hi ha entre accès, modificació, canvi i naixement.
4. Fes els següents canvis i comprova si canvia el **valor de hash** i **algun camp de stat** (compara amb els inicials):
  - Canvia un caràcter.
  - Copia el fitxer amb cp i comprova el hash i els valors de stat.
  - Canvia el nom del fitxer amb "mv fitxer.txt noufitxer.txt".
  - Canvia els drets d'accés (amb chmod), per exemple "chmod g+w fitxer.txt".
  - Canvia la data de modificació amb touch a "1-1-2000 22:55" (mirar ajuda).
  - Crea un enllaç simbòlic al fitxer, i calcula el hash i stat de l'enllaç simbòlic, per exemple "ln –s fitxer.txt"

---

## Preguntes

1. Quins canvis fan que el hash es modifiqui?
2. Quina explicació li dones a que hi hagi canvis que modifiquen el hash i d'altres que no?
3. Quina utilitat li trobes al hash?
4. Com pots detectar que un alumne ha falsificat la data d'un arxiu per mirar de lliurar la pràctica més tard?
5. Es pot falsificar de forma adequada l'arxiu per que no sospitem de l'alumne?
