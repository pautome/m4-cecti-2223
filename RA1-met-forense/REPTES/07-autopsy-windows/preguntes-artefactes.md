# Preguntes

1. On es poden trobar els fitxers de registre o hives?
2. Com obtenim evidències de l'execució de programes per part de l'usuari?
3. Quina carpeta conté el perfil de l'usuari? Quin és l'arxiu del hive de l'usuari?
4. Quins hives guarden informació sobre configuració de l'equip?
5. Què és un thumbcache?
6. Que indica l'extensió .LOG en un arxiu de hive?
7. On es guarden les configuracions d'aplicacions de cada usuari?
8. Quins hives guarden informació sobre configuracions dels perfils dels usuaris?
9. Per què necessitem establir la zona horària de l'equip?
10. Com obtenim informació sobre la configuració de les interfícies de xarxa?
11. Qué podem trobar a un fitxer d'accés directe (LNK)?
12. Quins hives guarden informació sobre comptes d'usuaris?
13. Què és un perfil mòbil d'usuari?
14. Què són els hives?
15. Com podem saber quants inicis de sessió ha realitzat un usuari?
16. Quin hive del registre permet trobar informació sobre els artefactes relatius a dispositius USB?
17. Quins hives guarden informació sobre perfil de maquinari usat a l'equip local?
18. Com podem obtenir evidències de la interacció dels usuaris amb dispositius de xarxa o externs?
19. En examinar els fitxers de logs, quin ID d'esdeveniment identifica un login amb èxit?
20. Com podem relacionar un arxiu al disc amb un usuari?
21. Què vol dir MRU? Com ens pot ajudar a obtenir informació?
22. Com podem trobar evidències d'arxius esborrats?
23. Què és SRUM? Per a què pot servir?
24. Què són les Jump Lists? Per a què pot servir?
25. Què és prefetch? Per a què pot servir?
