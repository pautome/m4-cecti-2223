# Repte: Anàlisi forense de captura de disc Windows amb Autopsy i anàlisi inicial del registre de Windows

## Temps

120 minuts aprox.

## Material i programari

- Reptes de TryHackme - https://tryhackme.com/
- Autopsy
- Registry Explorer

## Objectiu

- Farem servir Autopsy per analitzar una captura de disc d'equips Windows indeterminats i aprendre com fer servir l'eina. També farem servir altres eines per a analitzar triatges de Windows.

- Caldrà anar fent l'informe forense amb tots els detalls de l'anàlisi. Seguiu el model d'informe que us he proporcionat amb els apunts de metodologia forense.

## Exercici

1. Registrar un compte gratuït a la plataforma Tryhackme: https://tryhackme.com/

2. Resoldre la sala de Tryhackme "Autopsy": https://tryhackme.com/room/autopsy2ze0
3. Resoldre la sala de Tryhackme "Windows Forensics 1": https://tryhackme.com/room/windowsforensics1

## Preguntes de revisió (entrega)

1. Afegir les captures dels reptes solucionats del vostre compte amb TryHackme (miraré que tingueu el compte i hagueu solucionat els casos).

2. Indiqueu el temps REAL que heu dedicat a fer el repte.
