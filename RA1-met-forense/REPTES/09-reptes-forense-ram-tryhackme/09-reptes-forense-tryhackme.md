# Repte: Anàlisi forense de captura de RAM de Windows amb Volatility

## Temps

180 minuts aprox.

## Material i programari

- Reptes de TryHackme - https://tryhackme.com/
- Referència de comandes per imatges Windows de Volatility - https://github.com/volatilityfoundation/volatility/wiki/Command-Reference
- Laboratori de mostra solucionat amb Volatility - https://github.com/stuxnet999/MemLabs/tree/master/Lab%200

## Objectiu

- Farem servir Volatility per analitzar captures de RAM d'equips Windows indeterminats i aprendre quines comandes podem fer servir.

## Exercici

1. Registrar un compte gratuït a la plataforma Tryhackme: https://tryhackme.com/

2. Resoldre la sala de Tryhackme "Volatility": https://tryhackme.com/room/bpvolatility

3. Resoldre la sala de Tryhackme "Memory Forensics": https://tryhackme.com/room/memoryforensics

4. Resoldre la sala de Tryhackme Forensics: https://tryhackme.com/room/forensics

## Preguntes de revisió (entrega)

1. Afegir les captures dels reptes solucionats del vostre compte amb TryHackme (miraré que tingueu el compte i hagueu solucionat els casos).

2. Indiqueu el temps REAL que heu dedicat a fer el repte.
