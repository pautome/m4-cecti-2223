# Repte: Usar Autopsy per analitzar una imatge amb sistema d'arxius Linux

## Temps

60 minuts

## Material i programari

- Imatge de disc Linux_Financial_Case
- Autopsy per a Windows - https://www.sleuthkit.org/autopsy/download.php
- Autopsy User Guide - http://sleuthkit.org/autopsy/docs/user-docs/4.3/

## Objectiu

Extreure informació de la imatge de disc proporcionada amb Autopsy, un front-end gràfic per a Sleuthkit.

## Exercici

Escenari: Mark Watson és el director de finances en una agència d'anuncis, Stickideas. Se l'ha acusat de robar l'informe anual de finances (Earnings.xls) i donar-li a un contractista anomenat Frank Lewis per influir en la seva contractació. Mark nega qualsevol implicació en l'afer, com no.   

L'administrador IT t'informa que existeix un servidor Linux a l'oficina on es guarden els documents oficials. Tant Mark com Frank tenen una carpeta pròpia en el servidor.

Disposes de la imatge del disc dur del servidor per buscar l'evidència de que Frank disposa de permís o no per accedir a l'informe indicat.

1. Extreu la imatge, comprova de quin tipus és i comprova el seu hash.
 - MD5 (Linux Financial Case.001) = 7b39de0ca146c89ad73d1d421c8f7a05
 - SHA1 (Linux Financial Case.001) = c7b06f006ff79711e692bd2620aba4cc2a4426d2

2. Inicia Autopsy i crea un nou cas. Indica una carpeta a l'escriptori per guardar el cas, que serà el número 1 del nostre historial.

3. Selecciona el tipus de dades d'origen com a imatge o VM. Busca el fitxer proporcionat. Comprova que el hash que calcula Autopsy correspon al de la imatge.

4. Selecciona la zona horària local. NOTA: Cal prendre nota sempre en adquirir una imatge de l'hora i la zona horària de la màquina, especialment si volem poder correlacionar evidències de diferents zones horàries. Mai es pot assumir que l'hora i zona horària de la màquina és correcta

5. Selecciona tots els mòduls "Ingest". NOTA: els mòduls "Ingest" analitzen les dades de l'origen de dades sobre tots els fitxers i comprova el seu contingut o realitzant diverses operacions.

NOTA: Un cop creat el cas, es podrà reobrir escollint "Obrir cas existent" i seleccionant el cas *.aut corresponent.

6. Tanca el cas i torna'l a obrir per comprovar-ho.

7. Explora els continguts de la imatge per respondre les preguntes del cas. Autopsy mostra tot el que ha trobat en un arbre amb diferents criteris d'ordenació i indicant el número de ítems trobats entre parèntesi.

8. Crea un breu informe forense utiltizant totes les dades i metadades que puguis obtenir de la imatge, amb les teves conclussions. Utilitza el model d'informe forense dels apunts i crea el teu document plantilla que faràs servir en tots els informes futurs.  

---

## Preguntes de revisió

- Marca i justifica les respostes explicant que fas.

1. Quin és el número de inode de Earning.xls? Quin és el bloc de dades que té el contingut de Earning.xls (Pista: Metadata )
- 23041, 43
- 46082, 24
- 46082, 197122

2. Quan es va modificar per darrera vegada Earning.xls?
- 2015-11-13 12:44:28 EST
- 2015-11-12 22:40:53 EST
- 2015-11-15 22:34:28 EST
- 2015-11-15 22:48:32 EST

3. Quins són els ID d'usuari i grup de Earning.xls al directori de Mark, Finance_Confidential?
- 1001, 1001
- 2002, 2002
- 1000, 1000

4. Quins són els ID d'usuari i grup dels fitxers del directori de Frank? És diferent dels de Earning.xls del directori de Mark?
- 2002, 2002, Si
- 1001, 1001, Si
- 100, 100, No

5. Quins drets tenen 'others' al directori de Mark i Finance_Confidential?
- read, write, execute
- read, write
- write, execute
- read, execute

6. Quins drets tenen 'others' al fitxer Earning.xls? Significa que Frank pot llegir l'arxiu?
- Read, Si
- Read, No
- Write, Si
- Write, No

7. Hi ha algun arxiu esborrat al directori de Frank que pogués ser un soft-link a Earning.xls? Pista: El primer caràcter de la columna "Mode" és una 'l' i els arxius esborrats es maquen amb una creu vermella.
- Frank > appointments4
- Frank > documents > work.xls
- Frank > bin.xls

8. Com es pot canviar els drets sobre l'arxiu 'Earning.xls' per a que no sigui accessible per Frank? (Selecciona dos mètodes)
- Treure el dret read de Earning.xls a 'others'
- Treure el dret execute de Finance_Confidential (directori pare de Earning.xls) a ‘others’
- Treure el dret read de Finance_Confidential a ‘others’
