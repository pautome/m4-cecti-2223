1. Aplica metodologies d'anàlisi forense caracteritzant les fases de preservació, adquisició, anàlisi i documentació.

- Criteris d'avaluació
  1. Identifica els dispositius a analitzar per garantir la preservació d’evidències.
  2. Utilitza els mecanismes i les eines adequades per a l'adquisició i extracció de les evidències.
  3. Assegura l'escena i conserva la cadena de custòdia.
  4. Documenta el procés realitzat de manera metòdica.
  5. Considera la línia temporal de les evidències.
  6. Elabora un informe de conclusions a nivell tècnic i executiu.
  7. Presenta i exposa les conclusions de l'anàlisi forense realitzat.

  **Continguts**

1. Aplicació de metodologies d'anàlisi forenses:
  1. Identificació dels dispositius a analitzar.
  2. Recol·lecció d'evidències (treballar un escenari).
  3. Anàlisi de la línia de temps (TimeStamp).
  4. Anàlisi de volatilitat - Extracció d'informació (volatility).
  5. Anàlisi de Logs, eines més usades.
