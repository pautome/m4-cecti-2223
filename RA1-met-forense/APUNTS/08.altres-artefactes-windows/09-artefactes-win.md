# Artefactes de Windows

**INS Carles Vallbona**

**Pau Tomé**

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Artefactes de Windows](#artefactes-de-windows)
	- [Eines d'adquisició del registre](#eines-dadquisici-del-registre)
		- [KAPE](#kape)
		- [Autopsy](#autopsy)
		- [FTK Imager](#ftk-imager)
	- [Eines d'anàlisi del registre](#eines-danlisi-del-registre)
		- [Registry viewer (empresa Access Data, propietari)](#registry-viewer-empresa-access-data-propietari)
		- [Registry Explorer (Eric Zimmerman)](#registry-explorer-eric-zimmerman)
		- [RegRipper](#regripper)
	- [Recollida d'informació dels Comptes d'usuari](#recollida-dinformaci-dels-comptes-dusuari)
		- [Identificador de comptes d'usuari](#identificador-de-comptes-dusuari)
	- [El registre d'esdeveniments (Event log)](#el-registre-desdeveniments-event-log)
		- [Exemple d'ús](#exemple-ds)
	- [Extreure informació de fitxers existents](#extreure-informaci-de-fitxers-existents)
		- [Thumbcache (cache de miniatures d'imatges)](#thumbcache-cache-de-miniatures-dimatges)
		- [MRU (Most Recently Used)](#mru-most-recently-used)
		- [Arxius d'Office recents](#arxius-doffice-recents)
		- [Obrir/Guardar i diàleg d'arxius recentment usats](#obrirguardar-i-dileg-darxius-recentment-usats)
		- [Barres d'adreces/cerca al Windows Explorer](#barres-dadrecescerca-al-windows-explorer)
		- [Paperera de reciclatge (Recicle Bin)](#paperera-de-reciclatge-recicle-bin)
		- [Accessos directes (shortcut) LNK](#accessos-directes-shortcut-lnk)
		- [Shellbags](#shellbags)
	- [Obtenir Informació del sistema](#obtenir-informaci-del-sistema)
		- [Versió del sistema](#versi-del-sistema)
		- [Current Control Set](#current-control-set)
		- [Nom de l'ordinador](#nom-de-lordinador)
		- [Time Zone](#time-zone)
		- [Interfícies de xarxa](#interfcies-de-xarxa)
		- [Programes "Autorun"](#programes-autorun)
	- [Evidència d'execució](#evidncia-dexecuci)
		- [UserAssist](#userassist)
		- [Prefetch](#prefetch)
		- [ShimCache](#shimcache)
		- [AmCache](#amcache)
		- [BAM/DAM](#bamdam)
	- [Informació sobre dispositius externs i dispositius USB](#informaci-sobre-dispositius-externs-i-dispositius-usb)
		- [Identificació de dispositius](#identificaci-de-dispositius)
		- [Primera/Darrera vegada de connexió](#primeradarrera-vegada-de-connexi)
		- [Nom de volum del dispositiu USB](#nom-de-volum-del-dispositiu-usb)
	- [Eines i referències](#eines-i-referncies)

<!-- /TOC -->

---

**Atenció:** Com que els sistemes operatius canvien o s'actualitzen, els artefactes poden moure's o ser eliminats. Cal estar actualitzat sobre els possibles canvis.

---

## Eines d'adquisició del registre

Existeixen moltes eines per a fer adquisició del registre. Alguns exemples:

---

### KAPE

Eina d'adquisició i anàlisi de dades en viu que es pot utilitzar per adquirir el registre.
- Eina de entorn de comandes
- Disposa d'una interfície gràfica.

https://www.kroll.com/en/insights/publications/cyber/kroll-artifact-parser-extractor-kape

---

### Autopsy

Dona l'opció d'adquirir dades dels sistemes en viu o d'una imatge de disc.
- Després d'afegir la font de dades, cal anar a la ubicació dels fitxers que es vol extreure, fer clic dret i seleccionar l'opció "Extreure".

---

### FTK Imager

Eina similar a Autopsy.
- Permet extreure fitxers d'una imatge de disc o d'un sistema viu muntant la imatge del disc o la unitat del sistema a FTK Imager i usant l'opció "Exporta".
- També pot extreure els fitxers de registre a través de l'opció "Obtain Protected Files", per sistemes en viu.
- Permet extreure tots els "hives" del registre a disc.
- No copiarà l'arxiu "Amcache.hve" (evidència de programes que van ser executats per última vegada).

https://accessdata.com/product-download/ftk-imager-version-4-5

---

## Eines d'anàlisi del registre

També tenim moltes eines per a fer anàlisi del registre. Alguns exemples:

### Registry viewer (empresa Access Data, propietari)

Amb interfície d'usuari similar a l'Editor de Registres de Windows. Però hi ha un parell de limitacions:
- Només carrega un hive a la vegada
- No te en compte els registres de logs de les transaccions (últims canvis no fets).

https://accessdata.com/product-download/registry-viewer-2-0-0

---

### Registry Explorer (Eric Zimmerman)

Eric Zimmerman ha desenvolupat un grapat d'eines (https://ericzimmerman.github.io/#!index.md) que són molt útils per realitzar Forenses Digitals i Resposta davant d'Incidents.

![](imgs/09-artefactes-win-c28711dc.png)

Registry Explorer pot carregar múltiples "hives" simultàniament.
- Afegeix les dades dels logs de transacció al "hive" per actualitzar-ho.
- Té l'opció de "dreceres" a claus de registre importants interessants per a investigadors forenses. Es pot anar directament a aquestes claus de registre i valors amb l'element de menú "bookmarks".

Si hi ha problemes amb els plugins:

![](imgs/09-artefactes-win-000f4789.png)


---

Nota: Quan es carreguin els hives de registre al **RegistryExplorer**, ens advertirà que els hives  estan "bruts" (no actualitzats). Això vol dir que cal usar els registres de transaccions i indicar a RegistryExplorer els fitxers .LOG1 i .LOG2 amb el mateix nom de fitxer que el hive del registre, per integrar automàticament els registres de les transaccions i crear un hive "net". Una vegada li diem a RegistryExplorer on salvar el hive net, podem utilitzar-ho per a la nostra anàlisi i ja no necessitarem carregar els hives "bruts". RegistryExplorer ens guiarà a través d'aquest procés.

---

### RegRipper

Aquesta utilitat pren un "hive" com a entrada i emet un informe amb dades d'algunes de les claus i valors d'importància forense en aquest hive.
- L'informe de sortida és un fitxer de text i mostra tots els resultats en ordre seqüencial.
- També disposa de GUI.
- RegRipper no té en compte els registres de logs de transaccions. Hem d'utilitzar per exemple "Registry Explorer" per a fusionar els logs de transaccions amb els respectius "hives" del registre abans d'enviar la sortida a RegRipper per a un resultat més precís.

- Descàrrega - https://github.com/keydet89/RegRipper3.0

- Articles per aprendre amb exemples per utilitzar aquesta eina a:
  - https://www.hackingarticles.in/forensic-investigation-windows-registry-analysis/
  - https://resources.infosecinstitute.com/topic/registry-forensics-regripper-command-line-linux/
  - https://resources.infosecinstitute.com/topic/windows-registry-analysis-regripper-hands-case-study-2/
- Un altre manual en castellà - https://fwhibbit.es/registro-de-windows-prepara-la-cafetera

Contenidor Docker: https://github.com/phocean/dockerfile-regripper

~~~
FROM ubuntu:bionic
LABEL maintainer "jc@phocean.net"

ENV PERL5LIB /regripper

RUN apt-get update \
  && apt-get install -y perl git libparse-win32registry-perl \
  && adduser --disabled-login --system --no-create-home regripper \
  && git clone https://github.com/keydet89/RegRipper3.0.git /regripper \
  && chown -R regripper /regripper \
  && rm -rf /regripper/.git \
  && apt-get -y remove --purge git \
  && apt-get -y autoremove \
  && apt-get -y clean \
  && rm -rf /var/lib/apt/lists/*

VOLUME /hive

USER regripper

WORKDIR /regripper

ENTRYPOINT ["perl", "rip.pl"]
~~~

---

## Recollida d'informació dels Comptes d'usuari

Es pot obtenir informació del registre per a determinar l'últim inici de sessió o l'últim canvi de password. Aquesta informació es pot trobar a:
- **C:\windows\system32\config\ SAM\Domains\Account\Users**

La informació continguda aquí inclou:
- l'identificador relatiu (RID) de l'usuari
- nombre de vegades que l'usuari ha iniciat la sessió
- l'últim inici de sessió
- l'últim inici de sessió fallit
- l'últim canvi de contrasenya
- l'expiració de la contrasenya
- la política de contrasenyes
- la pista per recordar la contrasenya (hint)
- els grups del quals forma part l'usuari.

Podem moure'ns pel registre amb **Registry Explorer** d'Eric Zimmerman, prèvia exportació del "hive".

També podem fer ús de RegRipper (https://github.com/keydet89/RegRipper3.0 )  per extreure informació.

---

### Identificador de comptes d'usuari

**SID**: és l'identificador de seguretat utilitzat pel sistema operatiu Windows per identificar objectes del sistema a nivell intern (no només comptes d'usuari).
- Al final del SID tenim un **identificador relatiu (RID)**, que és l'últim número després del SID. Per exemple, el compte d'administrador te el RID 500. El compte convidat té un RID de 501.

En aquesta captura, es mostra el RID 1001, que indica que és un compte creat a posteriori per un administrador o usuari i no
un compte creat pel sistema mitjançant un procés automatitzat:

![](imgs/artefactes-win-5a758352.png)

Quan fem l'examen, normalment mirem el RID.
- Es pot associar un RID amb un compte d'usuari específic. A mesura que es creen comptes d'usuari, el RID augmentarà en una unitat.
- Si per exemple tenim un usuari amb RID 1005, i no podem trobar comptes de 1001 a 1004, és possible que
algú o alguna cosa hagi eliminat aquests comptes d'usuari.
- Buscarem al registre per trobar artefactes que permetin corroborar o no les nostres hipòtesis sobre el que va passar.

---

## El registre d'esdeveniments (Event log)

Una altra font d'informació per ajudar a determinar què ha passat al sistema són els registres d'esdeveniments.

Podem trobar els registres d'esdeveniments a la ruta:
- **C:\Windows\System32\winevt\logs**.

![](imgs/artefactes-win-ff0700eb.png)

---

Windows categoritza els esdeveniments en tres classes diferents:
- **Sistema**: Informació generada pel sistema operatiu Windows.
- **Aplicació**: Informació generada per aplicacions a la màquina local.
- **Seguretat**: Informació relacionada amb els intents d'inici de sessió.

Els esdeveniments de Windows es referencien per números, no per cadenes i cal buscar en alguna taula de referència els esdeveniments rellevants. Podem consultar una llista completa dels esdeveniments de Windows a https://www.ultimatewindowssecurity.com/securitylog/encyclopedia/default.aspx

També teniu la referencia d'events del sistema windows (document word adjunt) - Windows 10 and Windows Server 2016 security auditing and monitoring reference - https://www.microsoft.com/en-us/download/details.aspx?id=52630

Referències:

- Microsoft-eventlog-mindmap (2022) - https://github.com/mdecrevoisier/Microsoft-eventlog-mindmap

- Fantastic Windows Logon types and Where to Find Credentials in Them - https://www.alteredsecurity.com/post/fantastic-windows-logon-types-and-where-to-find-credentials-in-them

- GitHub - sbousseaden/EVTX-ATTACK-SAMPLES: Windows Events Attack Samples - https://github.com/sbousseaden/EVTX-ATTACK-SAMPLES

- Documents Applied Incident Response (2022) - https://www.appliedincidentresponse.com/resources/

---

### Exemple d'ús

Una excusa comuna dels usuaris que podrien ser acusats d'un ús delictiu o inadequat del sistema és que una altra persona tenia accés al seu sistema de forma remota.
- El **protocol d'escriptori remot (RDP)** és una manera d'accedir a un amfitrió des d'una altra ubicació.
- El **registre de seguretat** contindrà una entrada de qualsevol accés que s'hagi fet utilitzant el protocol RDP (des d'un altre equip).
- Els **esdeveniments 4778 i 4779**, mostren quan el servei es connecta/reconnecta o desconnecta.
- També es podria **cercar el tipus d'inici de sessió al sistema**. Quan examinem l'**esdeveniment 4624**, tindrem informació de:
  - dia i hora
  - el nom d'usuari
  - els mitjans amb els quals s'ha fet un inici de sessió amb èxit.
  - Tipus d'inici de sessió

Podem veure quin numero correspon a cada tipus d'inici de sessió a https://www.ultimatewindowssecurity.com/securitylog/encyclopedia/event.aspx?eventid=4624

![](imgs/artefactes-win-9c3850de.png)

Com es pot veure a la següent captura de pantalla del visualitzador d'esdeveniments, es pot utilitzar aquesta aplicació per revisar els fitxers de registre exportats (Action->Open Saved Log). Un cop s'ha carregat el fitxer de registre seleccionat que es vol examinar, es pot filtrar els resultats per mostrar només els esdeveniments que siguin rellevants per la recerca.

![](imgs/artefactes-win-99c89cab.png)

Els **esdeveniments que poden aportar pistes** respecte com es pot **comprometre un compte d'usuari** són:
- 4624
- 4625
- 4634 | 4647
- 4648
- 4672
- 4720

Si es troben molts inicis de sessió fallits o s'ha concedit drets d'administrador a un usuari que normalment no els te, es pot buscar en aquests esdeveniments per mirar de trobar que ha passat.

---

## Extreure informació de fitxers existents

Alguns incidents poden referir-se a imatges il·legals, robatori de dades, accés il·legal a dades, etc. En aquests casos és interessant **saber si l'usuari coneixia l'existència dels arxius en qüestió o si els arxius existeixen a l'equip de l'usuari**.

Alguns artefactes de Windows poden donar informació sobre aquests aspectes.

### Thumbcache (cache de miniatures d'imatges)

És una **base de dades de imatges en miniatura creades quan un usuari usa l'Explorador de fitxers de Windows**. Depenent de la mida de les miniatures podem tenir múltiple BD amb la mateixa imatge però amb diferents mides.
Però **que existeixi la miniatura no prova que l'usuari conegui l'existència de la imatge original**, ja que es poden crear miniatures de forma automàtica, sense saber-ho l'usuari.  

La "thumbcache" es troba al perfil de l'usuari:
- **AppData\Local\Microsoft\Windows\Explorer**

Hi ha eines open source com **Thumbcache Viewer** (https://thumbcacheviewer.github.io/).

![](imgs/artefactes-win-b871225c.png)

El nom de la miniatura no és el de la imatge original i per relacionar-les cal fer ús de la la base de dades d'indexació de Windows (**Windows Search Indexing database**) que es troba a:
- **C:\ProgramData\Microsoft\Search\Data\Applications\Windows\Windows.edb**

Es pot fer servir l'eina **ESEDatabaseView** (https://www.nirsoft.net/utils/ese_database_view.html ).

El nom d'un "thumbnail" (miniatura) és de tipus caràcters hexadecimals com ara 96 5a be bc cc 2b f2 27. Cal invertir l'ordre abans de buscar a la base de dades, i quedaria 27 f2 2b cc bc be 5a 96.

La informació de la ubicació de la imatge original es troba a:
- Windows 7: taula SystemIndex_0A.
- Windows 8/10: taula SystemIndex_
PropertyStore.

Un cop escollida la taula, indiquem quina fila volem posant el número hexadecimal de la miniatura.

---

### MRU (Most Recently Used)

És una **llista de fitxers usats recentment** que es guarda al perfil de l'usuari **a l'arxiu NTUSER.DAT**.

Hi ha uns quants MRU emmagatzemats al registre. Veurem algunes de les ubicacions més comuns.

- **OpenSavePidlMRU** del fitxer d'usuari NTUSER.DAT segueix els darrers 20 fitxers oberts/guardats via els diàlegs comuns de Windows (Open/Save As).

![](imgs/artefactes-win-bb843aff.png)

- **NTUSER.DAT\Software\Microsoft\Windows\CurrentVersion\Explorer\RecentDocs**: Llista de fitxers que s'ha executat usant l'explorador d'arxius de Windows. Les subclaus estan organitzades per extensions d'arxiu i es guarden els arxius oberts per ordre cronològic.

![](imgs/artefactes-win-12b4fd03.png)

Per veure per exemple els últims arxius PDF usats, podem mirar a:
- **NTUSER.DAT\Software\Microsoft\Windows\CurrentVersion\Explorer\RecentDocs\.pdf**

També podem mirar a les **entrades OneDrive i Cloudlog** per potser buscar evidències al núvol si el sospitós ha pogut guardar fitxers al núvol.

![](imgs/artefactes-win-72aac279.png)

---

### Arxius d'Office recents

Podem veure una llista d'**arxius d'Office recentment oberts** a:

- **NTUSER.DAT\Software\Microsoft\Office\VERSION**

El número de versió pot ser diferent, per exemple:

- **NTUSER.DAT\Software\Microsoft\Office\15.0\Word**

Podem veure l'equivalència entre el número de versió i el nom comercial a https://docs.microsoft.com/en-us/deployoffice/install-different-office-visio-and-project-versions-on-the-same-computer#office-releases-and-their-version-number

A partir de l'Office 365, Microsoft enllaça la ubicació amb el "live ID" de l'usuari. En aquest escenari, els arxius recents es poden trobar a la següent ubicació:

- **NTUSER.DAT\Software\Microsoft\Office\VERSION\UserMRU\LiveID_####\FileMRU**

---

### Obrir/Guardar i diàleg d'arxius recentment usats

Quan s'obre o es desa un fitxer, apareix un **diàleg que ens pregunta on desar o obrir aquest fitxer**. Una vegada que obrim/desem un fitxer en una ubicació específica, **Windows recorda aquesta ubicació**. Podem buscar-la examinant les claus de registre següents

- **NTUSER.DAT\Software\Microsoft\Windows\CurrentVersion\Explorer\ComDlg32\OpenSavePIDlMRU**
- **NTUSER.DAT\Software\Microsoft\Windows\CurrentVersion\Explorer\ComDlg32\LastVisitedPidlMRU**

---

### Barres d'adreces/cerca al Windows Explorer

Una altra manera d'identificar l'**activitat recent d'un usuari** és mirant les rutes que s'escriuen a la barra d'adreces de l'Explorador de Windows o cerques realitzades utilitzant les següents claus de registre:

- **NTUSER.DAT\Software\Microsoft\Windows\CurrentVersion\Explorer\TypedPaths**
- **NTUSER.DAT\Software\Microsoft\Windows\CurrentVersion\Explorer\WordWheelQuery**

---

### Paperera de reciclatge (Recicle Bin)

És un magatzem intermig dels arxius esborrats per l'usuari. Es tracta d'una carpeta oculta al directori arrel de cada disc anomenada **$Recycle Bin**.

En un disc amb format NTFS hi haurà subcarpetes amb els SID dels usuaris, que es creen en el moment que els usuaris inicien sessió per primer cop.

**Si un usuari esborra un fitxer**:
- Aquest canvia el nom per $Rxxxxxx on les x són 6 caràcters alfanumèrics aleatoris. l'extensió no canvia.
- Es crea un segon arxiu $Ixxxxxx on les x són les mateixes que al fitxer anterior. El fitxer te la mateixa extensió que el $R.

**L'arxiu $I guarda:**
- la data i hora de l'esborrat.
- la ruta on estava l'arxiu original.
- la mida original.   

![](imgs/artefactes-win-2c74e179.png)

**Si un usuari esborra un directori**:
- Tindrem els arxius $R i $I.
- El fitxer $R contindrà tots els arxius i subdirectoris amb els noms originals.

**Si l'usuari buida la paperera de reciclatge**
- S'actualitza la llista de blocs lliures al sistema d'arxius de manera que quan es necessiti espai es sobreescriuran les dades.
- Com que els fitxers $I són molt petits, seran residents a la MFT si el sistema d'arxius és NTFS i poden desaparèixer molt ràpid si es fa ús del disc després.

---

### Accessos directes (shortcut) LNK

Un fitxer .LNK s'utilitza pel sistema operatiu Windows com a **drecera o enllaç a fitxers, aplicacions o recursos**. És un mètode senzill i fàcil d'utilitzar que permet als usuaris accedir a documents o aplicacions utilitzats amb freqüència. El fitxer d'enllaç conté informació útil per a l'investigador forense digital, com ara:

- Registre MAC (Modification-Access-Creation)
- Mida de fitxer
- Ruta al fitxer      
- Detalls del volum

Aquesta informació **continuarà existint tot i que el fitxer enllaçat desaparegui**.

Podem obtenir informació com ara:
- identificar els fitxers oberts des d'un dispositiu USB extraïble específic però mai desats localment al sistema.

- El coneixement per part de l'usuari de fitxers específics que va obrir si aquests fitxers s'emmagatzemaven en el sistema local, estaven a dispositius extraïbles o s'emmagatzemaven en xarxa.

- La identificació d'arxius que ja no existeixen en una màquina local. Si un fitxer suprimit s'havia obert prèviament, es va generar un fitxer LNK que l'apunta.

Podem usar l'**eina LECmd** d'Eric Zimmerman  (https://ericzimmerman.github.io/ ) per analitzar els arxius LNK.

![](imgs/artefactes-win-161eece3.png)
![](imgs/artefactes-win-f48cba7e.png)

---

### Jump list

- https://dfir.pubpub.org/pub/wfuxlu9v/release/1

Les Jump list es van introduir amb Windows 7 i són creades automàticament per **permetre als usuaris accedir als elements accedits amb freqüència o recentment**.
- Les Jump list s'associen a aplicacions específiques de programari i registren fitxers oberts des de l'aplicació. associades amb aplicacions de programari a través d'identificadors d'aplicació o AppID. Són identificadors únics universals en tots els sistemes Windows. Per exemple, Excel 2010 està associat amb l'AppID 9839aec31243a928. La jump list de Destinacions Automàtiques per a Excel 2010 seria 9839aec31243a928.automaticDestinations-ms. Es poden consultar a: https://community.malforensics.com/t/list-of-jump-list-ids/158

- Per accedir a una Jump list, l'usuari faria clic amb el botó dret a l'aplicació de programari des de la barra de tasques (per exemple Microsoft Word) i es mostraria una llista de documents recents associats amb l'aplicació de programari.

Es guarden a:
- C:\Users\[User Profile]\AppData\Roaming\Microsoft\Windows\Recent\AutomaticDestinations
- C:\Users\[User Profile]\AppData\Roaming\Microsoft\Windows\Recent\CustomDestinations

Hi ha dues variants de Jump lists:
- **Destinacions automàtiques**: contenen característiques comunes en totes les aplicacions de programari. Contenen l'extensió de fitxer .automaticDestinations-ms. Són fitxers compostos que contenen múltiples fluxos de dades dins d'un únic fitxer. Dins de Destinacions Automàtiques, cada flux conté una entrada LNK incrustada que es pot extreure i analitzar. El flux "DestList" actua com una MRU per als fitxers oberts des de l'aplicació de programari (13Cubed 2017).

- **Destinacions personalitzades**: tenen característiques específiques de l'aplicació que poden variar en funció de la decisió del desenvolupador d'implementar les característiques. Les destinacions personalitzades tenen l'extensió del fitxer .customDestinations-ms. Les Destinacions personalitzades també poden contenir una sèrie d'entrades LNK per a fitxers oberts utilitzant l'aplicació de programari (13Cubed 2017).

Cada cop que **un arxiu sigui obert amb doble clic o usant l'opció File-Open a l'explorador d'arxius**, es **crea un accés directe** a:
- **%Username%\Appdata\Roaming\Microsoft\Windows\Recent**

Podem analitzar les Jump Lists amb Jump List View de NIRSOFT - https://www.nirsoft.net/utils/jump_lists_view.html

Amb tota aquesta informació **podem relacionar un arxiu concret a un usuari específic**. També podem saber la data en que es va crear el LNK i quin és el numero de sèrie del volum i el nom de host on està el fitxer.

**ATENCIÓ:** Algunes d'aquestes propietats **es poden desactivar per l'usuari i l'administrador**.  

---

### Shellbags

Shellbags és un **conjunt de claus de registre que registren la mida i la ubicació de les carpetes i biblioteques** a les quals l'usuari ha accedit **a través de la interfície gràfica (Windows Explorer)**. Es poden trobar artefactes que mostrin la interacció de l'usuari amb dispositius de xarxa, suports extraïbles o contenidors encriptats.

Cada usuari pot personalitzar la seva pròpia configuració de Windows Explorer, inclús per a cada carpeta individualment. Per tant trobem la informació al "hive" de registre anomenat **USRCLASS.DAT** ubicat al perfil de cada usuari a la carpeta:
- **AppData\Local\Microsoft\Windows**.

La majoria d'eines forenses comercials analitzen els "shellbags" de l'arxiu USRCLASS.DAT. Una alternativa de codi obert és **Shellbag Explorer d'Eric Zimmerman**.

Podem trobar la informació a les claus:
- **USRCLASS.DAT\Local Settings\Software\Microsoft\Windows\Shell\Bags**
- **USRCLASS.DAT\Local Settings\Software\Microsoft\Windows\Shell\BagMRU**
- **NTUSER.DAT\Software\Microsoft\Windows\Shell\BagMRU**
- **NTUSER.DAT\Software\Microsoft\Windows\Shell\Bags**

El Shellbag **no permet saber quan s'ha accedit als arxius**, només **saber si s'hi ha accedit o no i quina és la primera interacció**. D'aquesta manera podríem saber que l'usuari accedeix a serveis al núvol, per exemple.

![](imgs/artefactes-win-659e3d9a.png)

Per saber **data i hora d'accés haurem de mirar en una altra banda, per exemple al registre.**

Com que aquest artefacte es crea per l'**acció voluntària de l'usuari**, **es pot fer servir com a prova que un usuari no diu la veritat quan diu que no sap res d'un determinat arxiu**.

---

## Obtenir Informació del sistema

### Versió del sistema

Si només disposem de dades de "triatge" per realitzar forenses, podem determinar la **versió del sistema operatiu a través del registre**. Per trobar la versió del sistema operatiu, podem utilitzar la següent clau de registre:

- **SOFTWARE\Microsoft\Windows NT\CurrentVersion**

### Current Control Set

Els conjunts de Control són els "hives" que contenen la **configuració de la màquina usada per controlar l'inici del sistema**. Hi ha habitualment dos "conjunts de control":
- **ControlSet001**: En la majoria dels casos, ControlSet001 apunta al conjunt de control amb el qual la màquina va arrencar. Ubicat a SYSTEM\ControlSet001
- **ControlSet002**: és l'última "configuració bona coneguda". Ubicat a SYSTEM\ControlSet002

Windows crea un conjunt de control volàtil quan la màquina es troba en viu, anomenat "**CurrentControlSet**" (**HKLM\SYSTEM\CurrentControlSet**). Aquest "hive" conté la informació més exacta del sistema.

Podem esbrinar quin **conjunt de control s'està utilitzant** com a "ActualControlSet" mirant el següent valor de registre:

- **SYSTEM\Select\Current**

De la mateixa manera, l'**última configuració bona coneguda** es pot trobar utilitzant el següent valor de registre:

- **SYSTEM\Select\LastKnownGood**

És vital establir aquesta informació abans d'avançar en l'anàlisi. Com veurem, **molts artefactes forenses que recopilem es recolliran dels Conjunts de Control**.

### Nom de l'ordinador

El nom de l'equip es troba a:
- **SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName**

### Time Zone

La informació de la **zona horària és important perquè algunes dades de l'ordinador tindran els seus codis de temps a UTC/GMT i altres a la zona horària local**. Saber la zona horària local ajuda a establir una línia de temps quan es fusionen les dades de totes les fonts.

- **SYSTEM\CurrentControlSet\Control\TimeZoneInformation**

### Interfícies de xarxa

Podem obtenir una llista d'interfícies i dades de configuració a:

- **SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces**

**Cada interfície** es representa amb un **subclau d'identificador únic (GUID)**, que conté valors relacionats amb la configuració TCP/IP de la interfície:
- adreces IP
- adreça IP DHCP
- Màscara de subxarxa
- servidors DNS
- altres

Aquesta informació és important perquè ajuda a assegurar-se que es fa el forense a la màquina on se suposa que s'ha de fer.

Les xarxes a les quals s'ha connectat una màquina i la darrera data de connexió (última data d'escriptura d'aquestes claus) es poden trobar en les següents ubicacions:

- **SOFTWARE\Microsoft\Windows NT\CurrentVersion\NetworkList\Signatures\Unmanaged**
- **SOFTWARE\Microsoft\Windows NT\CurrentVersion\NetworkList\Signatures\Managed**

---

### Programes "Autorun"

Els **programes que s'inicien automàticament** en iniciar sessió es troben a:

- **NTUSER.DAT\Software\Microsoft\Windows\CurrentVersion\Run**
- **NTUSER.DAT\Software\Microsoft\Windows\CurrentVersion\RunOnce**
- **SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce**
- **SOFTWARE\Microsoft\Windows\CurrentVersion\policies\Explorer\Run**
- **SOFTWARE\Microsoft\Windows\CurrentVersion\Run**

I sobre els serveis trobem informació a:

- **SYSTEM\CurrentControlSet\Services**

La clau "Start" indica que s'inicia a l'arrencada si conté un 0x02.

---

## Evidència d'execució

- Windows Forensics: Evidence of Execution - https://frsecure.com/blog/windows-forensics-execution/
- Evidence Of Execution (utilitat Velociraptor) - https://docs.velociraptor.app/docs/forensic/evidence_of_execution/

### UserAssist

Windows manté un **seguiment de les aplicacions executades per l'usuari** utilitzant **Windows Explorer** amb propòsits estadístics a les claus de registre "UserAssist".

Aquestes claus contenen informació sobre:
- els programes executats
- l'hora en que s'han executat
- el nombre de vegades que han estat executats.

Però els programes que s'executen **utilitzant la línia de comandes no es poden trobar a les claus "UserAssist"**.

Podem trobar-ho en la següent ubicació dintre cada GUID (globally unique identifier). Els GUIDs representen rutes comunes del sistema, com ara la carpeta AppData de l'usuari, system32 o d'altres rutes mapejades.

- **NTUSER.DAT\Software\Microsoft\Windows\Currentversion\Explorer\UserAssist\{GUID}\Count**

Aquí trobarem una **llista de GUIDs de Windows** - https://docs.microsoft.com/en-us/dotnet/desktop/winforms/controls/known-folder-guids-for-file-dialog-custom-places?view=netframeworkdesktop-4.8&redirectedfrom=MSDN:

Les **entrades "count"** contenen les **rutes dels arxius executats, però codificades en ROT-13**.

Més info: https://www.magnetforensics.com/blog/artifact-profile-userassist/#:~:text=UserAssist%20Recovery%20with%20Magnet%20Forensics,the%20program%20was%20last%20executed.

Podem veure la interpretació directament amb **Registry Explorer**. Cal extreure el hive NTUser.dat anar al subregistre UserAssist:

![](imgs/09-artefactes-win-c28711dc.png)

---

### Prefetch

Prefetch és una característica que Microsoft va introduir a Windows per millorar l'experiència de l'usuari. **Permet temps de resposta més ràpids precarregant dades a RAM en previsió de la seva demanda** per l'usuari o el sistema.

- Els fitxers prefetch estan a la ruta: **%windows%\PREFETCH**
- Els fitxers PREFETCH tenen extensió .pf i contenen informació sobre els fitxers als que està associat un executable, com per exemple la llista de fitxers utilitzats per l'executable, el nombre de vegades que s'ha executat, i l'última data/hora d'execució.

La majoria de les eines forenses comercials poden analitzar els fitxers prefetch. Com a opció open source podem usar **WinPrefetchViewtool de NirSoft** (https://www.nirsoft.net/utils/winpreprefetch.view.html ).

![](imgs/artefactes-win-65840290.png)

Utilitzant aquests artefactes, es pot **determinar quines aplicacions estan sent utilitzades per l'usuari**, i això pot conduir al descobriment de particions ocultes, dispositius mòbils, encriptats contenidors, o emmagatzematge en núvol.

**NOTA:** Windows 2003, 2008 i 2012 desactiven per defecte el prefetch. També Windows 7 sobre discos SSD per allargar la seva vida.

---

### ShimCache

ShimCache és un mecanisme utilitzat per fer un **seguiment de la compatibilitat d'aplicacions amb el sistema operatiu i de totes les aplicacions executades a la màquina**. El seu propòsit principal és **assegurar la compatibilitat amb aplicacions**. També s'anomena "**Application Compatibility Cache" (AppCompatCache)**. Es troba a la següent ubicació del hive del sistema:

- **SYSTEM\CurrentControlSet\Control\Session Manager\AppCompatCache**

ShimCache emmagatzema:
- el nom del fitxer
- la mida del fitxer
- la data de l'última execució dels executables.

Registry Explorer no analitza les dades de ShimCache en un format llegible per humans i per tant usarem una altra eina anomenada **"AppCompatCache Parser" d'Eric Zimmerman**, que llegeix el hive del sistema com a entrada, analitza les dades i dona com a sortida un fitxer CSV.

~~~
AppCompatCacheParser.exe --csv <path to save output> -f <path to SYSTEM hive for data parsing> -c <control set to parse>
~~~

Podem visualitzar la sortida amb **EZviewer**, també d'Eric Zimmerman.

**NOTA:** És una bona alternativa a prefetch ja que està a tots els sistemes Windows.

---

### AmCache

El hive **AmCache és un artefacte relacionat amb ShimCache. Fa una funció similar a ShimCache**, ja que emmagatzema dades addicionals relacionades amb les execucions de programes. Aquestes dades inclouen:
- la ruta on s'executa
- data de la instal·lació, d'execució i de supressió
- els hashs SHA1 dels programes executats.

Aquest hive es troba a:

- **C:\Windows\appcompat\Programes\Amcache.hve**

La informació sobre els **últims programes executats** es pot trobar a la següent ubicació del hive:

- **Amcache.hve\Root\File\{Volume GUID}\**

Podem analitzar AmCache amb **Registry Explorer**.

---

### BAM/DAM

El monitor d'activitats en segon pla o **BAM (Background Activity Monitor)** manté una seguiment de l'activitat de les aplicacions en segon pla. El **"Desktop Activity Moderator" o DAM** és un component de Windows que optimitza el consum d'energia de l'ordinador. Tots dos formen part del sistema "Modern Standby" de Windows.

Les ubicacions següents contenen informació relacionada amb BAM i DAM:
- els últims programes executats
- les seves rutes completes
- l'última execució.

Tenim la informació a les claus:
- **SYSTEM\CurrentControlSet\Services\bam\UserSettings\{SID}**
- **SYSTEM\CurrentControlSet\Services\dam\UserSettings\{SID}**

Ho podem veure amb **Registry Explorer**.

---

### SRUM

El SRUM és una característica en els sistemes Windows moderns que recullen estadístiques sobre l'execució de binaris i també del tràfic de xarxa. La informació s'emmagatzema en una base de dades del motor d'emmagatzematge extensible (ESE). L'ESE és el format propietari de base de dades d'arxiu únic de Microsoft, que actua de manera similar a SQLite, com a motor d'emmagatzematge predeterminat per a moltes aplicacions, incloent la base de dades SRUM.

- https://velociraptor.velocidex.com/digging-into-the-system-resource-usage-monitor-srum-afbadb1a375#:~:text=SRUM%20is%20a%20feature%20in,Storage%20Engine%20(ESE)%20database
- SRUM is maybe one of the best Windows digital forensic artefacts, if you’re willing to roll your sleeves up. You can get proof of execution and execution runtime, as well as proof of network communication and the bytes sent and received - (https://twitter.com/Purp1eW0lf/status/1504491533487296517?t=X2iK8opKOJfWUTwyx8s5cQ&s=03)


Podem extreure les dades de SRUM amb l'eina SrumECmd de l'Eric Zimmerman - https://github.com/EricZimmerman/Srum

Per visualitzar els fitxers generats per l'eina SrumECmd, podem usar Timeline Explorer del mateix autor.

També podem usar ESEDatabaseView per a veure directament la BD - https://www.nirsoft.net/utils/ese_database_view.html

---

## Informació sobre dispositius externs i dispositius USB

Quan es realitzen forenses en una màquina, sovint sorgeix la necessitat d'identificar si algú ha connectat un dispositiu extraïble o USB.

### Identificació de dispositius

Les ubicacions següents mantenen el seguiment de les claus USB connectades a un sistema.
- **SYSTEM\CurrentControlSet\Enum\USBSTOR**
- **SYSTEM\CurrentControlSet\Enum\USB**

Aquestes ubicacions emmagatzemen:
- l'ID del venedor
- l'ID del producte
- la versió del dispositiu USB connectat

Es pot utilitzar per identificar dispositius de forma única. Aquestes ubicacions també emmagatzemen el moment en que es van endollar els dispositius al sistema.

Amb **Registry Explorer** podem veure aquesta informació d'una manera fàcil.

### Primera/Darrera vegada de connexió

La següent clau de registre controla la primera i última vegada que es va connectar el dispositiu extern i la darrera que es va desconnectar.  

- **SYSTEM\CurrentControlSet\Enum\USBSTOR\Ven_Prod_Version\USBSerial#\Properties\{83da6326-97a6-4088-9453-a19231573b29}\####**

Els símbols #### poden ser:
- 0064 per la primera connexió
- 0066 per la darrera connexió
- 0067 per la darrera desconnexió

### Nom de volum del dispositiu USB

Es pot trobar a:

- **SOFTWARE\Microsoft\Windows Portable Devices\Devices**

Cal correlar el número de sèrie obtingut anteriorment per saber a quin dispositiu correspon el nom.

---

Nota: Quan es carreguin els hives de registre al **RegistryExplorer**, ens advertirà que els hives  estan "bruts" (no actualitzats). Això vol dir que cal usar els registres de transaccions i indicar a RegistryExplorer els fitxers .LOG1 i .LOG2 amb el mateix nom de fitxer que el hive del registre, per integrar automàticament els registres de les transaccions i crear un hive "net". Una vegada li diem a RegistryExplorer on salvar el hive net, podem utilitzar-ho per a la nostra anàlisi i ja no necessitarem carregar els hives "bruts". RegistryExplorer ens guiarà a través d'aquest procés.

---

**Atenció:** Com que els sistemes operatius canvien o s'actualitzen, els artefactes poden moure's o ser eliminats. Cal estar actualitzat sobre els possibles canvis.

---

## Eines i referències

- Referència completa dels esdeveniments del sistema - https://www.ultimatewindowssecurity.com/securitylog/encyclopedia/default.aspx
- També teniu la referencia d'events del sistema windows (document word adjunt) - Windows 10 and Windows Server 2016 security auditing and monitoring reference - https://www.microsoft.com/en-us/download/details.aspx?id=52630
- Eines d'Eric Zimmerman per Windows - https://ericzimmerman.github.io/#!index.md
- Eines de Sysinternals per Windows - https://docs.microsoft.com/en-us/sysinternals/
- Eines de Nirsoft per Windows - https://www.nirsoft.net/
- Evidències d'execució - https://frsecure.com/blog/windows-forensics-execution/

- sysmon-config | A Sysmon configuration file for everybody to fork - https://github.com/SwiftOnSecurity/sysmon-config
- sysmon-modular | A Sysmon configuration repository for everybody to customise - https://github.com/olafhartong/sysmon-modular
- Generación de Insumos forenses con EvtxECMD, MFTECMD (Eric Zimmerman Tools) (2022) - https://www.youtube.com/watch?v=ZK2rD9OOZh4

---

### Antiforense

- Creating a Hidden Prefetch File to Bypass Normal Forensic Analysis (2019) - https://www.binary-zone.com/2019/05/26/creating-a-hidden-prefetch-file-to-bypass-normal-forensic-analysis/?s=09

---

VSS volume shadow copies in forensic - https://andreafortuna.org/2017/10/02/volume-shadow-copies-in-forensic-analysis/
- libvshadow - https://github.com/libyal/libvshadow
- Windows_Shadow_Volumes - https://forensicswiki.xyz/wiki/index.php?title=Windows_Shadow_Volumes

- https://leahycenterblog.champlain.edu/2014/01/23/volume-shadow-copy-blog-1/
- https://leahycenterblog.champlain.edu/2014/02/05/volume-shadow-copy-part-2/
- https://leahycenterblog.champlain.edu/2014/02/26/volume-shadow-copy-part-3/
- https://leahycenterblog.champlain.edu/2014/03/26/volume-shadow-copy-part-4/
- https://www.shadowexplorer.com/downloads.html

---

- chainsaw Rapidly Search and Hunt through Windows Forensic Artefacts - https://github.com/WithSecureLabs/chainsaw
