# ORGANITZACIO DE DISCOS

![](imgs/organització-discos-2bff61ee.png)

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [ORGANITZACIO DE DISCOS](#organitzacio-de-discos)
	- [DISC DURS](#disc-durs)
	- [DESCRIPCIO BASICA](#descripcio-basica)
	- [PARTICIONS](#particions)
		- [AVANTATGES DE FER PARTICIONAT](#avantatges-de-fer-particionat)
		- [1. PARTICIONAT MBR](#1-particionat-mbr)
			- [ESTRUCTURA D'UN DISC AMB MBR](#estructura-dun-disc-amb-mbr)
			- [PROCES D'ARRENCADA DEL PC AMB BIOS / MBR](#proces-darrencada-del-pc-amb-bios-mbr)
		- [2. PARTICIONAT GPT (GUID)](#2-particionat-gpt-guid)
			- [AVANTATGES DE GPT (RESPECTE MBR)](#avantatges-de-gpt-respecte-mbr)
	- [SISTEMA UEFI](#sistema-uefi)
		- [Procés d'arrencada amb UEFI](#procs-darrencada-amb-uefi)
	- [FORMATAT D'UNA PARTICIÓ O DISC](#formatat-duna-partici-o-disc)
	- [SISTEMES D'ARXIUS](#sistemes-darxius)
		- [TIPUS DE SISTEMES D'ARXIUS](#tipus-de-sistemes-darxius)
	- [REFERENCIES:](#referencies)
		- [DISC DUR](#disc-dur)
		- [Eines de particionat](#eines-de-particionat)
		- [Eines de recuperació d'arxius](#eines-de-recuperaci-darxius)
		- [BIOS i UEFI](#bios-i-uefi)
		- [Sistemes d'arxius](#sistemes-darxius)

<!-- /TOC -->
---

## DISC DURS

- "Como funciona el disco duro" - https://www.youtube.com/watch?v=fTRxLMJn_Jg
- "Diferencias HD y SSD" - https://www.youtube.com/watch?v=NyvXV45I2VI

---

![](imgs/organització-discos-bfd80a43.png)

Esquema simplificat d'un disc dur.

---

## DESCRIPCIO BASICA

- **Disc dur (Hard Disk Drive o HDD)** és un dispositiu d'emmagatzemament no volàtil. S'hi guarden grans quantitats de dades digitals en la superfície magnetitzada dels diversos discs que conté, que anomenem "plats" (platter). Els plats giren a gran velocitat i tenen dues cares on guardar informació.

Alguns dels principals termes per a calcular la capacitat d'un disc són:
- **Plat**: Cadascun dels discos que hi ha dintre del disc dur.
- **Cara**: Cadascun dels dos costats d'un plat
- **Capçal**: El nombre de capçals; equival a donar el nombre de cares, ja que hi ha un capçal per cara.
- **Pista**: Una circumferència dintre d'una cara; la pista 0 està situada a la vora exterior.
- **Cilindre**: Conjunt de diverses pistes; són totes les circumferències que estan alineades verticalment (una de cada cara).
- **Sector** : Cadascuna de les divisions d'una pista. És la unitat bàsica de treball. Sempre que s'accedeix a un disc, es llegeix/escriu mínim un sector sencer.
  - La grandària del sector no és fixa, sent l'estàndard més habitual de 512 bytes. Antigament el nombre de sectors per pista era fix, la qual cosa desaprofitava l'espai significativament, ja que a les pistes exteriors es poden emmagatzemar més sectors que en les interiors.

![](imgs/organització-discos-30143dbc.png)

Els conceptes en un dibuix

![](imgs/organització-discos-9c118bba.png)

Disc real

- **Temps d'accés**: Per calcular el temps que es triga en obtenir un determinat sector del disc (bloc), cal calcular la suma de:
1. Seek time = posicionament del capçal a la pista + temps d'espera del sector buscat (temps de gir del disc).
2. Temps de transferència, des del dispositiu a la memòria RAM.

---

## PARTICIONS

Per guardar la informació als discos durs, es creen **particions** (divisions lògiques del disc, tractades com a discos diferents).

Podem pensar en el fet de guardar dades al disc amb el símil de sembrar un camp.
- **Crear particions**: El primer pas és marcar la parcel·la que et toca posant tanques al voltant de la parcel·la.
- **Formatar amb un sistema d'arxius**: El segon pas és llaurar el camp i preparar-ho per a poder tirar-hi les llavors (posar-hi dades) si volem que creixin.

![](imgs/3.organització-discos-47b92317.png)
![](imgs/3.organització-discos-8adffa00.png)


Hi ha 2 models de particionat:
- **Particions MBR (Master Boot Record)**. Model Antic que funcionava amb sistema d'arrencada BIOS.
- **GPT (GUID Partition Table)** (GUID és Global Unique Identifier). Model més modern i que s'hauria de fer servir en discs grans. Funciona amb sistema d'arrencada UEFI.

---

### AVANTATGES DE FER PARTICIONAT

D'entrada, no es pot guardar dades al disc sense haver creat una taula de particions i al menys una partició. Els usos són:
- Poder **usar sistemes d'arxius antics** (ex: FAT), ja que tenen una mida màxima i cal dividir el disc en trossos més petits.
- Es pot **guardar una còpia de restauració** ràpida a una altra partició (ex: portàtils amb partició de recuperació).
- Permeten **coexistir diferents sistemes** operatius i sistemes d'arxius diferents.
- Permeten separar la **partició d'intercanvi** (swap).
- Permeten guardar per **separat la informació dels usuaris** per si cal reinstal·lar el sistema operatiu en una partició diferent.
- El possible desbordament de dades d'usuari no afecta al sistema (**quotes físiques**).

---

### 1. PARTICIONAT MBR

Quan s'inicia un PC, cal carregar el primer programa de control del PC. Aquest programa originalment era:
- **BIOS (Basic Input-Output System)**, creat per IBM al 1983 és el primer programa (firmware) que s'executa en quant el sistema s'inicia. Sol estar emmagatzemat en una memòria flash ubicada a la placa base, i és independent del sistema d'emmagatzemament.

![](imgs/organització-discos-091890f0.png)

ROM de BIOS dels primers PC's

**MBR (Master Boot Record)**: Creat per Microsoft al 1983. És un registre al sector 0 del disc dur (512 bytes), llegit per la BIOS en l'arrencada. Conté només 4 entrades (1 per partició) per a indicar on comencen les particions al disc.

---

![](imgs/organització-discos-bb7c0232.png)

Organització de la MBR (CHS=Cilynder,Head,Sector i LBA=Logical Block Address)

---

#### ESTRUCTURA D'UN DISC AMB MBR

Organització del disc

![](imgs/organització-discos-b2dd18b6.png)

---

**Tipus de particions MBR**

1. **Partició Primària**: En poden haver fins a 4 (només hi ha 4 entrades a la taula MBR), o fins a 3 primàries i 1 estesa. Suporta un sistema d'arxius.
2. **Partició Estesa**: Partició que cal subdividir en particions lògiques. No suporta sistema d'arxius directament.
3. **Partició Lògica**: Subdivisió de la partició estesa. Es poden crear les que vulguem a dintre. Suporta sist. Arxius directament.
4. **Partició Activa**: Estat d'una partició PRIMÀRIA (pot iniciar SOP en el sistema MBR, només hi havia una activa).

---

#### PROCES D'ARRENCADA DEL PC AMB BIOS / MBR

![https://www.incibe-cert.es/blog/bootkits](imgs/organització-discos-05dc3ec6.png)

1. El sistema s'engega i s'**executa l'autotest** (power-on self-test o **POST**)
2. La BIOS **inicialitza el maquinari** requerit per arrencar (disc, controladors de teclat, etc.).
3. La BIOS **llegeix el sector 0** del primer disc de la llista BIOS. **Executa el codi d'arrencada** de la "Master Boot Record" (primers 440 bytes).
4. Aquest carregador de la primera fase **arrenca el codi de la segona fase des d'una partició** (la que estigui activa o bé la que escull el codi del MBR)
5. El **boot loader de veritat és arrencat** i aquest carrega ja el sistema operatiu (en cadena, per exemple quan hi ha arrencada múltiple i GRUB ha de cridar el boot loader de Windows o bé carregant el kernel del sistema).

---

### 2. PARTICIONAT GPT (GUID)

**GPT (GUID Partition Table)**: és un estàndard que substitueix a MBR i està associat amb els sistemes UEFI.

![](imgs/organització-discos-4f146855.png)

- El seu nom  prové de que a cada partició se li associa un únic identificador global (**GUID o anomenat UUID en Linux**). És un identificador aleatori tan llarg que cada partició en el mon podria tenir el seu ID únic.

---

![](imgs/organització-discos-33769515.png)

Mapa d'un disc amb model GPT

---

#### AVANTATGES DE GPT (RESPECTE MBR)

1. Cada partició te el seu propi **identificador de disc (GUID) i identificador únic de partició (PARTUUID)**. Així es pot referenciar el disc i la partició independentment del sistema d'arxius que contingui.
2. **Cada partició te la seva etiqueta** independent del sistemes d'arxius (PARTLABEL).
3. **GPT no te cap límit** (més enllà dels que tinguin els propis sistemes operatius:
- Ni en mida de les particions
- Ni en el nombre de particions.
- Per defecte la taula GPT te espai per a 128 particions. Si es necessiten més particions, es pot reservar més espai per a taules de particions.
- Per exemple, Windows limita a 128 particions (mida de la taula de particions per defecte de GPT).

4. **No es necessiten particions esteses ni lògiques.**

5. **Usa LBA** (logical bloc address) de 64-bit per guardar números de sectors, de manera que la **mida màxima de disc és de 2 ZiB** (2^30 GiB). En canvi MBR està limitat a 2 TiB d'espai per disc (32 bits d'adreçament).

6. GPT crea una **capçalera i una taula de particions de backup** al final del disc que permet recuperar la taula primaria en cas que es fes malbé.

7. Usa **CRC32 per detectar errors** i corrupció de la capçalera o la taula de particions.


![](imgs/organització-discos-63b19961.png)

Múltiples d'unitats d'informació

---

## SISTEMA UEFI

![](imgs/organització-discos-160dfb39.png)

És el sistema d'inici de l'ordinador que ha substituït a BIOS.

**UEFI (Unified Extensible Firmware Interface)**, va ser creat al 2003 per Intel i perfeccionat al 2008 per UEFI fòrum **per substituir la BIOS i el MBR**. Té suport per llegir tant taules de particions com sistemes de fitxers.

- Per compatibilitat, UEFI manté el **MBR (de protecció)** per evitar que programari antic destrueixi dades de GPT.

- UEFI no arrenca mai el codi del Master Boot Record (MBR), existeixi o no. En comptes d'això, **arrenca programari ubicat a la NVRAM** (de la placa mare del PC).

- UEFI suporta nativament sistemes d'arxius **FAT12, FAT16, FAT32 i ISO-9660** (discs òptics).

- UEFI arrenca sempre **aplicacions EFI**. Aquestes aplicacions estan guardades com a fitxers a la ESP (una partició de sistema EFI de entre 100 i 500 MB, ESP=EFI System Partition). Cada fabricant pot guardar els seus fitxers a la **ESP (partició de sistema EFI)** a la carpeta "/EFI/nom_fabricant". Aquesta aplicació pot ser arrencada afegint una entrada d'inici a la NVRAM o des de la shell UEFI.

- Una **Shell UEFI** és un terminal pel firmware que permet arrencar aplicacions EFI que inclouen bootloaders UEFI. A més, el shell es pot usar per a obtenir informació sobre el sistema o el firmware. Per exemple, un mapa de memòria (memmap), canviar variables del boot manager (bcfg), executar programes de particionat (diskpart), carregar drivers UEFI, editar fitxer de text (edit), editar codi hexadecimal (hexedit), etc.

- L'especificació UEFI te suport per a **BIOS heredades (legacy)** arrencant amb el mòdul de suport de compatibilitat (**Compatibility Support Module o CSM**).

- Quan habilitem **CSM a la UEFI**, UEFI genera entrades d'arrencada per a tots els discos. Si escollim una entrada d'arrencada CSM, el sistema CSM de UEFI intentarà arrencar des del codi d'arrencada guardat al MBR, emulant un sistema BIOS.

https://wiki.archlinux.org/index.php/Arch_boot_process#UEFI

---

### Procés d'arrencada amb UEFI

![](imgs/organització-discos-192f11ca.png)

1. El sistema s'engega i s'executa l'autotest (power-on self-test o POST)
2. UEFI inicialitza el maquinari requerit per arrencar.
3. El Firmware llegeix les entrades d'arrencada de la NVRAM per determinar quina aplicació EFI arrencarà i des de quin disc i partició.
4. Una entrada d'arrencada podria ser simplement un disc. En aquest últim cas, el firmware busca la partició de sistema EFI en aquell disc i intenta trobar una aplicació EFI a la ruta per defecte \EFI\BOOT\BOOTX64.EFI (BOOTIA32.EFI en sistemes de UEFI de 32-bit UEFI). Així s'arrenquen dispositius extraïbles.
5. El Firmware arrenca l'aplicació EFI, que pot ser un carregador d'arrencada (boot loader) o el kernel d'un sistema operatiu. També podria ser una altra aplicació EFI com ara una shell EFI o un gestor d'arrencada (boot manager).
6. Si l'arrencada segura (Secure Boot) està activada, el procés d'arrencada verifica l'autenticitat de l'aplicació EFI amb la seva signatura digital.

   - Un **carregador d'arrencada (boot loader o bootstrap loader)** es un programari iniciat per la BIOS o la UEFI. És responsable de carregar el kernel amb els seus paràmetres, i un disc RAM inicial (si és Linux) basat en els fitxers de configuració. Després li passa el control.

   - S'utilitza un **gestor d'arrencada (boot manager)** per a casos en què hi ha diversos sistemes operatius instal·lats en un ordinador. La seva funció és demanar a l'usuari que triï un d'ells o que prengui un valor predeterminat. Un cop feta l'elecció, el gestor d'arrencada carrega i executa el carregador d'arrencada d'aquest sistema operatiu.

	 Un exemple de boot loader és el de Windows anomenat NTLDR (abreviació de NT Loader), llançat des del registre d'arrencada de volum de la partició del sistema per a sistemes BIOS. Per als sistemes UEFI és llançat pel microprogramari de l'ordinador des de la partició EFI.

- El propi kernel pot ser arrencat directament per UEFI. Es pot encara usar un boot loader o boot manager per separat per editar els paràmetres del kernel abans d'arrencar.

---

## FORMATAT D'UNA PARTICIÓ O DISC

És el procès d'inicialització de l'espai del disc per poder guardar fitxers. Implica la **creació d'un sistema de fitxers** a dintre la partició. Hi ha dues modalitats de formatat:

- **Formatat de baix nivell o física**: inicialització dels blocs lògics en els plats del disc físic (crear pistes i sectors). Generalment es fa a la fàbrica i no acostuma a ser canviat.

- **Formatat d'alt nivell o lògic**: S'escriu les estructures del sistema de fitxers en determinats blocs lògics per fer que la resta dels blocs lògics estiguin disponibles per al sistema operatiu i les seves aplicacions. Es creen la MBR, les taules d'arxius i les taules de blocs lliures i ocupats (tot això ocupa espai del disc que no es pot usar pels usuaris). Pot ser de dos tipus:
  - **Ràpid**: Només es declaren buits tots els blocs del disc. Les dades NO s'esborren (**es podrien recuperar**).
  - **Normal**: Es sobreescriuen tots els blocs de disc amb 0's.


Les dades esborrades encara es poden recuperar.
- Quan s'esborra un fitxer, a la taula de gestió de fitxers s'esborra l'enllaç als blocs que es tenien assignats al fitxer i es declaren blocs buits.
- Quan es fa un format ràpid, es declaren els blocs com lliures, tot i que les dades encara hi són.

Podríem visualitzar la situació pensant que el disc és una calaixera d'un pis compartit i cada calaix és un bloc de disc.

Ens assignen un calaix i ens diuen que està lliure. L'obrim i trobem a dintre roba. Si el volem fer servir, traiem aquella roba i posem la nostra. Simplement, l'arrendador del pis no ha volgut gastar temps en buidar el calaix.


Per què es poden recuperar les dades? Com evitar-ho? - https://www.howtogeek.com/125521/htg-explains-why-deleted-files-can-be-recovered-and-how-you-can-prevent-it/

---

## SISTEMES D'ARXIUS

**Sistema d'arxius**: component del sistema operatiu que controla com es guarden o recuperen dades als dispositius d'emmagatzemament.

**Funcions**:
- Gestió de l'espai de noms dels fitxers.
- Gestionar l'espai d'emmagatzematge:
  - Assignar espai a arxius.
  - Alliberar espai d'arxius esborrats.
- Administració de l'espai lliure/ocupat.
- Accés a les dades guardades (Trobar/guardar les dades dels fitxers)
- Organitzar fitxers al sistema
- Assegurar proteccions de fitxers (ACL's)

---

### TIPUS DE SISTEMES D'ARXIUS

Cada sistema operatiu sol tenir el seu propi sistema d'arxius. Els més utilitzats i coneguts:
- **FAT (16, 32)**: File allocation table (MSDOS, Windows 95, 98).
- **NTFS**: New Technology File System (A partir de Windows NT, Windows 2000, XP, 7, 8, 10, Windows Server).
- **Ext2, 3, 4**: Extended File System Versions 2, 3 o 4, Linux.
- **HFS**: Hierarchical File System, MacOS
- **HFS+**: Extended Hierarchical File System, MacOS

---

## REFERENCIES:

### DISC DUR

- Disc dur - https://ca.wikipedia.org/wiki/Disc_dur
- Formatat de baix nivell - https://www.webopedia.com/TERM/L/LLF.html
- Formatat d'alt nivell - https://www.webopedia.com/TERM/H/HLF.html

---

### Eines de particionat

- Particionat - https://wiki.archlinux.org/index.php/Partitioning
- MBR amb fdisk - https://wiki.archlinux.org/index.php/Fdisk
- GPT (GUID) amb gdisk- https://wiki.archlinux.org/index.php/GPT_fdisk
- Eina gdisk (oficial) - https://www.rodsbooks.com/gdisk/

---

### Eines de recuperació d'arxius

- Llistat d'eines comunes - https://wiki.archlinux.org/index.php/File_recovery

---

### BIOS i UEFI

- BIOS vs UEFI (i MBR vs GPT): https://www.incibe-cert.es/blog/bootkits
- UEFI - https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface
- Recuperar arrencada UEFI des de Shell UEFI - https://www.frikisdeatar.com/recuperar-el-arranque-uefi-con-uefi-shell/
- Comandes de shell UEFI - https://docstore.mik.ua/manuals/hp-ux/en/5991-1247B/ch04s13.html
- UEFI Boot Manager (Intel) - https://software.intel.com/en-us/articles/uefi-boot-manager-1

---

###  Sistemes d'arxius

- Sistemes d'arxius - https://en.wikipedia.org/wiki/File_system
- FAT wikipedia - https://en.wikipedia.org/wiki/File_Allocation_Table
- FAT - http://www.ntfs.com/fat-systems.htm
- NTFS - https://en.wikipedia.org/wiki/NTFS
- NTFS tècnic - https://www.ntfs.com
- Ext3 - https://en.wikipedia.org/wiki/Ext3
- Ext4 - https://en.wikipedia.org/wiki/Ext4
- HFS - https://es.wikipedia.org/wiki/Sistema_de_archivos_de_Apple#Encriptaci%C3%B3n
