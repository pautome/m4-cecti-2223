# Funcionament dels dispositius SSD

**INS Carles Vallbona**

**Pau Tomé**

---

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Funcionament dels dispositius SSD](#funcionament-dels-dispositius-ssd)
	- [SSD (Solid State Drive)](#ssd-solid-state-drive)
		- [Organització de la informació](#organitzaci-de-la-informaci)
		- [Operacions bàsiques: Lectura, escriptura, modificació, esborrat](#operacions-bsiques-lectura-escriptura-modificaci-esborrat)
		- ["Wear-leveling" (anivellament del desgast)](#wear-leveling-anivellament-del-desgast)
		- [FTL (Flash Translation Layer)](#ftl-flash-translation-layer)
			- [Mapatge lògic de blocs](#mapatge-lgic-de-blocs)
			- [Garbage collection](#garbage-collection)
		- [Comanda TRIM (TRIM = "Retallar")](#comanda-trim-trim-retallar)
		  - [TRIM en Windows](#TRIM-en-Windows)
      - [TRIM en Linux](#TRIM-en-Linux)  
		- [El sobre-aprovisionament](#el-sobre-aprovisionament)
	- [Forense SSD](#forense-ssd)
	- [Referències](#referncies)

<!-- /TOC -->

---

## SSD (Solid State Drive)

Les **unitats d'estat sòlid (Solid-state drives)** s'anomenen així perquè no es basen en peces mòbils com els HDD. En un SSD, les dades es guarden a un conjunt de cel·les NAND-flash (és la tecnologia més usada, n'hi d'altres com NOR-flash).

---

<img src="imgs/ssd-c2f4da1f.png" width="700" align="center" />

Arquitectura d'un SSD

---

Els transistors utilitzats en DRAM, han de ser refrescats múltiples vegades per segon. En canvi el **NAND-flash està dissenyat per mantenir el seu estat de càrrega fins i tot quan no està alimentat**, pel que és no volàtil.

El principal avantatge d'aquest sistema és que, en no tenir parts mòbils, els **SSD poden operar a velocitats molt superiors** a les d'un HDD típic.

Una propietat important dels mòduls NAND-flash és que **les seves cel·les es  desgasten**, i per tant tenen una vida limitada, diferent segons el tipus de NAND-flash. De fet, els transistors que formen les cel·les emmagatzemen bits mitjançant la retenció d'electrons. En cada cicle P/E (és a dir, "Program/Erase", "Programar" aquí significa escriptura), els electrons poden quedar atrapats en el transistor per error, i després d'algun temps, acumular-se massa electrons que faran que les cel·les quedin inutilitzables.

En el **flash-NAND** s'usa lògica negativa, un 0 significa que tenim un 1. Els bits s'emmagatzemen en cel·les, que són de tres tipus:
- **SLC (Single Level Cell):** 1 bit per cel·la (un sol nivell de cel·la) i una llarga esperança de vida. Cost de fabricació alt.
- **MLC (Multiple Level Cell):** 2 bits per cel·la, amb més latència i menys vida que el SLC.
- **TLC (Triple Level Cell):** 3 bits per cel·la, amb la latència més gran i la menor vida.

La **majoria dels SSD orientat al públic en general estan basats en MLC o TLC**, i només els SSD professionals estan basats en SLC.

L'elecció del tipus de memòria adequat depèn de la càrrega de treball per a la qual s'utilitzarà la unitat, i amb quina freqüència és probable que les dades s'actualitzin.
- Per a **càrregues de treball d'alta actualització**, SLC és la millor opció
- Per a **càrregues de treball d'alta lectura i baixa escriptura** (ex: emmagatzematge de vídeo i transmissió), llavors TLC és millor. A més, les unitats TLC sota una càrrega de treball d'ús habitual donen una bona esperança de vida.

---

### Organització de la informació

El flaix NAND està organitzat en:
- **Blocs**: una quadrícula o matriu de cel·les NAND.
- **Pàgines**: Cada fila individual de la quadrícula.

<img src="imgs/ssd-7b650952.png" width="400" align="center" />

Organització de la memòria Flash en blocs i pàgines


Les mides més comunes de pàgina són 2K, 4K, 8K, o 16K, amb 128 a 256 pàgines per bloc. Per tant, la mida del bloc varia típicament entre 256 KB (2K * 128) i 4 MB (16K * 256).

La unitat més petita que es pot **llegir o escriure** d'un bloc és una pàgina. Però les **pàgines no es poden esborrar individualment**, només es poden **esborrar blocs sencers**.

<img src="imgs/ssd-4778e880.png" width="500" align="center" />

Latència d'accés comparada dels dispositius en microsegons

---

Hi ha dues coses que s'han d'observar en el gràfic anterior.
- Afegir **més bits** per cel·la de NAND té un impacte significativament **negatiu en el rendiment de la memòria**.
- És **pitjor per a les escriptures** que per a les lectures.

---

### Operacions bàsiques: Lectura, escriptura, modificació, esborrat

A causa de l'organització de les cel·les NAND-flash, no és possible llegir o escriure cel·les individuals.

- **Lectura:** No és possible llegir menys d'**una pàgina alhora**. Es pot demanar només un byte al sistema operatiu, però es recuperarà una pàgina completa del SSD, obligant a llegir moltes més dades de les necessàries.

- **Escriptura:** Quan s'escriu a un SSD, **s'escriu per pàgines**. Per tant, fins i tot si una operació d'escriptura només afecta un byte, s'escriurà de totes maneres una pàgina sencera. L'escriptura de més dades de les necessàries es coneix com a **amplificació d'escriptura** (**write amplification**). Els termes "escriure” i "programar” s'utilitzen indistintament en la majoria de publicacions i articles relacionats amb SSDs.

- **Modificació:** Les pàgines no es poden sobreescriure al contrari del que passava als HDD. Només es pot escriure una pàgina NAND-flash si està en estat «lliure». Quan es canvien les dades cal fer una operació anomenada **«read-modify-write»**:
  1. el contingut de la pàgina es copia en un registre intern
  2. les dades s'actualitzen
  3. la nova versió s'emmagatzema en una pàgina «lliure»

  Les dades no s'actualitzen a la mateixa pàgina, ja que s'hauria d'esperar l'esborrat d'aquesta, cosa molt lenta i que es fa per blocs. Una vegada que les dades s'han guardat, la pàgina original es marcarà com a **«viciada» ("stale")**, i romandrà així fins que s'elimini.

  Si intentem llegir pàgines marcades com a «viciada» ("stale") només obtindrem una seqüència de zeros, tot i que la informació encara estarà present a la memòria flash. L'única manera d'obtenir la informació és llegir directament els xips flash, cosa bastant complicada per que cal reconstruir l'ordre de les pàgines LBA que no coincidirà amb l'ordre físic de les pàgines.  https://www.gillware.com/flash-drive-data-recovery/flash-memory-amnesia-resurrecting-data-through-direct-read-of-nand-memory/

---

<img src="imgs/ssd-ba5ccb2a.png" width="700" align="center" />

Procès read-modify-write simplificat (sense "garbage collection")

---

- **Esborrat:** Les pàgines no es poden sobreescriure, i una vegada marcades com "viciades", l'única manera de tornar-les a alliberar és esborrar-les. Però, no és possible esborrar pàgines individuals, **cal fer-ho per blocs sencers alhora**. Des de la perspectiva d'usuari, només es poden emetre ordres de lectura i escriptura quan s'accedeix a les dades. L'ordre d'esborrat s'activa automàticament pel procés de **recollida d'escombraries ("garbage collection")** en el controlador SSD quan necessita recuperar pàgines per tenir espai lliure.

<img src="imgs/ssd-d3a2ec64.png" width="700" align="center" />

By Music Sorter at English Wikipedia, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=11568602

---

### "Wear-leveling" (anivellament del desgast)

Com que les cel·les NAND-flash es desgasten, cal distribuir el treball entre les cel·les de la forma més uniforme possible perquè les pàgines arribin al seu límit de cicle P/E i es desgastin homogèniament.

Això implica també **moure informació d'un bloc a un altre** a nivell intern del dispositiu, en principi no sent necessari. Això provoca un augment de l'**amplificació d'escriptura**.

Hi ha blocs que mai no canvien **(cold data)** i d'altres que canvien contínuament **(hot data)**, així que cal detectar cada tipus i moure els "cold data" a d'altres blocs per que el bloc que ocupaven vagi gastant-se també.

En cas contrari, hi haurà cel·les que moriran molt abans que les altres i l'espai en disc disminuirà. Aquest procés el gestionaran els algorismes de "garbage collection" de la controladora SSD.

Per tant, la gestió de blocs busca un terme mig entre maximitzar l'anivellament de desgast i minimitzar l'amplificació d'escriptura.

---

### FTL (Flash Translation Layer)

Als SSD es requereix un component addicional per amagar les característiques internes de la memòria flash-NAND i exposar només una matriu de LBA a l'amfitrió. Aquest component s'anomena la Capa de Traducció Flash (FTL), i resideix en el controlador SSD. El FTL és crític i té dos propòsits principals: el **mapatge lògic de blocs i la recollida d'escombraries**.

#### Mapatge lògic de blocs

El mapatge lògic de blocs **tradueix adreces de blocs lògics (LBA) de l'espai de l'ordinador en adreces de blocs físics (PBA) en l'espai físic de memòria NAND-flash**. Aquest mapatge pren la forma d'una taula, que per a qualsevol valor LBA dóna el corresponent PBA. **Aquesta taula de mapatge s'emmagatzema a la RAM de l'SSD per millorar la velocitat d'accés**, i es manté** en memòria flaix en cas d'apagada**. Quan l'SSD s'engega, la taula es carrega des de la flash i es reconstrueix a la RAM del SSD.

![](imgs/ssd-7c553950.png)

Mapeig de pàgines

---

Podem tenir tres possibilitats de mapatge:
- **A nivell de pàgina**: per a cada **pàgina lògica tenim una entrada a la taula que apunta a una pàgina física**. Tot i que és molt flexible, la taula de mapatge requereix molta RAM. Això pot **augmentar significativament els costos** de fabricació.
- **A nivell de blocs**: per a cada **bloc lògica tenim una entrada a la taula que apunta a un bloc físic,** on totes les pàgines estan en la mateixa posició que al LBA. Tindrem moltes menys entrades a la taula, cosa que millora el problema d'emmagatzematge, però fa empitjorar el rendiment ja que caldrà reescriure els blocs quan es modifiqui una pàgina per a tenir les pàgines ordenades al bloc físic com està al LBA.
- **Híbrid**: Per tant es fa servir un model híbrid anomenat "log-block mapping", que barreja una taula de blocs amb logs de canvis que s'han de fer totes d'una tirada. No parlarem més aquí, però podeu consultar a "4.2 Logical block mapping" https://codecapsule.com/2014/02/12/coding-for-ssds-part-3-pages-blocks-and-the-flash-translation-layer/

---

#### Garbage collection

Per a millorar el rendiment del SSD, donat que les operacions d'esborrat de blocs tenen una alta latència, aquestes s'executaran en segon pla en moments d'inactivitat (**idle garbage collection**) o bé en paral·lel amb operacions d'escriptura.

Es busquen els blocs amb pàgines marcades com a "viciats" (stale), s'esborren i es posen en estat "free".

En el cas que hi hagi una alta càrrega d'operacions al disc, pot passar que no hagi donat temps a esborrar pàgines i per tant calgui aplicar el garbage collection en el mateix moment. Això **farà alentir molt el funcionament del disc**.

Les solucions que es poden aplicar són la **comanada TRIM** i el **sobre-aprovisionament del disc**.

---

### Comanda TRIM (TRIM = "Retallar")

**Quan s'esborren** fitxers d'un sistema d'arxius, **els blocs que utilitzava es declaren lliures** a les taules de fitxers. Però **el controlador de l'SSD no sabrà que s'han alliberat** i només veurà l'espai lliure quan els blocs lògics que contenien els arxius siguin sobreescrits. Cal pensar que pot haver diferents sistemes d'arxius als discs SSD i la controladora no sap quins són.

**En el moment que es sobreescriguin pel sistema operatiu**, el procés de recollida d'escombraries esborrarà els blocs associats amb els fitxers suprimits proporcionant pàgines lliures per a les escriptures entrants. Com a conseqüència, **en lloc d'esborrar els blocs tan aviat com se sap que tenen dades "viciades"** (en realitat ho sap el sistema operatiu però no el controlador del SSD), **l'esborrament es retarda i això baixa el rendiment**.

A més, com que les pàgines que contenen arxius suprimits són desconegudes per al controlador SSD, **el mecanisme de recollida d'escombraries continuarà movent-los per assegurar l'anivellament de desgast**. Això augmentarà l'amplificació d'escriptura, i interferirà amb la càrrega de treball en primer pla de l'amfitrió.

Una **solució al problema de l'esborrat retardat és la comanda "TRIM"**, que pot ser enviada pel sistema operatiu per notificar al controlador SSD que les pàgines ja no s'utilitzen en l'espai lògic. Amb aquesta informació, **el procés de recollida d'escombraries sap que no cal moure aquestes pàgines, i també que pot esborrar-les quan sigui necessari**. L'ordre TRIM només funcionarà si el controlador SSD, el sistema operatiu i el sistema de fitxers ho suporten.

Es pot consultar a la taula https://en.wikipedia.org/wiki/Trim_(computing) quins sistemes operatius i sistemes de fitxers admeten TRIM
- **Sota Linux**, el suport per al ATA TRIM es va afegir a la versió 2.6.33. Encara que els sistemes de fitxers ext2 i ext3 no admeten TRIM, ext4 i XFS, entre d'altres, ho admeten.
- **Sota Mac OS** 10.6.8, HFS+ suporta l'operació TRIM.
- El Windows 7, només suporta TRIM per a SSD utilitzant una interfície SATA i no PCI-Express.

La **majoria de les unitats recents suporten TRIM**, permetent que la recollida d'escombraries funcioni tan aviat com sigui possible per millorar significativament el rendiment futur. Per tant, és molt preferible utilitzar SSDs que suporten TRIM, i assegurar-se que el suport està habilitat tant en el sistema operatiu com en el sistema de fitxers.

El sistema operatiu pot ser que no envii l'ordre TRIM al disc de manera immediata, de manera que apagar l'equip pot evitar que l'ordre s'enviï. De tota manera, un cop rebuda l'ordre, **no hi ha un mecanisme per inhabilitar l'execució de les comandes TRIM per part del disc SSD** i s'esborraran les dades sense remei.

TRIM - https://es.wikipedia.org/wiki/TRIM
- https://datarecovery.com/rd/trim-command-solid-state-drives/

---

#### TRIM en Windows

Requisits per a usar TRIM (Windows 7, 8 o 10):
- Un disc dur SSD amb el firmware TRIM habilitat.
- La BIOS o UEFI ha d'estar habilitada en la manera AHCI/SATA.
- Usar NTFS o ReFS (sistema d'arxius resistent)

Per comprovar l'estat de TRIM:
~~~
C:\>fsutil behavior query DisableDeleteNotify
NTFS DisableDeleteNotify = 0  (Deshabilitado)
ReFS DisableDeleteNotify = 0  (Deshabilitado)

(0= TRIM habilitat!!)
~~~

Desactivar TRIM (1) / Activar TRIM (0)  (cal consola d'administrador):
~~~
fsutil behavior set DisableDeleteNotify 0
~~~

També es pot activar TRIM en mode gràfic:
- En HDD l'optimització consisteix en fer una defragmentació que mai s'ha de fer en un SSD.
- En SSD l'optimització consisteix en enviar la comada TRIM per alliberar pàgines de fitxers esborrats. Es recomanable fer-ho cada setmana en un ús normal.

![](imgs/ssd-78e8f2f7.png)

Opció optimitzar del disc en Windows.

Consultar:
- https://www.solvetic.com/tutoriales/article/3164-comprobar-y-habilitar-trim-ssd-windows-10-8-7/

---

#### TRIM en Linux

Requisits per a usar TRIM en Linux:

- El sistema operatiu ha de suportar TRIM. Totes les distros Linux amb un kernel igual o posterior al 2.6.28 suporten TRIM.
- Un disc dur SSD amb el firmware TRIM habilitat. La totalitat d'unitats SSD actuals tenen ple suport.
- El sistema d'arxius ha de ser btrfs, ext3, ext4 gfs2m jfs, vfat, f2fs, ntfs, ocfs2 o xfs.

Tenim dues opcions per a configurar TRIM:
- **Continu**: Executa TRIM cada vegada que s'esborra un fitxer. No es recomanable per que pot baixar el rendiment i no podrem recuperar arxius esborrats per error.
- **Periòdic**: Executarà TRIM una vegada cada cert temps. Recomanable en un ús normal, cada setmana. Això envia totes les operacions de TRIM d'una tacada.

Consulteu:   
- Activar TRIM en Linux - https://geekland.eu/activar-trim-correctemente-linux/
- Optimització SSD en Debian - https://wiki.debian.org/SSDOptimization
- SSD Archlinux - https://wiki.archlinux.org/title/Solid_state_drive

---

### El sobre-aprovisionament

Aquesta tècnica és simplement **tenir més blocs físics que blocs lògics**, mantenint una proporció dels blocs físics reservats per al controlador i no visibles per a l'usuari. La majoria dels fabricants de SSD professionals ja inclouen sobre-aprovisionament, generalment de l'ordre del 7% al 25%.

Els usuaris poden crear sobre-aprovisionament simplement **particionant el disc a una capacitat lògica menor que la seva capacitat física màxima**. Per exemple, es podria crear una **partició de 90 GB en una unitat de 100 GB**, i deixar 10 GB per sobre-aprovisionament.

El controlador SSD podrà fer servir les pàgines no utilitzades per les operacions de moure pàgines. També substituiran a les pàgines que s'estan desgastant a l'espai usat.

**IMPORTANT:** Si es fa sobre-aprovisionament, no caldrà aplicar TRIM perque hi haurà blocs lliures per al garbage collection.

---

## Forense SSD

S'ha demostrat que **un SSD pot canviar amb el temps**: en crear una imatge d'un SSD i calcular el hash, l'evidència digital SSD canvia i amb el temps s'obté un valor de hash diferent.

Aquests **canvis es produeixen inclús fent servir blocadors d'escriptura**, ja que són interns a la controladora del SSD i es deuen a:
- la recol·lecció d'escombraries (garbage collection).
- ús de la comanda TRIM.
- la varietat de l'agressivitat del recol·lector d'escombraries de cada fabricant (es pot tenir més o menys temps abans que es faci el TRIM). Per a alguns sistemes de fitxers amb TRIM activats, les dades suprimides no es poden recuperar després d'1 hora del disc en funcionament.

Avui dia la fabricació de SSDs augmenta, el que indica que **cada vegada seran més present en les investigacions forenses digitals**. El problema principal és que hi ha una idea preconcebuda que es tracta de mitjans d'emmagatzematge i, per tant, es tracten com a HDD tradicional. Aquest tractament de les proves digitals **podria posar en perill la integritat i la seva admissibilitat en un tribunal**.

A més, els sistemes de fitxers amb **TRIM activat poden netejar un SSD en menys d'un minut**, de manera que totes les cel·les estaran a 0.
- Això és ideal per a un ràpid sanejament i l'eliminació permanent de totes les dades sense possibilitat de recuperació.
- La **irrecuperabilitat és molt atractiva per als criminals**, que podrien fer un esborrat de totes les dades de forma remota (per exemple un format ràpid), cosa que envia les comandes TRIM des del SOP al SSD.

**Recomanacions**:
1. **Apagar estirant el cable** i no tancant l'ordinador "correctament" per evitar que es pugui enviar les ordres TRIM al SDD. **No es pot desactivar l'aplicació de les comandes TRIM un cop el SSD les ha rebut**.
2. **Extreure el dispositiu** amb les mesures de protecció antiestàtiques i guardar-ho en bosses antiestàtiques.
3. Posar en mans d'experts especialitzats el procès de crear la imatge bit a bit del disc.
4. **Treballar sempre amb un sistema operatiu amb TRIM desactivat** si hem de connectar el disc a una estació forense.


S'estan desenvolupant nous mètodes per a preservar les evidències SSD, per exemple: **Deconstruct and Preserve (DaP)**: A method for
the preservation of digital evidence on Solid
State Drives (SSD) - https://eprints.mdx.ac.uk/20792/1/mitchell_et_al_1.pdf

---

## Referències

- 5 cosas TERRIBLES que no sabías sobre tus SSD - https://www.youtube.com/watch?v=VMwhHmolWhs

- 5 cosas que NUNCA debes hacer con tu SSD - https://www.youtube.com/watch?v=ORczERjAwRc&t=0s

- ¡Haz que Windows DESPEGUE! - Protege tu SSD con un RAMDisk - https://www.youtube.com/watch?v=_f5SlCHQIA4

- Qué es TRIM y por qué debemos activarlo en nuestro SSD - https://geekland.eu/trim-debemos-activarlo-ssd/
  - https://geekland.eu/activar-trim-correctemente-linux/
  - https://geekland.eu/activar-trim-en-windows/

- Coding for SSDs - Explicació de com funcionen els SSD. Emmanuel Goossaert - https://codecapsule.com/2014/02/12/coding-for-ssds-part-1-introduction-and-table-of-contents/

- Write Amplification i garbage collection - https://en.wikipedia.org/wiki/Write_amplification#BG-GC

- How SSD Data Recovery Differs from Hard Drive Data Recovery - https://datarecovery.com/rd/solid-state-drive-ssd-data-recovery-are-my-files-lost-forever/

- SSDOptimization - https://wiki.debian.org/SSDOptimization

- ReFS vs NTFS - https://hardzone.es/reportajes/que-es/refs-sistema-archivos-windows-ntfs/

- Flash Memory Amnesia – Resurrecting Data through Direct Read of NAND Memory - https://www.gillware.com/flash-drive-data-recovery/flash-memory-amnesia-resurrecting-data-through-direct-read-of-nand-memory/

- Flash memory - https://en.wikipedia.org/wiki/Flash_memory

- Los mejores discos SSD:
  - Tipos de memorias en los discos SSD: 3D MLC, TLC, QLC… - https://www.losmejoresdiscosssd.es/ssd-vs-hdd-informacion-tecnica-sobre-los-ssds/
  - Degradació dels SSD - https://www.losmejoresdiscosssd.es/degradacion-y-retencion-de-datos-en-los-ssd/
  - Disc SSD sense RAM - https://www.losmejoresdiscosssd.es/discos-ssd-dram-less/
  - Mort súbita dels SSD - https://www.losmejoresdiscosssd.es/muerte-subita-en-los-discos-ssd/

- Recuperació de dades Gillware - https://www.gillware.com/
