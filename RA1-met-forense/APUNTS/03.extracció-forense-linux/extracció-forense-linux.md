# Eines d'extracció d'evidències en Linux

**INS Carles Vallbona**

**Pau Tomé**

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Eines d'extracció Linux](#eines-dextracci-linux)
- [Captació d'evidències volàtils en viu](#captaci-devidncies-voltils-en-viu)
	- [lsof](#lsof)
		- [Exemples](#exemples)
		- [Trobar espais ocults al disc](#trobar-espais-ocults-al-disc)
	- [Recopilar informació d'un sistema en marxa](#recopilar-informaci-dun-sistema-en-marxa)
	- [Adquirir memòria en Linux](#adquirir-memria-en-linux)
- [Adquirir imatge de disc](#adquirir-imatge-de-disc)
	- [Eina dd (“Data Duplicator” o “Disk Dump”...)](#eina-dd-data-duplicator-o-disk-dump)
		- [Fer imatge en local](#fer-imatge-en-local)
		- [Enviar imatge per xarxa](#enviar-imatge-per-xarxa)
		- [Treball amb blocs](#treball-amb-blocs)
		- [Crear dispositiu net](#crear-dispositiu-net)
	- [Altres eines](#altres-eines)

<!-- /TOC -->

## Adquisició d'evidències volàtils en viu

Veurem una sèrie de comandes per obtenir informació en viu de sistemes Linux.

### lsof

"ls open files": Llistat d'arxius oberts i per quin procés. Permet passar com a paràmetre:
- fitxer regular
- directori
- fitxer especial de blocs (discs)
- fitxer especial de caràcters (teclat, consola...)
- referència de codi en execució
- biblioteca
- "stream"
- fitxer de xarxa (socket)

Més informació: https://www.geeksforgeeks.org/unix-file-system/

#### Exemples

- **lsof**: Tots els fitxers que pertanyen a tots els processos actius
- **lsof -i [ipaddress]**: Connexions Internet d'una adreça IP determinada
- **lsof -i 4 -a -p 1234**:  Tots els fitxers de xarxa IPv4 en ús pel procès amb PID 1234
- **lsof /dev/hda3**:  Tots els fitxers oberts del dispositiu /dev/hda3
- **lsof /u/abe/foo**: Tots els processos que tenen obert el fitxer /u/abe/foo

### Trobar espais ocults al disc

Els fitxers en Linux es referencien mitjançant enllaços. Un mateix fitxer pot tenir més d'un enllaç però ocupa l'espai només una vegada al disc. Quan un fitxer no té enllaços que l'apuntin, aquest fitxer es declara esborrat al sistema d'arxius.

Però si es crea un procès que obri un fitxer i s'elimina l'enllaç que apunta al fitxer (s'esborra per exemple via comanda rm), el procès que el tenia obert encara pot continuar escrivint al fitxer i guardar-hi dades, i l'efecte és:
- Els recursos de disc segueixen en ús.
- El fitxer és invisible a la comanda ls.

Per veure els fitxers que tenen un comptador d'enllaços menor que 1 (esborrats):
- lsof +L1

Per veure què conté aquest fitxer ocult:
- Buscar el PID del procès que té obert aquest fitxer amb "lsof"
- Fer: cd /proc/PID/cwd (cwd=current working directory)

---

## Altres comandes per recopilar informació d'un sistema en viu

- **date** : dia i hora actuals
~~~
date
dilluns, 30 de maig de 2022, 20:47:39 CEST
~~~

- **uptime** : Des de quan està el sistema en marxa
~~~
$ uptime
 20:47:11 up  2:13,  1 user,  load average: 0,48, 0,18, 0,21
~~~

- **uname -a** : informació tècnica del sistema
~~~
$ uname -a
Linux Equip01 5.13.0-44-generic #49~20.04.1-Ubuntu SMP Wed May 18 18:44:28 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
~~~

- **ifconfig**, **ip** : targetes de xarxa i si estan o no en mode promiscu

- **ps -eaf o top** : veure processos i serveis poc usuals

- **netstat -at** o **lsof -i 4** : veure connexions TCPv4 des de i cap a aquest ordinador.

- **w o who o users** : Usuaris amb sessió iniciada

- **find / -uid 0 -perm -4000 2>/dev/null** : Buscar programes SUID

- **tail -f var/log/messages** : Veure els logs

- **find /directory_path -type f -a=x -mtime -1** : Fitxers executables modificats l'últim dia

---

## Adquisició de memòria RAM en Linux

Sempre s'ha de tenir accés físic al sistema i privilegis de root. Algunes eines que es poden fer servir:
- **Linux Memory Extractor (LiME)**: Actualment la més utilitzada. Es tracta d'un mòdul del Kernel que un cop carregat permet adquirir memòria RAM a Linux o Android. https://github.com/504ensicslabs/lime

- Acquire Volatile Memory for Linux (AVML) - https://github.com/microsoft/avml

- **Fmem**: També és un mòdul del Kernel (fmem.ko) que crea un dispositiu nou anomenat ""/dev/fmem". https://github.com/NateBrune/fmem
  - Per fer la còpia usem: sudo dd if=/dev/fmem of=mem.dd (no hi ha restriccions en posicions de memòria accedides).

- **Memdump**: Per Linux, Unix, FreeBSD, Solaris. Està limitat a un rang d'adreces concret. http://www.porcupine.org/forensics/tct.html

---

## Adquisició d'imatges de disc

Tenim eines natives per a crear imatges de disc a Linux (no limitades a sistemes d'arxius de Linux, es pot crear de qualsevol disc).

### Eina dd (“Data Duplicator” o “Disk Dump”...)

Serveix per a adquirir una imatge sense importar el sistema d'arxius.

#### Fer imatge en local

Ús bàsic de dd, que es pot fer servir per a copiar un dispositiu complet, una partició, un fitxer o una part de qualsevol d'ells.
~~~
dd if=<origen-copia> of=<destí-copia>

// Copiar fitxer
dd if=/home/pep/fitxer.txt of=/mnt/forense/desti.txt

// Copiar dispositiu en dispositiu
dd if=/dev/sda of=/dev/sdf

// Copiar partició en fitxer imatge
dd if=/dev/sda2 of=/mnt/forense/desti.txt
~~~

#### Enviar imatge per xarxa

~~~
// Enviar a un altre host per xarxa
dd if=/dev/sda1 | nc 192.168.1.2 2222

// al destí cal Executar (servidor netcat escoltant)
nc -l 2222 > destí  
~~~

#### Treball amb blocs

Es pot definir blocs de lectura/escriptura. Un bloc gran (sobre 8k) baixa el temps de la còpia.
~~~
bs=n (bytes) entrada i sortida
ibs=n (bytes) entrada
obs=n (bytes) sortida
count=s (blocs) atura la còpia quan arriba a s blocs (de l'entrada)
~~~

Es pot saltar blocs, tant en origen com en destí.
~~~
// Saltar n blocs en entrada (mida definida per ibs)
skip=n
// Saltar m blocs en destí (mida definida per obs)
seek=m
~~~

**ATENCIÓ**: Si dd troba un error de lectura, el procès s'atura. Es pot forçar a continuar i omplir la part errònia amb 0's a la còpia posant els paràmetres:
~~~
conv=noerror,sync
~~~

Exemple: Mida de bloc 1M i mostrar progrès, ignorant errors i omplint amb 0's al destí la part que dona errors de lectura:
~~~
dd if=/dev/sda of=/dev/sdb bs=1024k status=progress conv=noerror,sync
dd if=/dev/sda of=/dev/sdb bs=1M status=progress conv=noerror,sync
~~~

#### Crear un dispositiu net (wipe)

Podem esterilitzar un dispositiu (escrivint 0's o dades aleatòries) amb:
~~~
dd if=/dev/zero of=destí
dd if=/dev/random of=destí
~~~

---

### Altres eines per adquisició d'imatges

#### dcfldd

Última actualització al 2006, desenvolupat pel laboratori forense del Department de Defensa de U.S.A, http://dcfldd.sourceforge.net/
- Permet crear el hash de les dades enviades
- Mostra barra de progrès
- Envia els Logs d'errors a un arxiu de sortida per analitzar-ho

#### dc3dd

Desenvolupat pel Centre de cibercrim del Department de Defensa, Similar a dcfldd però més actualitzat (2016). https://sourceforge.net/projects/dc3dd/files/ - https://www.kali.org/tools/dc3dd/

- hashing "on-the-fly"
- partició en trossos de fitxers
- escriptura de patrons a disc
- Mostra barra de progrès
- Verifica arxius
- Envia els Logs d'errors a un arxiu de sortida per analitzar-ho
