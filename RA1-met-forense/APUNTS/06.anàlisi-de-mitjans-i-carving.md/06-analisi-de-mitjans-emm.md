# Anàlisi de mitjans d'emmagatzemament

**INS Carles Vallbona**

**Pau Tomé**

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Anàlisi de mitjans d'emmagatzemament](#anlisi-de-mitjans-demmagatzemament)
	- [Investigació de mitjans](#investigaci-de-mitjans)
	- [Progressió en l'anàlisi de mitjans](#progressi-en-lanlisi-de-mitjans)
	- [Cerca d'informació de cadenes de text o bytes](#cerca-dinformaci-de-cadenes-de-text-o-bytes)
		- [Expressions regulars](#expressions-regulars)
			- [Exemples amb grep:](#exemples-amb-grep)
			- [Símbols més utilitzats per RegEx](#smbols-ms-utilitzats-per-regex)
			- [Aprendre i comprovar expressions regulars](#aprendre-i-comprovar-expressions-regulars)
	- [Recuperar dades esborrades (buscar a l'espai no assignat)](#recuperar-dades-esborrades-buscar-a-lespai-no-assignat)
		- [1. Buscar Fitxers esborrats a partir de la informació al sistema d'arxius](#1-buscar-fitxers-esborrats-a-partir-de-la-informaci-al-sistema-darxius)
		- [2. Procès de cerca amb Carving (tallar, esculpir)](#2-procs-de-cerca-amb-carving-tallar-esculpir)
			- [Eines de Carving](#eines-de-carving)
		- [3. Buscar a l'espai sobrant als blocs de dades i altres estructures (Slack Space)](#3-buscar-a-lespai-sobrant-als-blocs-de-dades-i-altres-estructures-slack-space)
	- [4. Buscar a l'espai assignat. Fitxers i artefactes.](#4-buscar-a-lespai-assignat-fitxers-i-artefactes)
	- [Referències](#referncies)

<!-- /TOC -->

## Investigació de mitjans

Analitzarem dispositius d'emmagatzematge físics com discs durs, discs SSD, pendrives, discos òptics i memòries flash.

Investigarem i buscarem als dispositius:

1. **Espai assignat amb contingut (fitxers existents) o "Allocated space"** amb espai assignat. El sistema d'arxius reconeix aquest espai con utilitzat.
2. **Espai no assignat "Unallocated space"**. No està ocupat per arxius, tot i que pot contenir dades d'algun arxiu prèviament esborrat. Aquests clústers o blocs estan disponibles per a fitxers.
3. **Espai desaprofitat "Slack space"**, que és l'espai que sobra en un clúster o bloc per que el fitxer que s'hi guarda no l'omple completament.
4. **Sectors/blocs/clústers marcats com erronis o "Bad blocks"**. En principi són blocs defectuosos que el sistema ha descartat per no usar-los, però poden haver siguts marcats així per algú malintencionat per a amagar informació.
5. **Zones del disc amagades** o usades pel sistema.

---

## Progressió en l'anàlisi de mitjans

La progressió en l'anàlisi de mitjans (segons Brian Carrier https://www.basistech.com/management/dr-brian-carrier/) aniria així:

1. **Disc:** És l'emmagatzematge físic a dispositius com ara discs durs, SSD o dispositius flash.
2. **Volum:** És un contenidor que comprèn 1 o més discos. Es pot confondre amb el terme partició, però la partició queda confinada a un sol disc físic, mentre el volum pot abastar més d'un disc físic o diferents parts de diferents discs.
3. **Sistema d'arxius:** S'utilitza per a gestionar fitxers dintre un volum. Controla per exemple la creació de fitxers i l'espai lliure i ocupat.
4. **Unitat de dades:** La unitat mínima d'informació en el sistema de fitxers. En Unix s'anomenen blocs i en Windows clústers o unitats d'assignació.
5. **Metadades:** Les dades sobre les dades, o sigui, dades que descriuen com són les dades emmagatzemades. Inclou informació com ara marques de temps de modificació, accés, i creació, així com d'altres informacions per a gestionar els fitxers.

La finalitat de l'anàlisi de mitjans és trobar **artefactes** rellevants per a **provar o rebatre les hipòtesis** que haguem construït.

---

## Cerca d'informació de cadenes de text o bytes

És el mètode bàsic que podem fer servir per a investigar a una imatge de disc i/o memòria.

Podem fer servir llistes de paraules clau:
- **Genèriques**: Útils a qualsevol cas. Ex: llistes de conceptes relacionades amb un tipus d'investigació, com ara activitats fraudulentes (comissió, pagament, contracte, diner B...) o una altra per a investigació d'imatges il·lícites (porno, babies, etc).
- **Específiques**: Fetes a mida pel cas actual. Ex: usuaris participants, llocs, el llenguatge que fan servir els participants ("jerga"), etc.

Ens podem trobar diferents sistemes de codificació:
- **ASCII(American Standard Code for Information Exchange)**: limitat a 256 codis i en principi per a llenguatge anglès (1 byte).
- **Unicode**: desenvolupat per superar les limitacions d'ASCII. Cada caràcter ocupa 2 bytes, i per tant 65536 símbols diferents.

---

### Expressions regulars

Quan cerquem cadenes podem **crear patrons de cerca amb les expressions regulars (RegEx)**. D'aquesta manera obtenim flexibilitat en les cerques (per exemple per ignorar majúscules i minúscules, paraules que comencen per una arrel...).

Una expressió regular una cadena de caràcters per generar un patró (pattern). Segons la utilitat farem servir uns símbols o altres, tot i que hi ha coses comunes. Cal mirar la documentació de l'eina.

#### Exemples amb grep:

Cercar una adreça IP:
~~~
$ cat exemple.txt
hola
que tal
192.168.0
dasd
192.168.0.255
sdfjjlksdf
sdfñlfsdjk
10.8.1.1

$ grep -E "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}" exemple.txt
192.168.0.255
10.8.1.1

$ grep -E "([0-9]{1,3}\.){3}[0-9]{1,3}" exemple.txt
192.168.0.255
10.8.1.1

$ grep -E "([[:digit:]]{1,3}\.){3}[[:digit:]]{1,3}" exemple.txt
192.168.0.255
10.8.1.1
~~~

Cercar paraules amb uns caràcters o no:
~~~
$ cat ex2.txt
pato
p3to
p4to
plto
p0to
peto
pito
poto
puto
$ grep -E "p[ia]to" ex2.txt
pato
pito
$ grep -E "p[^ia]to" ex2.txt
p3to
p4to
plto
p0to
peto
poto
puto
~~~

Validar un email
~~~
[a-zA-Z0-9_.+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}
~~~

Validar un telèfon
~~~
^\+?([0-9]{1,3})?\s?\(?[0-9]{3}\)?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}$
~~~

#### Símbols més utilitzats per RegEx

- . Representa qualsevol caràcter, una vegada.
- \* Indica que l'anterior símbol pot estar de 0 a infinites vegades. Ex: ca*t podria ser cat, caat, caaat, etc.
- \* Indica que l'anterior símbol pot NO estar. Ex: \.e01? podria ser ".e01" o ".e0"
- \+ Indica que l'anterior símbol pot estar de mínim 1 a infinites vegades.
- \ Indica que el següent caràcter es pren de forma literal. Ex: \\. vol dir que no s'interpreti l'acció associada al '.' si no un '.' literal. Ex: "fin\\". és l'expressió "fin." i posar \\\hola seria l'expressió "\\hola" indicant que volem el símbol "\\" literal.
- ^ Indica principi de línia. Ex: ^123 mostra les línies que comencen per 123.
- $ Indica final de línia. Ex: 123$ mostra les línies que acaben per 123.
- {n} Indica que l'anterior símbol pot estar n vegades. Ex: a{3} és "aaa"
- {n,m} Indica que l'anterior símbol pot estar de n a m vegades. Ex: a{1,3} és "a", "aa", "aaa".
- (...) Agrupa símbols i expressions. Ex: (ab){2} seria abab. ((ab){2}) ((ab){2}) seria "abab abab" (amb espai al mig).
- [...] Indica que pot ser un dels símbols entre els "claudàtors". Ex: p[aeiou]to pot ser "pato", "peto", "pito", "poto", "puto".
- [^...] Indica que no pot ser un dels símbols entre els "claudàtors". Ex: p[^aei]to pot ser "poto", "puto", "p3to", "pgto", etc. però no pot ser "pato", "peto", "pito".
- [...-...] Indica que el símbol està dintre del rang marcat pels caràcters entre el guió. Ex: [a-z] és només un símbol de 'a' a 'z'. [a-zA-Z] és només un símbol de 'a' a 'z' o de 'A' a 'Z'.

**ATENCIÓ:** El rang depèn de la codificació dels símbols, de manera que [0-9] són tots els símbols de ASCII del 48 a 57 i no es pot escriure com [1-0] per que el darrer símbol te un codi menor que el primer. Cal mirar la taula ASCII i/o Unicode abans https://elcodigoascii.com.ar/ i https://www.ssec.wisc.edu/~tomw/java/unicode.html

---

#### Aprendre i comprovar expressions regulars

- Expressions regulars (grep i d'altres). Per a aprendre i fer proves de forma visual
  - https://regexr.com/
  - https://regex101.com/
- Joc per aprendre (reptes en forma d'història) - https://www.therobinlord.com/projects/slash-escape
- Exemples de RegEx - https://patchthenet.com/articles/a-quick-guide-to-regular-expressions/

---

## Recuperar dades esborrades (buscar a l'espai no assignat)

### 1. Buscar Fitxers esborrats a partir de la informació al sistema d'arxius

Quan un fitxer és esborrat en diferents sistemes de fitxers, normalment:
1. Només s'esborra l'entrada de la taula de directoris o arxius el seu nom o la inicial del nom en FAT (ja que només sobreescriu el primer caràcter del fitxer posant el codi hexa 0xE5 que indica entrada de directori lliure).
2. S'esborra l'enllaç al primer bloc del fitxer. Amb FAT no s'esborra i es pot anar a buscar. També a vegades el fitxer és tan petit que es troba a la pròpia taula de fitxers con ara al sistema NTFS que es guarda a la MFT (Master File Table). Aquests fitxers es diuen "residents".
3. Es declaren lliures els blocs que el fitxer ocupava, a la taula de blocs ocupats i lliures (és un bitmap normalment).
4. S'esborren els enllaços a la resta de blocs assignats al fitxer (depenent de com es gestiona el sistema d'arxius).

Per tant, per recuperar els arxius cal fer el procès invers al que el sistema de fitxers ha fet per esborrar-los. És un procés depenent del sistema de fitxers.

1. Buscar entrades de directori que s'han utilitzat abans.
2. Llegir l'adreça del primer bloc del fitxer.
3. Anar llegint i guardant en un fitxer nou el contingut dels blocs consecutius mentre no estiguin assignats.
4. Aturar-se quan es troba la marca de final de fitxer (EOF) o bé quan el següent bloc està assignat a un fitxer. En aquest últim cas, el fitxer podria estar fragmentat o bé s'han assignat els seus blocs a un altre fitxer (i sobreescrit la informació).

---

### 2. Procès de cerca amb Carving (tallar, esculpir)

Aquest procés és utilitzat principalment en la informàtica forense per a extreure informació a partir d'una quantitat de dades en brut sense necessitat de conèixer el sistema de fitxers amb el qual s'han creat els fitxers.

En general, tots els tipus de fitxers tenen característiques comunes. Per exemple, atenent la seva estructura, tots els fitxers JPG/*JFIF comencen per FF D8 FF E0 i acaben per FF D9. Tots els tipus de fitxers tenen una estructura similar. Utilitzen una constant coneguda com Magic Number, la qual permet identificar el corresponent tipus de fitxer. http://www.garykessler.net/library/file_sigs.html

Existeixen diferents tècniques de «file carving»:
- Basades en la capçalera d'un fitxer i en el final o, si es desconeix aquest, en la grandària màxima d'arxiu (dada disponible en la capçalera).
- Basades en l'estructura d'un fitxer: capçalera, peu, cadenes significatives, grandària, etc.
- Basades en el contingut del fitxer: entropia, reconeixement del llenguatge, atributs estàtics, etc. Exemple: HTML, XML, etc.


Alguns fitxers també tenen marca de final de fitxer, així com capçalera, o guarden la mida del fitxer a la capçalera.

![](imgs/06-analisi-de-mitjans-emm-6dfd4cac.png)

File Carving

---

D'aquests fitxers no podrem saber el nom ni les metadades del sistema d'arxius ni tampoc l'espai d'emmagatzematge real que ocupa, a no ser que part d'aquesta informació es guardi al mateix fitxer com una metadada del seu contingut. Un exemple són els fitxers de fotos que contenen informació EXIF. https://www.ionos.es/digitalguide/paginas-web/diseno-web/que-son-los-datos-exif/

Això implica que s'ha de conèixer quin és el format dels diferents tipus d'arxius que es poden guardar al disc. Bàsicament quina capçalera, contingut i cua tenen els arxius.

![](imgs/06-analisi-de-mitjans-emm-4878e17f.png)

Format de fitxer Jpeg http://www.institutopoincare.com.br/site/index.php?option=com_content&view=article&id=30:datacarving&catid=8&Itemid=126

---

Aquest mètode permet extreure fitxers continguts a dintre d'altres fitxers o simplement concatenats a la cua del fitxer "contenidor".

Exemple, crear un jpg que contingui a dintre dues imatges tot i que només veiem una:
~~~
cat fitxer1.jpg fitxer2.jpg > fitxer3.jpg
~~~

#### Eines de Carving

- Exemple carving manual - https://forensicallyfit.wordpress.com/2018/08/21/manual-file-carving/
- File Carving incibe - https://www.incibe-cert.es/blog/file-carving
- Explicació i Exemples de carving - https://resources.infosecinstitute.com/topic/file-carving/

Llista:
- Bulk Extractor - https://github.com/simsong/bulk_extractor
- Foremost - http://foremost.sourceforge.net/
- Scalpel - https://github.com/sleuthkit/scalpel
- Forensic Toolkit (FTK) AccessData
- Encase
- PhotoRec
- Revit
- TestDisk
- Magic Rescue
- F-Engrave

---

Més informació
- File Carving INCIBE - https://www.incibe-cert.es/blog/file-carving
- https://www.forensicfocus.com/articles/a-survey-on-data-carving-in-digital-forensics/
- File Carving - El hacker.net - https://blog.elhacker.net/2010/04/file-carving.html
---

### 3. Buscar a l'espai sobrant als blocs de dades i altres estructures (Slack Space)

Els sistemes d'arxius treballen amb una unitat mínima d'emmagatzematge anomenada **"bloc"** en Linux i **"clúster" o "unitat d'assignació"** en Windows. Aquesta unitat mínima d'assignació correspon a una agrupació de sectors. Els sectors es gestionen i s'accedixen en grups per motius d'eficiència.

Quan un arxiu demana espai, se li **assigna com a mínim una unitat d'assignació completa**. Tots els arxius desaprofiten una part de l'última unitat d'assignació. Això fa que el tros que no s'utilitza pugui contenir un porció de dades guardades per un fitxer anterior que va ser esborrat i els seus blocs alliberats per ús posterior. Aquestes dades podrien ser una prova per un cas.

![](imgs/06-analisi-de-mitjans-emm-a88d876e.png)

Slack space a un clúster

També pot quedar espai sobrant a les particions, zones no accessibles del disc i altres.

---

## 4. Buscar a l'espai assignat. Fitxers i artefactes.

A l'espai assignat tenim tots els fitxers de dades i del sistema. Aquests fitxers del sistema configuren els anomenats "Artefactes".

Els artefactes de Windows són els objectes que contenen informació sobre les activitats que realitza l'usuari de Windows. El tipus d'informació i la ubicació de l'artefacte varia d'un sistema operatiu a un altre. Els artefactes de Windows contenen informació sensible que es recull i analitza en el moment de l'anàlisi forense.

Cal localitzar-los físicament al disc o a memòria, on també trobem còpies dels artefactes (còpies temporals de treball, normalment).

En Windows tenim un munt d'artefactes que ens permeten investigar:
- Perfils d'usuari (User Profiles).
- Registre de Windows (Windows Registry).
- Comptes d'usuaris i el seu ús.
- Informació sobre arxius.
- Informació sobre execució de programes.
- Informació sobre dispositius USB connectats.

Aquests artefactes tenen formes diferents segons la versió del sistema Windows.

---

## Referències

- Fragview's main purpose is to visualize disk content by displaying its map. - https://github.com/i-rinat/fragview

- Klennet Carver i explicació carving 	- https://www.klennet.com/carver/overview.aspx

- Soft carving cnw recovery - https://www.cnwrecovery.com/
