# Registre de windows (hives)

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Registre de windows (hives)](#registre-de-windows-hives)
	- [Ús del registre en anàlisi forense](#s-del-registre-en-anlisi-forense)
	- [Definició](#definici)
	- [Els Hives](#els-hives)
		- [Hive de perfil d'usuari](#hive-de-perfil-dusuari)
	- [Formats de Hive](#formats-de-hive)
	- [Fitxers de suport del registre de Windows](#fitxers-de-suport-del-registre-de-windows)
	- [Hives estàndard](#hives-estndard)
	- [Tipus de valors guardats al registre](#tipus-de-valors-guardats-al-registre)
	- [Referències](#referncies)

<!-- /TOC -->

---

## Ús del registre en anàlisi forense

El registre de Windows conté informació que és útil durant una anàlisi forense i és una font excel·lent per trobar evidències. Conèixer el tipus d'informació que podria existir en el registre i la seva ubicació és crític durant el procés d'anàlisi forense.

## Definició

**Registre de Windows:** És una base de dades jeràrquica central utilitzada en Windows 98, Windows CE, Windows NT i Windows 2000 per emmagatzemar informació necessària per configurar el sistema per a un o més usuaris, aplicacions i dispositius de maquinari.

El Registre **conté informació que Windows referencia contínuament**, com ara:
- perfils per a cada usuari
- les aplicacions instal·lades a l'ordinador
- els tipus de documents que cada aplicació pot crear
- la configuració del full de propietats per a carpetes i icones d'aplicació
- el maquinari que existeix al sistema
- els ports que s'estan utilitzant...

Encara que el Registre és comú en diversos sistemes operatius Windows, hi ha algunes diferències entre ells.

---

## Els Hives

El **registre de Windows es composa de "hives"** (ruscs en català i en castellà, colmena)

**Hive:** Un hive (rusc) és un **grup lògic de claus, subclaus i valors en el registre que té un conjunt de fitxers de suport** que es carreguen a memòria RAM quan s'inicia el sistema operatiu o un usuari inicia sessió.

### Hive de perfil d'usuari

Cada cop que un nou usuari inicia sessió per primera vegada a un ordinador, la seva carpeta personal que es troba a:
- **\Users\usuari\** per a versions actuals de Windows
- **\Documents and Settings\usuari** per XP, NT i Win2000.

Es crea un nou "hive" per a aquest usuari amb un fitxer **"NTuser.dat" per al perfil d'usuari**. El "hive" d'un usuari conté informació de registre específica que pertany a la configuració de:
- les aplicacions de l'usuari
- l'escriptori
- l'entorn
- les connexions de xarxa i les impressores.

Els "hive's" del **perfil d'usuari es troben sota la clau HKEY_USERS**.

---

Microsoft defineix aquests tipus de perfils d'usuari (configurables en entorns locals i en entorns de servidor):
- **Perfil Local d'usuari**: Aquest perfil es crea al disc local. Els canvis seran específics per a l'usuari i s'emmagatzemaran en l'ordinador local. **(NTuser.dat)**

- **Perfil mòbil d'usuari ("roaming")**: Aquest perfil és creat per l'administrador i basat en un perfil de xarxa, que es baixarà al host local quan l'usuari iniciï sessió al sistema. **Els canvis es fan en l'equip local, però es farà a la còpia del servidor quan l'usuari tanqui la sessió**. Amb aquest tipus de perfil, l'usuari tindrà el mateix perfil quan s'entra des de diferents equips de la xarxa. (Això només es troba a Entorns empresarials.)

- **Perfil d'usuari obligatori (mandatori)**: Aquest perfil és un **perfil mòbil** creat per l'administrador per blocar als usuaris un conjunt específic de paràmetres en els hosts de la xarxa. **No es permet a l'usuari fer canvis al perfil**. Qualsevol canvi fet per l'usuari a l'entorn es perdrà. **(NTuser.man)**. https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/bb776895(v=vs.85)
- **Perfil d'usuari Temporal**: Es crea quan es produeix un error quan el sistema està carregant el perfil de l'usuari. **Quan l'usuari surt, el perfil s'esborra** (Windows 2000 i posteriors).

---

La **carpeta personal de l'usuari** es crea amb els següents directoris, ntre d'altres:
- \Users\$USER$\Documents
- \Users\$USER$\Music
- \Users\$USER$\Pictures
- \Users\$USER$\Videos
- \Users\$USER$\AppData (oculta)

![](imgs/08-registre-windows-dcda4905.png)

La carpeta oculta "**AppData**" conté preferències i configuracions específiques de l'usuari. Conté les subcarpetes:

![](imgs/08-registre-windows-36e2ebd6.png)

**\Users\$USER$\AppData\Roaming**: Per a dades de perfil mòbil.
- \Users\$USER$\AppData\Roaming\Microsoft\Windows\Cookies
- \Users\$USER$\AppData\Roaming\Microsoft\Windows\Network Shortcuts
- \Users\$USER$\AppData\Roaming\Microsoft\Windows\Printer Shortcuts
- \Users\$USER$\AppData\Roaming\Microsoft\Windows\Recent
- \Users\$USER$\AppData\Roaming\Microsoft\Windows\SendTo
- \Users\$USER$\AppData\Roaming\Microsoft\Windows\Start Menu
- \Users\$USER$\AppData\Roaming\Microsoft\Windows\Templates

![](imgs/08-registre-windows-c8066c20.png)

**\Users\$USER$\AppData\Local**: conté dades relacionades amb la instal·lació de programes, específic de l'equip local i no es sincronitza amb el servidor (en un entorn amb servidor). També es guarden dades temporals, com ara caches:
- \Users\$USER$\AppData\Local
- \Users\$USER$\AppData\Local\Microsoft\Windows\History
- \Users\$USER$\AppData\Local\Microsoft\Windows\Temporary Internet Files

![](imgs/08-registre-windows-88c757f1.png)

**\Users\$USER$\AppData\LocalLow**: conté dades d'accés de baix nivell, com ara arxius temporals de l'explorador funcionant en mode protegit.

![](imgs/08-registre-windows-8352a990.png)

---

## Formats de Hive

Els arxius de registre tenen els dos formats següents:
- **estàndard:** l'únic format compatible amb Windows 2000. També està suportat per versions posteriors de Windows per compatibilitat amb versions anteriors.
- **"més recent":** Compatible amb Windows XP. En les versions de Windows que suporten el format més recent, **els següents "hive"s encara utilitzen el format estàndard: HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE\SAM, HKEY_LOCAL_MACHINE\Security, i HKEY_USERS\.DEFAULT**; tots els altres "hives" utilitzen el format més recent.

---

## Fitxers de suport del registre de Windows

La majoria dels fitxers de suport per als "hive" es troben al directori **%SystemRoot%\System32\Config**. Aquests fitxers s'actualitzen cada vegada que un usuari inicia la sessió. Les extensions de nom de fitxer dels fitxers d'aquests directoris, o en alguns casos la manca d'una extensió, indiquen el tipus de dades que contenen.

- **sense extensió**: Còpia completa de les dades del "hive" en concret.
- **.alt:** Còpia de seguretat del "hive" HKEY_LOCAL_MACHINE\System hive que es considera crític. Només aquest te un fitxer .alt.
- **.log:** És un **log de transaccions dels canvis fets a les claus i valors del "hive"**. Serveix per poder reproduir tots els canvis, fins i tot els que no s'han pogut registrar al fitxer del hive. Així es podran refer en reiniciar. Cal tenir-los en compte en el forense i aplicar-los al hive si aquest no està correctament actualitzat. En el forense, **Registry Explorer** se n'adona i permet incorporar-los si es vol.

---

Explicació a: https://www.mandiant.com/resources/digging-up-the-past-windows-registry-forensics-revisited

---

- **.sav:** Còpia de seguretat d'un "hive". En Windows, el setup te dues etapes: primer mode text i després mode gràfic. Windows Server 2003 i Windows XP/2000 copien els fitxers de "hive" tal i com estan al final de l'etapa de setup en mode text per protegir-ho d'errors que poden produir-se si l'etapa en mode gràfic falla. En cas de fallada, només es torna a repetir l'etapa en mode gràfic quan l'equip es reinicia i el fitxer .sav s'utilitza per a restaurar les dades del "hive" per tornar a començar.

Cada 10 dies Windows fa còpia dels hives a **C:\Windows\System32\Config\RegBack** de manera que si algú ha modificat els hives o hi ha errors, aquí podríem trobar versions anteriors. De totes maneres, a partir de la versión 1803 de Windows 10, ja no es fa còpia de seguretat del registre de forma automàtica en la carpeta RegBack. https://docs.microsoft.com/es-es/windows/client-management/advanced-troubleshooting-boot-problems

---

## Hives estàndard

Es pot visualitzar el **registre de Windows** amb l'eina **regedit.exe**:

![](imgs/registre-windows-b00e7771.png)

Els 5 hives principals de Windows, desplegats en regedit

---

Aquesta és la llista dels "hives" estàndard i els fitxers que els suporten que es troben a:
- **%Systemroot%\System32\Config**
- els perfils d'usuari que es troben a **\Users\usuari\ntuser.dat** i **\Users\usuari\AppData\Local\Microsoft\Windows** com arxius ocults).
- **C:\Windows\AppCompat\Programs\Amcache.hve**: Un altre hive important que serveix per guardar la informació dels programes que s'han executat recentment.

El tamany màxim del **nom d'una clau és de 255 caràcters**. Aquestes són les claus predefinides utilitzades pel sistema.

| Clau                        | Explicació        |
|-----------------------------|:------------------  
| HKEY_CURRENT_USER | Informació de l'usuari que ha iniciat sessió: les carpetes d'usuari, colors de la pantalla, configuració del panell de control, etc. S'associa al perfil (profile) d'usuari. |  
| HKEY_USERS | Conté tots els perfils d'usuari carregats activament (sessions obertes). HKEY_CURRENT_USER és una subclau de HKEY_USERS. |
| HKEY_LOCAL_MACHINE |  Conté informació de configuració de l'equip particular per a tots els usuaris. |
| HKEY_CLASSES_ROOT | Subclau de HKEY_LOCAL_MACHINE\Software. Es guarda quin programa obre cada tipus d'arxiu des de Windows Explorer. Abreviat HKCR. Des de Windows 2000, s'emmagatzema aquesta informació dintre les claus HKEY_LOCAL_MACHINE i HKEY_CURRENT_USER. |
| HKEY_LOCAL_MACHINE\Software\Classes | Conté configuració per defecte que s'aplica a tots els usuaris de la màquina local. |
| HKEY_CURRENT_USER\Software\Classes | Conté configuracions que sobrescriuen els paràmetres per defecte i aplica els particulars de l'usuari. |
| HKEY_CLASSES_ROOT | Mostra una vista del registre que combina la informació de les dues fonts de configuració. També mostra aquesta vista combinada per programes dissenyats per a versions anteriors de Windows. |
| HKEY_CURRENT_CONFIG |	Conté informació sobre el perfil de maquinari que usa l'equip local en iniciar-se.

---

| Clau                        | Abrev.        | Fitxers |
|-----------------------------|:-------------:|:------------|
| HKEY_CURRENT_USER           | HKCU	        | Ntuser.dat, Ntuser.dat.log (al perfil de cada usuari) |
| HKEY_CURRENT_USER\Software\CLASSES   | HKCU\Software\CLASSES  | UsrClass.dat (\Users\usuari\AppData\Local\Microsoft\Windows) |
| HKEY_LOCAL_MACHINE\SAM      | HKLM\SAM      |	Sam, Sam.log, Sam.sav |
| HKEY_LOCAL_MACHINE\Security | HKLM\Security |	Security, Security.log, Security.sav |
| HKEY_LOCAL_MACHINE\Software | HKLM\Software |	Software, Software.log, Software.sav |
| HKEY_LOCAL_MACHINE\System   | HKLM\System   | System, System.alt, System.log, System.sav |
| HKEY_USERS\.DEFAULT         |               | Default, Default.log, Default.sav |
| HKEY_CURRENT_CONFIG         |               | System, System.alt, System.log, System.sav |
| HKEY_CLASSES_ROOT           |               | System, System.alt, System.log, System.sav |

---

Per canviar la **configuració de l'usuari interactiu** s'ha de canviar **HKEY_CURRENT_USER\Software\Classes** però no a HKEY_CLASSES_ROOT.

Per canviar la **configuració per defecte** cal fer-ho a H**KEY_LOCAL_MACHINE\Software\Classes**.

---

## Tipus de valors guardats al registre

El registre tindrà claus compostes per un nom i un valor contingut a la clau. Cada clau pot guardar valors de diferents tipus per exemple:

| Tipus        | Explicació        |
|--------------|:------------------  
| REG_BINARY   | Dades binàries en cru. |
| REG_DWORD    | Número de 4 bytes. |
| REG_SZ       | String de tamany fix. |
| REG_MULTI_SZ | Múltiples strings. |
| etc. | |

Exemple:

![](imgs/registre-windows-c131c903.png)

Consultar tipus a: https://docs.microsoft.com/en-US/troubleshoot/windows-server/performance/windows-registry-advanced-users

---

## Referències

- The Defender's Guide to the Windows Registry (2022) - https://github.com/Defenders-Guide/TheDefendersGuide/blob/main/Windows%20Registry/The_Defenders_Guide_to_the_Windows_Registry.md

- Sobre el registre (Microsoft) - https://docs.microsoft.com/es-es/windows/win32/sysinfo/about-the-registry?redirectedfrom=MSDN

- https://docs.microsoft.com/en-us/windows/win32/sysinfo/registry-hives

- https://docs.microsoft.com/en-US/troubleshoot/windows-server/performance/windows-registry-advanced-users

- https://www.andreafortuna.org/2017/10/18/windows-registry-in-forensic-analysis/
