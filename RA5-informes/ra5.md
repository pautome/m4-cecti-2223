5. Documenta anàlisis forenses elaborant informes que incloguin la normativa aplicable.

- Criteris d'avaluació:

  1. Defineix l'objectiu de l'informe pericial i la seva justificació.
  2. Defineix l'àmbit d'aplicació de l'informe pericial.
  3. Documenta els antecedents.
  4. Recopila les normes legals i reglaments complerts en l'anàlisi forense realitzat.
  5. Recull els requisits establerts pel client.
  6. Inclou les conclusions i la seva justificació.

**Continguts**

5. Documentació i elaboració d'informes d'anàlisis forenses. Apartats dels que es compon l'informe:
  1. Full d'identificació (títol, raó social, nom i cognoms, signatura).
  2. Índex de la memòria.
  3. Objecte (objectiu de l'informe pericial i la seva justificació).
  4. Abast (àmbit d'aplicació de l'informe pericial - resum executiu per a una supervisió ràpida de el contingut i resultats).
  5. Antecedents (aspectes necessaris per a la comprensió de les alternatives estudiades i les conclusions finals).
  6. Normes i referències (documents i normes legals i reglaments esmentats en els diferents apartats).
  7. Definicions i abreviatures (definicions, abreviatures i expressions tècniques que s'han utilitzat al llarg de l'informe).
  8. Requisits (bases i dades de partida establerts pel client, la legislació, reglamentació i normativa aplicables).
  9. Anàlisi de solucions - resum de conclusions de l'informe pericial (alternatives estudiades, quins camins s'han seguit per arribar-hi, avantatges i inconvenients de cadascuna i quina és la solució finalment triada i la seva justificació).
  10. Annexos.
