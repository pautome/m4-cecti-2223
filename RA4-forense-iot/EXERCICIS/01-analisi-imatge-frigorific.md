# Repte: Analitzar la imatge de memòria d'un frigorífic intel·ligent SAMSUNG

## Temps
5 hores ?

## Material i programari

- 2020-09-17 SWGDE Technical Notes on Internet of Things Devices_v1.0 - https://drive.google.com/file/d/1zcDxCLSrwTbwFlTAtjwB6UxMgMHOj3GY/view?usp=sharing
- Manuals, simulador i fotos del frigorífic - https://www.samsung.com/ca/support/model/RF22M9581SG/AC/
- Exemple carving manual - https://forensicallyfit.wordpress.com/2018/08/21/manual-file-carving/
- File Carving incibe - https://www.incibe-cert.es/blog/file-carving
- Explicació i Exemples de carving - https://resources.infosecinstitute.com/topic/file-carving/
- Imatge de memòria del frigorífic (8 GB) - https://drive.google.com/drive/folders/1Kk9a4-VJI2MDuJF3lNIhIX1W2jhzDolb?usp=share_link
- Altres imatges IoT - https://www.vtolabs.com/iot-forensics
- Blog Forense IoT - https://blog.digital-forensics.it/2020/12/a-journey-into-iot-forensics-episode-1.html

---

## Ajuda

### Carving

Aquest procés és utilitzat principalment en la informàtica forense per a extreure informació a partir d'una quantitat de dades en brut sense necessitat de conèixer el sistema de fitxers amb el qual s'han creat els fitxers.

En general, tots els tipus de fitxers tenen característiques comunes. Per exemple, atenent la seva estructura, tots els fitxers JPG/*JFIF comencen per FF D8 FF E0 i acaben per FF D9. Tots els tipus de fitxers tenen una estructura similar. Utilitzen una constant coneguda com Magic Number, la qual permet identificar el corresponent tipus de fitxer. http://www.garykessler.net/library/file_sigs.html

Existeixen diferents tècniques de «file carving»:
- Basades en la capçalera d'un fitxer i en el final o, si es desconeix aquest, en la grandària màxima d'arxiu (dada disponible en la capçalera).
- Basades en l'estructura d'un fitxer: capçalera, peu, cadenes significatives, grandària, etc.
- Basades en el contingut del fitxer: entropia, reconeixement del llenguatge, atributs estàtics, etc. Exemple: HTML, XML, etc.


### Eines de Carving

Llista:
- Bulk Extractor
- Foremost
- Scalpel
- Forensic Toolkit (FTK) AccessData
- Encase
- PhotoRec
- Revit
- TestDisk
- Magic Rescue
- F-Engrave

---

## Objectiu

Analitzar una imatge obtinguda d'un frigorífic intel·ligent. Teniu els hashes a la carpeta compartida.
- Imatge de memòria del frigorífic (8 GB) - https://drive.google.com/drive/folders/1Kk9a4-VJI2MDuJF3lNIhIX1W2jhzDolb?usp=share_link

---

## Repte

L'adquisició de la flash del frigorífic ja s'ha fet i ara cal fer l'anàlisi i trobar tota la informació disponible del dispositiu:

- Quin és el maquinari del dispositiu i quina informació del programari i sistema operatiu es pot trobar.
- Quina informació es pot trobar sobre comptes d'usuari.
- Quina és la configuració de xarxa del dispositiu.
- Com està configurat el frigorífic.
- Llistat d'aplicacions instal·lades al dispositiu.
- Dades de les Apps que podem trobar, per exemple:
  - Històric de navegació.
  - Fotos de la càmera del refrigerador i informació associada, si n'hi ha.
  - Geolocalització amb Glympse.
- Fitxers multimèdia continguts a la memòria.
- Informació d'ús i de despesa d'energia.

1. Utilitzeu alguna eina per a carregar la imatge. Analitzeu el sistema d'arxius i expliqueu l'organització del sistema d'arxius.
2. Utilitzeu, si ho necessiteu, alguna eina de "carving" per a extreure els fitxers interessants. Al menys, proveu les 3 primeres de la llista i comproveu com fa l'extracció dels arxius.
3. Fer una anàlisi de la imatge seguint la metodologia forense i realitzeu simultàniament un **informe** de les troballes realitzades. Utilitzeu el model del RA5 als apunts.
