4. Realitza anàlisi forense en dispositius de l’IOT, aplicant metodologies establertes, actualitzades i reconegudes.

- Criteris d'avaluació:
  1. Identifica els dispositius a analitzar garantint la preservació de les evidències.
  2. Utilitza mecanismes i eines adequades per a l'adquisició i extracció d'evidències.
  3. Garanteix l'autenticitat, completesa, fiabilitat i legalitat de les evidències extretes.
  4. Realitza anàlisis d'evidències de manera manual i mitjançant eines.
  5. Documenta el procés de manera metòdica i detallada.
  6. Considera la línia temporal de les evidències.
  7. Manté la cadena de custòdia.
  8. Elabora un informe de conclusions a nivell tècnic i executiu.
  9. Presenta i exposa les conclusions de l'anàlisi forense realitzat.

  **Continguts**

  4. Realització d'anàlisis forenses a IOT:
      1. Identificar els dispositius a analitzar.
      2. Adquirir i extreure les evidències.
      3. Analitzar les evidències de manera manual i automàtica.
      4. Documentar el procés realitzat.
      5. Establir la línia temporal.
      6. Mantenir la cadena de custòdia.
      7. Elaborar les conclusions.
      8. Presentar i exposar les conclusions.
