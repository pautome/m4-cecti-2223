# Seguretat a IoT

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Seguretat a IoT](#seguretat-a-iot)
	- [OWASP (Open Web Application Security Project)](#owasp-open-web-application-security-project)
	- [Projecte IoTGoat](#projecte-iotgoat)
	- [Llista de vulnerabilitats IoT](#llista-de-vulnerabilitats-iot)
	- [OWASP IoT Top ten (2018)](#owasp-iot-top-ten-2018)
		- [1. Contrasenyes febles, visibles o codificades](#1-contrasenyes-febles-visibles-o-codificades)
		- [2. Serveis de xarxa no segurs](#2-serveis-de-xarxa-no-segurs)
		- [3. Interfícies d'ecosistema IoT insegures](#3-interfcies-decosistema-iot-insegures)
		- [4. Manca de mecanismes d'actualització segurs](#4-manca-de-mecanismes-dactualitzaci-segurs)
		- [5. Ús de components insegurs o obsolets](#5-s-de-components-insegurs-o-obsolets)
		- [6. Protecció de privacitat insuficient](#6-protecci-de-privacitat-insuficient)
		- [7. Transferència de dades i emmagatzematge insegurs](#7-transferncia-de-dades-i-emmagatzematge-insegurs)
		- [8. Manca de sistemes de gestió de dispositius](#8-manca-de-sistemes-de-gesti-de-dispositius)
		- [9. Configuració predeterminada insegura](#9-configuraci-predeterminada-insegura)
		- [10. Manca de reforçament (hardening) físic](#10-manca-de-reforament-hardening-fsic)
	- [Exemples de Protocols de comunicació IoT](#exemples-de-protocols-de-comunicaci-iot)
		- [MQTT](#mqtt)
		- [CoAP](#coap)
	- [Referències](#referncies)

<!-- /TOC -->
---

## OWASP (Open Web Application Security Project)

OWASP és una fundació sense ànim de lucre que treballa per millorar la seguretat del programari a la web (https://owasp.org/).

Porta a terme centenars de projectes i compta amb:
- Eines i recursos
- Una gran comunitat
- Sistemes d'educació i entrenament

Alguns dels projectes més destacats són:
- **Top 10 Web Application Security Risks**: Documentació sobre els 10 atacs més habituals a aplicacions web - https://owasp.org/www-project-top-ten/
- També disponible la versió per a IoT (**OWASP IoT Top ten**)
- **Cheat Sheets**: "Xuletes" de referència ràpida sobre seguretat - https://owasp.org/www-project-cheat-sheets/
- **Zed Attack Proxy**: Eina molt semblant a BURP Proxy per analitzar i modificar peticions i respostes web - https://owasp.org/www-project-zap/
https://wiki.owasp.org/index.php/OWASP_Internet_of_Things_Project#IoT_Top_10

---

## Projecte IoTGoat

IoT Goat és un **microprogramari insegur basat en OpenWrt**. L'objectiu del projecte és **ensenyar als usuaris les vulnerabilitats més comunes** que es troben normalment en dispositius IoT.

Les vulnerabilitats es basen en les 10 vulnerabilitats principals documentades per OWASP: https://www.owasp.org/index.php/OWASP.Internet.of.Things.Projecte.

Github del projecte: https://github.com/scriptingxss/IoTGoat

---

## Llista de vulnerabilitats IoT

- https://wiki.owasp.org/index.php/OWASP_Internet_of_Things_Project#tab=IoT_Vulnerabilities

Aquesta taula conté una llista de vulnerabilitats de dispositius IoT que poden servir també per a dirigir l'extracció i l'anàlisi forense.

Vulnerabilitat |	Superfície d'atac | Explicació
-- |------|---
1. Enumeració d'usuaris | - Interfície d'Administració <p> - Interfície Web del dispositiu <p> - Interfície cloud <p> - Aplicació Mòbil | - Capacitat d'obtenir una col·lecció de noms d'usuari vàlids interactuant amb el mecanisme d'autenticació
2. Passwords dèbils | - Interfície d'Administració <p> - Interfície Web del dispositiu <p> - Interfície cloud <p> - Aplicació Mòbil | - Capacitat de posar passwords com ara '1234' or '123456'. <p> - Ús de passwords per defecte pre-programats
3. Bloqueig de compte | - Interfície d'Administració <p> - Interfície Web del dispositiu <p> - Interfície cloud <p> - Aplicació Mòbil | - Capacitat de continuar fent intents d'autenticació després de 3 o 5 intents fallits.
4. Serveis no encriptats | - Serveis de xarxa del dispositiu | - Els serveis de xarxa no són encriptats o no es fa correctament per prevenir "eavesdropping" (escoltar d'amagat) o el "tampering" (la manipulació) per part d'atacants.
5. Authenticació de dos factors | - Interfície d'Administració <p> - Interfície web cloud <p> - Aplicació Mòbil | - Manca de mecanismes d'autenticació com ara un token de seguretat o un scanner d'empremptes dactilars.
6. Encriptació pobrement implementada | - Serveis de xarxa del dispositiu | - Existeix encriptació però configurada malament o bé no actualitzada, per exemple, usant SSL v2
7. Actualitzacions enviades sense encriptació | - Mecanisme d'actualització | - Les actualitzacions s'envien sense usar TLS i/o sense xifrar el propi arxiu  
8. Destinació de l'actualització que es pot escriure | - Mecanisme d'actualització | L'espai d'emmagatzematge on es guarda l'actualització es pot escriure per tothom permetent que el firmware es pugui modificar i distribuir a tots els usuaris
9. Denegació de servei | - Serveis de xarxa del dispositiu | - El servei pot ser atacat de manera que es denega el servei o fins i tot usar el dispositiu
10. Extracció del dispositiu d'emmagatzematge | - Interfícies del dispositiu físic | - Es pot extreure físicament el mitjà d'emmagatzematge del dispositiu
11. No existeix un mecanisme manual d'actualització | - Mecanisme d'actualització | No es pot forçar una comprovació manual d'actualització
12. No existeix un mecanisme manual d'actualització | - Mecanisme d'actualització | - No es pot actualitzar el dispositiu
13. Versió de Firmware o data d'actuañització | - Firmware del dispositiu | La versió actual del firmware no es pot visualitzar i/o tampoc la data de la darrera actualització.
14. Firmware and storage extraction |	- Interfície JTAG / SWD <p> - extracció In-Situ <p> - Interceptar una actualització OTA (Over The Air o sense fils) <p> - Descarregar des de la web del fabricant <p> - Connectar al eMMC en placa (tapping) <p> - Dessoldar el chip Flash/eMMC SPI i llegir-ho amb un adaptador/gravador | - El Firmware conté molta informació útil com ara el codi font i els binaris dels serveis en execució, passwords predefinits, claus ssh, etc.
15. Manipular el fluxe d'execució del dispositiu  | - Interfície JTAG / SWD <p> - Atacs de canal lateral com ara el "glitching" | - Amb l'ajuda d'un adaptador JTAG i el depurador de codi "gdb" es pot modificar l'execució del firmware del dispositiu i saltar-se gairebé tots els controls de seguretat basats en programari. <p> - Amb els atacs de canal lateral es pot també modificar el flux d'execució o es pot usa per a filtrar informació interessant del dispositiu
16. Obtenir accés de consola | - Interfícies Sèrie (SPI / UART) | - Connectant una interfície sèrie, podem obtenir accés complet a la consola del dispositiu. Les mesures de seguretat habituals consisteixen en "bootloaders" personalitzats per evitar que l'atacant pugui accedir al mode monousuari, tot i que això pot ser també evitat.
17. Components insegurs de tercers | - Programari | - Versions obsoletes de busybox, openssl, ssh, servidors web, etc.

---

## OWASP IoT Top ten (2018)

Es tracta de la llista de vulnerabilitat IoT ordenades per importància d'incidència.

### 1. Contrasenyes febles, visibles o codificades

Es tracta de:
- l'ús de credencials febles:
  - fàcilment deduïbles per força bruta.
  - disponibles públicament, com les contrasenyes per defecte
  - que no es poden canviar, el que es coneix com "hardcoded" o guardades directament al firmware.
- existència de "portes del darrera" (backdoors) en el microprogramari (firmware)
- programari de client que concedeix accés no autoritzat al sistema.

### 2. Serveis de xarxa no segurs

Serveis de xarxa innecessaris o insegurs que s'executen en el propi dispositiu, especialment en els exposats a Internet (que es poden cercar amb buscadors com ara Shodan).

Això permet:
- comprometre la confidencialitat, integritat/autenticitat, o disponibilitat de la informació
- un control remot no autoritzat, etc.

### 3. Interfícies d'ecosistema IoT insegures

Webs insegures, API backends, el núvol o interfícies mòbils de l'ecosistema IoT que es troben fora del dispositiu i que permeten comprometre el dispositiu o els seus components.

Els problemes comuns són:
- la manca d'autenticació/autorització.
- falta d'encriptació o encriptació feble.
- una manca de filtratge dels paràmetres d'entrada i sortida, que podrien permetre atacs d'injecció per exemple.

### 4. Manca de mecanismes d'actualització segurs

Això inclou:
- la manca de validació del microprogramari del dispositiu, per exemple mitjançant firmes.
- la manca de sistemes d'enviament d'actualització segurs, amb protocols que no xifren el trànsit.
- la manca de mecanismes "anti-rollback", o sigui, instal·lar versions anteriors a l'actual que possiblement tinguin alguna vulnerabilitat que ara està corregida.
- la manca de notificacions de canvis de seguretat a causa de les actualitzacions instal·lades.

### 5. Ús de components insegurs o obsolets

Ús de components i/o llibreries de programari obsolets o insegurs que podrien permetre que el dispositiu sigui compromès.

Això inclou:
- la personalització insegura de les plataformes del sistema operatiu.
- l'ús de components de programari o maquinari de tercers d'una cadena de subministrament compromesa.

### 6. Protecció de privacitat insuficient

Es tracta de poder fer servir de manera insegura, indeguda, o sense permís la informació personal de l'usuari que s'emmagatzema en el dispositiu o en l'ecosistema IoT.

### 7. Transferència de dades i emmagatzematge insegurs

Manca de xifratge o control d'accés de dades sensibles en qualsevol lloc de l'ecosistema. Fins i tot en repòs, en trànsit o durant el processament.

### 8. Manca de sistemes de gestió de dispositius

Manca de suport de seguretat en els dispositius desplegats en producció (conjunt de dispositius que treballen en equip, per exemple).

S'inclou:
- la gestió dels actius.
- la gestió d'actualitzacions.
- el desmantellament segur.
- el seguiment de sistemes i capacitat de resposta.

### 9. Configuració predeterminada insegura

Els dispositius o sistemes surten al mercat amb una configuració predeterminada:
- o bé és insegura
- o bé no te la capacitat de fer el sistema més segur restringint les configuracions que poden modificar els operadors del dispositiu (usuaris).

### 10. Manca de reforçament (hardening) físic

Manca de mesures d'enduriment físic. Això permet als possibles atacants accedir a informació sensible que pot ajudar:
- en un futur atac remot
- a prendre el control local del dispositiu.

---

## Exemples de Protocols de comunicació IoT

Existeixen molts protocols de comunicació IoT. A continuació hi ha 2 exemples.

### MQTT

https://coap.technology/

El MQTT (Message Queueing Telemetry Transport) és un estàndard OASI i ISO. Es basa en un model client/server amb un mecanisme de publicació / suscripció de missatges.

Bàsicament facilita la comunicació entre un gran nombre de dispositius i els gestiona mitjançant "brokers".
- Els clients publiquen els missatges cap a un broker i/o es subscriuen a un broker per rebre els seus missatges.
- Els missatges estan organitzats per tipus, que en essència són “etiquetes” que actuen com un sistema per enviar missatges als subscriptors.

![](imgs/02-seguetat-iot-4be7c5bf.png)

https://www.pickdata.net/es/noticias/mqtt-vs-coap-mejor-protocolo-iot

---

Com altres protocols IoT, és lleuger i senzill d'usar i implementar. Funciona bé per entorns limitats i automatitzats com M2M (Machine-to-machine) i IoT.

És un protocol de capa d'aplicació que s'executa sobre TCP i pot córrer sobre qualsevol altre protocol de xarxa que proporcioni un protocol fiable, ordenat i amb comunicació bidireccional (per exemple, WebSockets).

MQTT ha guanyat molta popularitat per la seva simplicitat, arquitectura flexible i per estar preparat per a desplegament al núvol en entorns:
- ecosistemes IIoT (IoT Industrial)
- sistemes de control industrial (ICS)
- Enterprise IoT, etc.

---

### CoAP

https://coap.technology/

El CoAP (Constrained Application Protocol) és un protocol apropiat per dispositius petits amb CPU, memòria i alimentació limitats. Les seves millors característiques són la seva simplicitat i la portabilitat amb HTTP (és com un germà pobre del protocol HTTP per a dispositius limitats).

Amb CoAP, un node client pot enviar una comanda a un altre node via un paquet CoAP.
- Fa servir el protocol UDP i per tant necessita ACK's pels missatges en que es necessiti confiabilitat.
- El servidor CoAP l'interpretarà, extraurà la informació del paquet i decidirà quina acció realitzarà depenent de la seva lògica.
- El servidor no necessàriament realitzarà la confirmació de la petició.

![](imgs/02-seguetat-iot-5a8f03eb.png)

https://www.pickdata.net/es/noticias/mqtt-vs-coap-mejor-protocolo-iot

---

## Referències

- RFC 7228 Terminology for Constrained-Node Networks - https://datatracker.ietf.org/doc/html/rfc7228

- MQTT vs CoAP - https://www.pickdata.net/es/noticias/mqtt-vs-coap-mejor-protocolo-iot
