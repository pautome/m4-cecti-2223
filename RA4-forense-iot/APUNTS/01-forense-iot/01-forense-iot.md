# Forense digital a dispositius IoT

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Forense digital a dispositius IoT](#forense-digital-a-dispositius-iot)
	- [Què és IOT?](#qu-s-iot)
	- [Característiques de IoT](#caracterstiques-de-iot)
		- [1. inteligència](#1-inteligncia)
		- [2. Connectivitat](#2-connectivitat)
		- [3. Natura dinàmica](#3-natura-dinmica)
		- [4. Escala enorme](#4-escala-enorme)
		- [5. Sensors](#5-sensors)
		- [6. Heterogeneïtat](#6-heterogenetat)
	- [Arquitectura IoT](#arquitectura-iot)
		- [1. Sensors i actuadors](#1-sensors-i-actuadors)
		- [2. Sistemes d'adquisició de dades](#2-sistemes-dadquisici-de-dades)
			- [Gateways d'accés a xarxa i Internet](#gateways-daccs-a-xarxa-i-internet)
			- [Sistemes d'adquisició de dades (gateway IoT)](#sistemes-dadquisici-de-dades-gateway-iot)
		- [3. Edge Analytics](#3-edge-analytics)
		- [4. Data Center/ Cloud (Analysis, management, and storage of data)](#4-data-center-cloud-analysis-management-and-storage-of-data)
		- [Dispositius mòbils i aplicacions](#dispositius-mbils-i-aplicacions)
	- [Sistemes operatius per IoT](#sistemes-operatius-per-iot)
		- [Reptes tecnològics de IoT](#reptes-tecnolgics-de-iot)
			- [1. Seguretat](#1-seguretat)
			- [2. Connectivitat](#2-connectivitat)
			- [3. Compatibilitat i Longevitat](#3-compatibilitat-i-longevitat)
			- [4. Stàndards](#4-stndards)
			- [5. Anàlisi i accions intel·ligents](#5-anlisi-i-accions-intelligents)
		- [Mesures de seguretat IoT](#mesures-de-seguretat-iot)
			- [Mesures genèriques](#mesures-genriques)
		- [Mesures de seguretat de xarxa](#mesures-de-seguretat-de-xarxa)
		- [Mesures físiques](#mesures-fsiques)
	- [Reptes del Forense IoT](#reptes-del-forense-iot)
		- [1. Distribució de les dades](#1-distribuci-de-les-dades)
		- [2. Temps de vida de les dades](#2-temps-de-vida-de-les-dades)
		- [3. Anonimització de les dades](#3-anonimitzaci-de-les-dades)
		- [4. Manca d'actualització dels dispositius](#4-manca-dactualitzaci-dels-dispositius)
		- [5. Varietat de dispositius IoT](#5-varietat-de-dispositius-iot)
		- [6. Transformació de les dades](#6-transformaci-de-les-dades)
	- [Les evidències electròniques en dispositius IoT i el sistema legal](#les-evidncies-electrniques-en-dispositius-iot-i-el-sistema-legal)
	- [Extracció d'evidències digitals de dispositius IoT](#extracci-devidncies-digitals-de-dispositius-iot)
		- [1. Obtenir una imatge d'una memòria Flash.](#1-obtenir-una-imatge-duna-memria-flash)
		- [2. Obtenir un bolcat de dades amb dd i netcat.](#2-obtenir-un-bolcat-de-dades-amb-dd-i-netcat)
		- [3. Usar JTAG per obtenir informació del firmware.](#3-usar-jtag-per-obtenir-informaci-del-firmware)
		- [4. Usar UART per obtenir informació del firmware](#4-usar-uart-per-obtenir-informaci-del-firmware)
	- [Analisi de les dades](#analisi-de-les-dades)
	- [Possibles eines](#possibles-eines)
	- [Referències](#referncies)

<!-- /TOC -->
---

## Què és IOT?

Les sigles fan referència a "**Internet of Things**". Però no existeix una definició única des que Kevin Ashton (treballant a Procter & Gamble) al 1999 va crear el terme IoT.

En general, es pot entendre IoT com la xarxa de dispositius (coses) que són capaços de comunicar-se i compartir dades. Interactuen amb altres dispositius i objectes vius (persones, animals, plantes) fent servir **sensors i actuadors** per recol·lectar i intercanviar dades via Internet o una xarxa local privada o alguna xarxa global no connectada a Internet.

<img src="imgs/forense-iot-1b395a75.png" width="400" align="center" />

Esquema bàsic IoT

---

Una nevera pot servir com a exemple de dispositiu IoT si millorem les seves característiques afegint capacitats de comunicar-se amb altres dispositius i persones (la família), compartint dades referents a diferents ítems, com ara temperatura i altres dades útils com dates futures de caducitat via missatges de text a un mòbil. Si es produeix un crim, els components de la nevera IoT podrien donar pistes per solucionar el cas.   

---

## Característiques de IoT

### 1. intel·ligència

Combina algorismes, ordinadors, programari i maquinari que permet que respongui a situacions concretes de forma intel·ligent.  

### 2. Connectivitat

Permet comunicació entre dispositius que contribueixen a la intel·ligència col·lectiva de la xarxa IoT.

### 3. Natura dinàmica

IoT recull dades del seu entorn que canvien contínuament i fan que l'estat dels dispositius canviï dinàmicament. Per exemple:
- dormint i tornant a despertar
- connectats i desconnectats
- llegint valors físics com temperatura, localització i velocitat. etc.

També el nombre de dispositius canvia contínuament, augmentant indefinidament en l'actualitat.

### 4. Escala enorme

El nombre de dispositius IoT serà molt més que el de l'actual Internet.
- Les dades generades també són enormes.
- Tot això implica un gran esforç per gestionar-ho.

### 5. Sensors

IoT necessita els sensors per a interaccionar amb l'entorn, i això genera un flux de dades molt gran.  

### 6. Heterogeneïtat

IoT es basa en diferent maquinari i plataformes de xarxa.
- es requereix una alta escalabilitat i interoperativitat.

---

El fet de que existeixin tants tipus de dispositius IoT es converteix en un arma de doble fil:
- Es poden **recollir evidències de milers d'aparells**, que permet obtenir informació sobre activitats il·legals (lloc, hora, comportament, imatges, veu, condicions de salut, etc.).
- La gran quantitat de dispositius diferents i d'informació a recollir fa **molt més difícil la investigació**.

---

## Arquitectura IoT

En aquests diagrames es pot veure una representació amb elements reals de IoT.

<img src="imgs/forense-iot-5fe139b1.png" width="500" align="center" />

https://www.researchgate.net/publication/335608547_Internet_of_Things_Technology_based_on_LoRaWAN_Revolution

---

<img src="imgs/forense-iot-89934080.png" width="600" align="center" />


https://www.researchgate.net/figure/A-generic-Internet-of-Things-IoT-network-architecture_fig1_282853869

---

L'arquitectura del sistema IoT es sol descriure com un procés de **quatre etapes**:
- Les dades flueixen des dels **sensors** connectats a les "**coses**" a través d'una **xarxa**.
- Finalment, arriben a un centre de dades corporatiu o al **núvol** per al seu processament, anàlisi i emmagatzematge.

<img src="imgs/forense-iot-28100fd8.png" width="600" align="center" />

https://www.tutorialandexample.com/iot-architecture

---

En la "Internet de les coses", una "**cosa**" pot ser una màquina, un edifici o fins i tot una persona.

Els processos de l'arquitectura de la IoT també envien dades en l'altra direcció en forma d'instruccions o comandes que indiquen a un **actuador** o a un altre dispositiu connectat físicament que realitzi alguna acció per a controlar un procés físic.

Un **actuador** pot fer una cosa tan senzilla com encendre un llum o tan important com detenir una línia de muntatge si es detecta una fallada imminent.

---

### 1. Sensors i actuadors

Components per obtenir i enviar dades per part del sistema. Segons el NIST:

**Sensor:** porció d'un dispositiu IoT que proveeix d'una observació d'un aspecte del mon físic en la forma de mesura de dades.
- Examples de sensors són temperatura, moviment, giroscopi, proximitat, i sensors de pressió.

**Actuador:** Dispositiu per a moure o controlar un mecanisme o sistema. Operat amb una font d'energia, normalment elèctrica, pressió hidràulica d'un fluid o pressió neumàtica. L'actuador converteix aquesta energia en moviment.
- Exemples d'actuadors inclouen **motors** (servo, pas a pas o "stepper", motors de corrent continu), **vàlvules de control**, etc.

La porta d'un garatge te sensors fotoelèctrics que usen llum visible o infraroja per detectar objectes que passin entre el transmissor i el receptor de llum. El propòsit dels sensors és evitar que la porta es tanqui o obri a sobre d'algú.

En una investigació forense és molt important **assegurar-se de la integritat de les dades de sensors i actuadors** i buscar possibles sensors falsos a la xarxa que podrien portar a l'entorn IoT a decisions incorrectes.

---

### 2. Sistemes d'adquisició de dades

Aquest nivell implica el **treball a prop dels sensors i actuadors**, així com la **connexió en xarxa**.

En aquest nivell és molt important el **processament de grans quantitats de dades** recollides en el nivell anterior i reduir-les a una mida òptima per a l'anàlisi posterior.

També es fa la **conversió de les dades** en cru a sistemes més organitzats.

<img src="imgs/forense-iot-c7189bbe.png" width="400" align="center" />

https://www.esparkinfo.com/blog/guide-iot-architecture.html

---

Estan implicats aquí:

#### Gateways d'accés a xarxa i Internet

Normalment routers via Wi-Fi o sense fils i/o xarxes LAN cablejades. També realitzen processament addicional.

Aquests dispositius es poden analitzar per obtenir també evidències forenses (**forense de xarxa**).

#### Sistemes d'adquisició de dades (gateway IoT)

Poden gestionar múltiples dispositius IoT (per exemple la porta del garatge i els sensors de llum i/o càmeres) i **obtenen dades dels sensors via xarxa i generen sortides pels actuadors**.

És possible que el dispositiu IoT sigui gestionat directament sense gateway IoT, però la manca de seguretat dels dispositius IoT poden forçar a la creació de gateways per millorar la seguretat.

Cal tenir en compte en el **forense dels gateways IoT** per a obtenir evidències forenses.

---

### 3. Edge Analytics

En alguns casos, de forma opcional, existeix una infraestructura de xarxa (física o virtual) entre els components IoT i el núvol. S'utilitza per a:
- **Realitzar anàlisis i preprocessament**. Per exemple, coses com el machine learning i sistemes de visualització.
- També es fa **processament addicional** per reduir la latència abans de passar al següent nivell.

<img src="imgs/forense-iot-b2faeea4.png" width="500" align="center" />

https://www.esparkinfo.com/blog/guide-iot-architecture.html

---

Es coneix també com a **“fog computing”**. Els investigadors forenses han de tenir-la present també

---

### 4. Data Center/ Cloud (Analisi, gestió i emmagatzemament de dades)

Les dades arribaran a un data center o al núvol.
- Es fa un **processament en profunditat**.
- Cal habilitats d'anàlisi en el mon digital i humà.
- Es **combinen dades d'altres orígens** per a fer aquest anàlisi en profunditat

El núvol proporciona ubiqüitat i recursos de computació configurables. És el lloc perfecte per a guardar dades de milers de dispositius IoT. Per exemple, una cafetera IoT, de manera que es pot programar, encendre-la i apagar-la des de qualsevol lloc del mon.

Hi ha una sèrie de reptes a enfrontar que ja hem vist quan hem parlat de **forense al núvol**.

---

### Dispositius mòbils i aplicacions

Gairebé tots els dispositius IoT són controlats i gestionats utilitzant un mòbil i una aplicació de mòbil, tauleta o ordinador, en lloc de tenir un controlador hardware propi.

D'aquesta part **es pot fer l'anàlisi forense seguint les metodologies a dispositius i aplicacions mòbils**.

El problema és que els dispositius IoT van canviant constantment així com les tecnologies mòbils i les xarxes. Cal estar en formació contínuament.

---

## Sistemes operatius per IoT

L'ús de sistemes operatius per maquinari IoT, dels que tenim propietaris i opensource, normalment es classifica en dos grups:
- **dispositius finals**, amb molta menys capacitat.
- **gateways**, amb molta més capacitat i que carreguen més processos.

<img src="imgs/forense-iot-db3679f6.png" width="500" align="center" />

https://devopedia.org/iot-operating-systems

---

El sistema operatiu IoT és una evolució dels sistemes operatius "embebuts" i tenen una sèrie de restriccions degudes a les limitacions del maquinari.

Existeixen dispositius que treballen directament sense sistema operatiu (baremetal), però l'augment de complexitat (més sensors, més processament de dades, més connectivitat) cada cop fan més necessaris els sistemes operatius.

---

### Reptes tecnològics de IoT

Existeixen una bona quantitat de reptes degut a la natura de l'ecosistema IoT. Els components IoT usen **protocols i tecnologies molt variats**. Com a resultat tenen configuracions complexes i un disseny pobre.

Podem diferenciar 5 paràmetres importants en aquests reptes tecnològics.

#### 1. Seguretat

IoT ha causat **molts problemes de seguretat**, ja que cada cop es connecten més dispositius i les aplicacions estan mal dissenyades.
  - Hi pot haver **manipulació de les dades d'usuaris** si la transmissió de dades no és segura, tant entre dispositius amb protocols M2M (machine to machine) com entre dispositiu i gateway IoT.
  - Alguns dispositius **poden afectar la seguretat física de les persones** o la seva salut, com ara dispositius mèdics implantats o cotxes amb vulnerabilitats.

---

#### 2. Connectivitat

Cal connectar múltiples dispositius. Actualment es fa servir un **model client/server centralitzat** per autenticar, aprovar i enllaçar dispositius a la xarxa. Aquest sistema pot ser un coll d'ampolla en el funcionament de IoT.

Cal una gran inversió en el **manteniment de clústers de servidors al cloud**, per a poder gestionar el gran intercanvi d'informació. En cas de caigudes de servidors el servei podria caure.

---

#### 3. Compatibilitat i Longevitat

Existeix una gran incompatibilitat entre els dispositius per culpa de:
- La manca d'estandarització de protocols M2M (Machine to machine).
- La manca d'unificació dels serveis cloud.
- Multitud de firmwares i sistemes operatius.  

A més, moltes tecnologies usades queden obsoletes molt ràpidament davant dels nous estàndards.

---

#### 4. Stàndards

No existeixen estàndards ni documentació sobre bones pràctiques i això pot produir comportaments estúpids dels dispositius per mal disseny.
- **Cal estàndards per guiar els fabricants** ja que els desenvolupadors dissenyen els dispositius sense tenir consciència dels possibles problemes que poden produir.

---

#### 5. Anàlisi i accions intel·ligents

El nivell final de la implementació IoT és extreure informació analitzant les dades i cal tecnologies cognitives i models associats.

---

### Mesures de seguretat IoT

Cal aplicar una sèrie de mesures de seguretat en la utilització de dispositius IoT (que després van en contra de l'anàlisi forense, sobretot les mesures físiques).

#### Mesures genèriques

- Utilitzar **contrasenyes robustes**.
- **Canviar la contrasenya estàndard** i fins i tot el nom d'usuari durant la instal·lació del producte IoT.
- Assegurar que els **comptes d’ usuaris no poden ser esbrinats** utilitzant funcionalitats com, per exemple, la de recuperació de contrasenyes.
- Assegurar que es **bloqueja temporalment l'accés** a un compte després de diverses fallades d'accés.
- Assegurar que les credencials (nom d'usuaris, contrasenyes, tokens de accés, galetes, etc.) no estan exposades a Internet mentre transiten a través d'ella. Cal utilitzar sempre **connexions xifrades** amb autenticació TLS.
- Implementar, si és possible, l'**autenticació mitjançant la verificació de dos o més factors**.
- Detectar i bloquejar peticions o intents anòmals d'aconseguir entrar al sistema/dispositiu.
- Mantenir el **programari i firmware actualitzat**.
- **Deshabilitar les característiques** i funcionalitats que no volem utilitzar.

### Mesures de seguretat de xarxa

- **Canviar els ports TCP** de connexió per defecte del dispositiu.
- Es recomanable utilitzar **dispositius intermedis** entre el dispositiu IoT i Internet, per exemple un router configurant-lo amb les mesures de seguretat i control adequades.
- Usar **Antivirus, tallafocs, analitzadors de malware**, etc…
- Si no utilitzem la connectivitat de xarxa del nostre dispositiu, apagar-la. Si la utilitzem, o si és necessària per al funcionament del dispositiu.
- Verificar que el tauler d'administració no és accessible des d'internet.
- Aplicar una **segmentació de xarxa sòlida** per als dispositius IoT connectats. Ens hem de preguntar: ¿El dispositiu s’ha de connectar a internet? ¿Cal que accedeixi a la mateixa xarxa a la qual es connecten els nostres altres dispositius corporatius?
- **Deshabilitar o protegir l'accés remot** als nostres dispositius IoT mentre no sigui necessari. L'accés remot és la funcionalitat que permet controlar-los a distància.
- Investigar i **aprofitar les mesures de seguretat** que ofereix el nostre dispositiu IoT en concret.

### Mesures físiques

- Assegurar que els **mitjans d'emmagatzematge no es poden extreure fàcilment**.
- Assegurar que les **dades emmagatzemades estan xifrades** en repòs.
- Assegurar que els **ports USB i altres ports externs no poden ser utilitzats per a un accés nociu** al dispositiu.
- Assegurar que el **dispositiu no pot ser desmuntat** fàcilment.
- Assegurar que només es permeten aquells ports externs, com ara els USB, que són realment necessaris per al correcte funcionament del dispositiu.
- Assegurar que el producte té l'habilitat de limitar les capacitats administratives.

---

## Reptes del Forense IoT

### 1. Distribució de les dades

Moltes dades IoT estan distribuïdes entre diferents àrees no gestionades per l'usuari.
  - Núvol
  - PC's de sobretaula o portàtils
  - telèfons mòbils

Llavors es fa **molt difícil decomissar les evidències i recollir-les**. A més, la informació pot residir en diferents països i ser de diferents tipus (personals o no) que impliquen **lleis diferents**.

---

### 2. Temps de vida de les dades

Les dades tenen **temps de vida curt** per que les dades poden ser sobreescrites degut a la limitació de la capacitat dels dispositius IoT, i per tant es poden perdre evidències.

Normalment els dispositius envien les dades a la xarxa local o el núvol abans d'esborrar-les.

---

### 3. Anonimització de les dades

Molts **registres estan anonimitzats** per que el proveïdor no permet la gestió de la informació exacta i per tant és difícil reconèixer el criminal.

---

### 4. Manca d'actualització dels dispositius

Quan es creen productes nous, es deixa de donar suport a les actualitzacions dels dispositius i als frameworks associats.

---

### 5. Varietat de dispositius IoT

Els mateixos dispositius IoT són un problema.
- Pot passar que la bateria estigui molt baixa, sobretot si el dispositiu és petit, en un lloc tancat o té l'apariència d'un dispositiu tradicional (per exemple un interruptor).
- És difícil examinar-lo internament i a vegades transportar-lo al laboratori.
- Existeixen molts sistemes operatius i sistemes de gestió de dades. Molts són sistemes propietaris dels que no hi ha informació pública.

---

### 6. Transformació de les dades

Les dades del dispositiu físic i les guardades al núvol poden no coincidir.
- Preprocessament, resum, etc.

En ocasions caldria retornar a l'estat original de les dades per a ser admesos a un judici.

---

### 7. Integritat i cadena de custòdia

Com en el cas dels dispositius mòbils i en el núvol, es fa difícil mantenir-ho correctament.

---

## Les evidències electròniques en dispositius IoT i el sistema legal

**Exemple: El cas de Mr. Richard G. Dabate (The State of Connecticut, United States of America)**

Els dispositius "wearable" (que es poden "vestir" com una peça de roba) com ara els sensors de fitness, recullen informació sobre l'estat de salut d'una persona (activitat física, ritme cardíac, pols, temperatura, etc.). El “Fitbit” és un d'aquests dispositius que va servir com una evidència en aquest cas.

El 23 de Desembre de 2015, la policia va trobar morta la senyora de Richard G. Dabate a la seva residència. El senyor Dabate va declarar que un intrús havia entrat a casa seva i la va matar.

El 4 d'Abril de 2017, es va arrestar al senyor Dabate per l'assassinat de la seva dona. Entre les evidencies admeses al judici es troba el Fitbit que la policia va trobar al cos de la senyora Dabate en el moment dels fets. El Fitbit, i altres evidencies recollides de computadors, telèfons mòbils, post de xarxes socials, i del sistema d'alarma de la casa, van permetre a la policia crear una línia de temps que contradeia les declaracions del senyor Dabate.

El cas va quedar pendent per la pandèmia de COVID-19.

https://es.gizmodo.com/resuelven-un-caso-de-asesinato-gracias-a-la-pulsera-fit-1849440079

https://www.eldebate.com/tecnologia/20220826/condenado-65-anos-carcel-gracias-pulsera-actividad-mujer_55841.html

---

## Extracció d'evidències digitals de dispositius IoT

Degut a la gran varietat de dispositius IoT (models, sistemes, estructures de documents, programari i maquinari tancat) **no existeix una aproximació estàndard pel procès forense.**

Però **es poden aplicar tècniques** com les següents:

- Obtenir una **imatge d'una memòria Flash**.
- Obtenir un **bolcat de dades amb dd i netcat**.
- Usar **procediments JTAG i UART** per obtenir informació del firmware.

També es pot fer servir els **protocols de xarxa com ara Telnet, SSH, Ethernet i Wi-Fi** per accedir als dispositius.

---

### 1. Obtenir una imatge d'una memòria Flash.

Es pot obtenir una **imatge de l'emmagatzemament intern** si el dispositiu pot ser enllaçat a un PC fent servir eines com ara:
- FTK Imager
- X-ways Legal Science
- Winshex

Exemples de memòries internes serien chips NAND/NOR Flash i targetes SD / CF / MMC.

---

### 2. Obtenir un bolcat de dades amb dd i netcat.

Si podem arrencar una versió de Linux al dispositiu i tenim una xarxa funcional, podem extreure una imatge del disc o memòria del dispositiu amb l'eina **dd combinada amb netcat**.

---

### 3. Usar JTAG per obtenir informació del firmware.

JTAG és port de connexió estàndard del IEEE (1149.1) originalment dissenyat per a testejar les plaques electròniques dels dispositius. Usat com a eina de forense, **JTAG permet obtenir informació del firmware**.

S'ha de fer servir amb compte per que pot fer malbé el dispositiu.

---

### 4. Usar UART per obtenir informació del firmware

UART es un receptor/transmissor universal asíncron serie, que **permet connectar via cable serie** a un dispositiu que tingui port sèrie.

https://www.analog.com/en/analog-dialogue/articles/uart-a-hardware-communication-protocol.html

---

## Analisi de les dades

Un cop extreta la informació caldrà fer l'anàlisi. Els sistemes de fitxers arrel i de client contenen normalment:
- **Logs** de les operacions fetes.
- La **configuració dels usuaris**
- La **configuració del sistema**, moltes vegades Linux.

## Possibles eines a fer servir

1. Digital Forensic Framework
2. Open Computer Forensic Architecture
3. CAINE
4. SANS Investigative Forensic Toolkit – SIFT
5. EnCASE
6. WindowsSCOPE
7. Sleuthkit
8. Oxygen Forensic Suite

---

## Referències

- **Model IoT**
  - A Comprehensive Guide On IoT Architecture (com són en la realitat els diferents nivells) - https://www.esparkinfo.com/blog/guide-iot-architecture.html
  - **Els 4 nivells de IoT** - https://es.digi.com/blog/post/the-4-stages-of-iot-architecture
  - **IoT tutorial** - Explicació i **projectes pràctics d'implementació IoT** amb Arduino - https://www.tutorialandexample.com/iot-tutorial
  - **Devopedia sistemes operatius IoT** - https://devopedia.org/iot-operating-systems
	- Publicacions IoT i tecnològiques - http://kth.diva-portal.org/

---

- **Aplicacions i frameworks**
	- 40 Top Internet-of-Things (IoT) Applications & Examples You Should Know - https://www.esparkinfo.com/blog/iot-applications-examples.html
	- Top 25 Open Source IoT Frameworks You Need To Know - https://www.esparkinfo.com/blog/open-source-iot-frameworks.html

---

- **Seguretat IoT**
  - **Expliot - HANDS ON INTERNET OF THINGS HACKING - Manual IoT molt complet** - Conceptes, protocols, vulnerabilitats, maquinari, ràdio, firmware - https://store.expliot.io/products/hands-on-internet-of-things-hacking
  - **OWASP Internet of Things Project** - IoT Attack Surface Areas Project - https://wiki.owasp.org/index.php/OWASP_Internet_of_Things_Project#tab=IoT_Attack_Surface_Areas
	- **IoT penetration testing cookbook** - https://github.com/PacktPublishing/IoT-Penetration-Testing-Cookbook
	- **Shodan i IoT**- https://resources.infosecinstitute.com/topic/shodan-iot-problem/#gref
- **INFOSEC IoT Security articles** - https://resources.infosecinstitute.com/topics/iot-security/

---

- **Forense IoT**
  - **Metodologia d'extracció** - https://www.ijitee.org/wp-content/uploads/papers/v9i4/C8087019320.pdf
	- **SWD vs JTAG** - https://embeddedinventor.com/swd-vs-jtag-differences-explained/
	- JTAG - ¿Qué es JTAG y cómo puedo usarlo? - https://www.xjtag.com/es/what-is-jtag/
	- **EMBEDDED INVENTOR** - Introducció al maquinari embebut - https://embeddedinventor.com/
	- Què és JTAG - https://www.xjtag.com/es/what-is-jtag/
  - Ports per obtenir info - https://embeddedinventor.com/swd-vs-jtag-differences-explained/#The_SWD_Standard
