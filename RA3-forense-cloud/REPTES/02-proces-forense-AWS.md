# Repte: Procés forense amb AWS (adquisició)

## Temps
3 hores ?

## Material i programari

- Curs amb recursos AWS - https://awsacademy.instructure.com/courses/9662
- Login general - https://www.awsacademy.com/LMS_Login
- Vídeo demostratiu - https://www.youtube.com/watch?v=3oto8Bl2vaE&t=608s
- Tutorial LAMP Server (Amazon Linux) - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html
- Manual de EC2 - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html?icmpid=docs_ec2_console

- AWS Cloud Log Extraction (10-2-2023) -  https://www.sans.org/blog/aws-cloud-log-extraction/

- Cloud forensics: https://blog.tofile.dev/2022/08/22/cloud-forensics.html

---

## Ajuda

**ATENCIÓ: No canvieu la regió geogràfica de les instàncies d'Amazon. La versió gratuïta no ho permet. Tampoc modifiqueu les regions dels objectes que creeu, ja que no es veuran entre si**.

- Adquisición de evidencias forenses en la nube (Yolanda Olmedo, CSIRT-CV) - https://www.youtube.com/watch?v=juXD6yx3kwo

---

## Objectiu

Realitzar les passes del procés forense en un cas simulat:
- Preparació de l'entorn
- Identificació i adquisició d'evidències

Familiaritzar-se amb les mesures de seguretat disponibles a AWS IaaS.

- **Cloud Trail**: Segueix les operacions d'accés a l'API d'AWS (usuaris, opreacions, etc.)
- **Cloud Watch**: Mètriques de rendiment (CPU, RAM. Emmagatzematge) i a més facturació per detectar despeses com ara minar criptomonedes en les nostres instàncies de forma i·legal.
- **Logs VPC**: Xarxes privades d'AWS.

---

## Repte

### 1. Preparar l'entorn

#### Forensic Readiness

1. Crear una estació forense per preparar les eines (Windows o Linux).
2. Determinar quines evidències forenses recollirem de l'equip compromès, atenent a quin sistema operatiu té i quins són els artefactes que contenen informació rellevant.
3. Fer una selecció de les eines d'adquisició i triatge que podem fer servir (extracció d'evidències volàtils i persistents).
4. Crear un volum nou, afegir-ho a l'equip forense i ficar-hi les eines forenses d'extracció en calent, triatge, etc. que hem seleccionat.
5. Desconnectar el volum.

#### Aïllar el sistema

1. Crear una instància de Windows que simularà l'equip compromès.
2. Connectar remotament via RDP a l'equip. Canviar el fons de pantalla per indicar que està compromès.
3. Crear un "snapshot" del volum del Windows compromès, per evitar canvis posteriors.
4. Aïllar la xarxa de la màquina compromesa, permetent només accedir-hi via la IP pròpia.

### 2. Identificació i adquisició d'evidències

**NOTA: Mantenir en tot moment la integritat (hash abans i després).**

Seguir l'ordre de volatilitat per adquirir les evidències.

---

**Ordre de volatilitat**: Es tracta de l'ordre en que farem l'adquisició d'evidències. Cal respectar sempre aquest ordre en l'adquisició per evitar que desapareguin les evidències o les contaminem involuntàriament.

1. Registres i memòria cau ("caché")
2. Taula de rutes i memòria cau ARP, memòria cau DNS, connexions de xarxa, taula de processos, estadístiques del Kernel (nucli)
3. Memòria RAM de la màquina
4. Informació temporal del sistema
5. Disc físic
6. Logs del sistema
7. Configuració física i topologia de la xarxa
8. Documents

(Revisar apunts RA1 )

En el núvol:

1. Memòria RAM.
2. Dades de triatge.
3. Metadades del client en el proveïdor de serveis al núvol.
4. Disc dur del sistema.

---

Per a fer-ho, connectar el volum amb les eines forenses prèviament creat a l'equip compromès.

1. Feu l'adquisició de RAM.

2. Feu el triatge de l'equip compromès.    

3. Per a l'adquisició del disc, a partir de l'snapshot anterior, crear un nou volum sencer "copia-disc-compromes". Ara ja tenim una còpia del disc sense haver de clonar. El podrem clonar o analitzar directament al núvol.

4. Ara afegir el volum "copia-disc-compromès" a l'estació forense per a fer la clonació o anàlisi si cal.

5. Cal crear un volum "proves-forenses" per a guardar les dades forenses.

6. Al sistema operatiu de l'estació forense muntar en mode només lectura l'"snapshot" "copia-disc-compromes" i clonar al volum de destí amb una eina adient.

---

## Preguntes de revisió

1. Fes un petit informe resum de les mesures forenses que s'hauria d'aplicar en els equips desplegats al núvol, prèviament a una possible incidència.
2. Crea un document on es detalla el procés pas a pas per la investigació forense.
3. Indica les hores reals dedicades al repte.
