# Repte: Crear imatge de memòria en AWS

## Temps
3 hores ?

## Material i programari

- Formació gratuïta AWS Universitat de Granada (2023) - https://incubadoradetalento.es/aws/

- Curs amb recursos AWS - https://awsacademy.instructure.com/courses/9662
- Login general - https://www.awsacademy.com/LMS_Login
- Vídeo demostratiu - https://www.youtube.com/watch?v=3oto8Bl2vaE&t=608s
- Tutorial LAMP Server (Amazon Linux) - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html
- Manual de EC2 - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html?icmpid=docs_ec2_console

---

## Ajuda

Gestió de paquets
~~~
yum check-update
yum update
// Per un grup de paquets
yum update "Development Tools"
yum update httpd
yum install httpd
yum update httpd
sudo yum install mc
~~~

Arrencar servei, comprovar, aturar
~~~
sudo systemctl start httpd
sudo systemctl status httpd
sudo systemctl stop httpd
// inici automàtic amb sistema o no
sudo systemctl enable httpd
sudo systemctl disable httpd
~~~

---

## Objectiu

Familiaritzar-se amb AWS IaaS, i preparar una infraestructura forense. Cal crear dues instàncies de màquines virtuals EC2 Linux per a practicar una adquisició de memòria amb LiME al núvol.

Amazon Web Services (AWS) ofereix serveis AWS PaaS i AWS IaaS.

---

## Repte

- Abans de començar heu de validar el vostre compte a AWS educatiu.
- Heu de tenir un correu amb l'enllaç per accedir al sistema (comproveu spam).
- Disposeu de 100$ per a fer proves amb el cloud d'Amazon.
- Si no voleu gastar el "saldo", recordeu aturar les instàncies que no feu servir (tot i que en principi el laboratori les atura quan feu STOP).

**ATENCIÓ: No canvieu la regió geogràfica de les instàncies d'Amazon. La versió gratuïta no ho permet.**

1. Anar al menú "módulos"

2. Picar Learner Lab - Foundational Services

3. Llegir el Readme per saber com funciona el laboratori (és genèric per a poder crear instàncies "lliures").

**ATENCIÓ: No piqueu mai el botó RESET per que eliminaria les instàncies. Les sessions de treball duren 4 hores i no s'esborra res entre sessions mentre piquem botó STOP.**  

4. Crear dues instàncies ("victima-elteunom" i "forense-elteunom")

- Picar Start Lab i apareixerà la consola de gestió de AWS CLI (al centre), per gestionar les instàncies i etc. via comandes o Python en lloc de l'entorn web.

- Després anar a enllaç AWS a l'esquerra a dalt (ara en verd). Cal permetre la finestra emergent.

- Crear una màquina virtual EC2 (Amazon Elastic Compute Cloud) amb "Launch a virtual machine". Només apliqueu les "Free Tier Elegible" (si voleu fer proves amb d'altres, ho podeu fer).
  - Application and OS Images (Amazon Machine Image): Amazon Linux 2
  - Instance type Info: Per defecte (t2 micro). Quan més gran, més cost.
  - Key pair (login): Necessitarem el parell de claus per connectar. interactivament via SSH i per moure arxius amb SCP. Cal crear un parell de claus nou. https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html
  - Network settings: Permetre SSH, HTTP, HTTPS.
  - Configure storage: Per defecte, EBS (Elastic block store).
  - Advanced details: Per defecte.

5. Ara posar en marxa les instàncies i connectar via SSH des de l'equip local.
- Mirar les propietats de la instància i buscar els seu nom DNS públic de l'estil "ec2-3-89-110-237.compute-1.amazonaws.com".
- Obrir una consola i connectar via ssh. Mirar com es fa al botó "Connect" a dalt a la dreta.

6. Mirar d'instal·lar un servidor web a la "víctima". Provar que funciona accedint al servidor.
- picar "Public IPv4 DNS" i canviar https per http
- picar "Public IPv4 address" i canviar https per http

7. Configurar Filezilla a l'equip local per moure arxius als/dels equips virtuals al núvol (víctima i forense).

![](imgs/01-AWS-ab440ea8.png)

8. Preparar la instància d'equip forense per a obtenir la imatge forense:
- instal·lar utilitats nc, LiME, git, capçaleres del kernel (reviseu la pràctica de LiME).
- obrir el port 4444 ("instances", "Network & Security") per poder rebre la imatge via netcat.
- crear el mòdul del kernel i enviar via ssh a la víctima.
- obtenir la imatge de RAM enviant remotament a l'equip forense amb la IP privada, NO la pública, en cas que es pugui. Així tot ho fem dintre la xarxa d'AWS i serà més ràpid sense consumir ample de banda d'internet. De fet, fer-ho per IP pública seria una bogeria!
- comprovar hash de la imatge en rebre-la.

9. Descarregar la imatge obtinguda per l'estació forense a la màquina local. Comprovar hash.

10. Comprova que la imatge es pot usar amb Volatility obtenint la llista de processos actius.

---

## Preguntes de revisió

1. Quin tipus de sistema operatiu tenim a les instàncies EC2?
2. Quina versió del kernel hi ha?
3. Quins noms DNS i IP pública tenen les instàncies creades?
4. Si atureu el laboratori i el torneu a engegar, les adreces IP públiques i privades es mantenen?
5. Es pot utilitzar l'adreça privada per a connectar entre l'estació forense i la víctima?
6. Fes un petit informe resum de les mesures forenses que s'hauria d'aplicar en els equips desplegats al núvol, prèviament a una possible incidència.

7. Indica les hores reals dedicades al repte.
