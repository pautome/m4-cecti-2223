# Forense digital al núvol

**INS Carles Vallbona**

**Pau Tomé**

---

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Forense digital al núvol](#forense-digital-al-nvol)
	- [Que és "Cloud Forensics" i com s'utiltitza?](#que-s-cloud-forensics-i-com-sutiltitza)
	- [Tipus de serveis Cloud](#tipus-de-serveis-cloud)
		- [SaaS](#saas)
		- [PaaS](#paas)
		- [IaaS](#iaas)
	- [Tipus de Cloud](#tipus-de-cloud)
		- [Cloud Públic](#cloud-pblic)
		- [Cloud Privat](#cloud-privat)
		- [Cloud Híbrid](#cloud-hbrid)
	- [Supervisió del servei Cloud](#supervisi-del-servei-cloud)
	- [Reptes de l'anàlisi fornese en Cloud](#reptes-de-lanlisi-fornese-en-cloud)
		- [Reptes legals](#reptes-legals)
		- [Reptes organitzatius](#reptes-organitzatius)
		- [Reptes tècnics](#reptes-tcnics)
	- [Estratègies d'anàlisi forense en cloud](#estratgies-danlisi-forense-en-cloud)
		- [Requeriments per l'anàlisi forense](#requeriments-per-lanlisi-forense)
		- [Fases del procés forense en cloud](#fases-del-procs-forense-en-cloud)
			- [1. Preparació de l'entorn](#1-preparaci-de-lentorn)
				- [A: Aspectes regulatoris](#a-aspectes-regulatoris)
				- [B: Aspectes organitzacionals](#b-aspectes-organitzacionals)
				- [C: Aspectes tècnics](#c-aspectes-tcnics)
			- [2. Identificació i adquisició d'evidències](#2-identificaci-i-adquisici-devidncies)
				- [Extracció d'evidències:](#extracci-devidncies)
				- [Carregar evidències al sistema local](#carregar-evidncies-al-sistema-local)
			- [3. Preservació d'evidències](#3-preservaci-devidncies)
			- [4. Anàlisi i creació de l'informe](#4-anlisi-i-creaci-de-linforme)
				- [4.1. Inspecció inicial / Triatge](#41-inspecci-inicial-triatge)
				- [4.2. Anàlisi forense preliminar](#42-anlisi-forense-preliminar)
				- [4.3. Anàlisi forense en profunditat](#43-anlisi-forense-en-profunditat)
			- [5. Presentació de l'informe (Report)](#5-presentaci-de-linforme-report)
	- [Referències](#referncies)

<!-- /TOC -->

---

Amb les tecnologies cloud les empreses poden hostatjar el seu programari i aplicacions en servidors externs i econòmics estalviant temps, diners i evitant haver de gestionar maquinari dedicat. També es pot emmagatzemar grans quantitats de dades de forma segura.

Però quan s'implementa cal tenir present les estratègies per a **poder realitzar forense al cloud en cas d'un incident**.

## Que és "Cloud Forensics"?

Fa referència a les **investigacions relatives a incidents que impliquen el núvol**, incloent **filtració de dades i identificació de robatoris**.
- Si s'ha **implementat prèviament mesures forenses**, es tindrà una protecció addicional i es podran preservar millor les evidències.
- En cas contrari, pot ser que no es tingui dret d'accés a les dades o evidències necessàries, sobretot si està hostatjat en un proveïdor extern.

Aquest tipus de forense pot ser **més complicat** per que les dades guardades externament poden estar afectades per una **jurisdicció diferent segons la ubicació física dels servidors**.

Cal comentar que el forense en cloud és **molt dependent del tipus de Cloud que s'estigui utilitzant.**

---

## Tipus de serveis Cloud

Es pot escollir entre els serveis Cloud:
- **SaaS (Software as a Service)**: **distribueix programari** als usuaris mitjançant internet i **evita que s'hagin de preocupar de la gestió i el manteniment**.
- **PaaS (Platform as a Service)**: proporciona un **entorn on els desenvolupadors poden construir i desplegar aplicacions**.
- **IaaS (Infrastructure as a Service)**: ofereix els recursos de computació per **allotjar, construir i operar serveis**.

<img src="imgs/1-forense-cloud-1cce344b.png" width="500" align="center" />

https://www.stackscale.com/es/blog/modelos-de-servicio-cloud/

---

### SaaS

En la plataforma **SaaS (Software as a Service)**, el programari i les dades estan al cloud, i s'hi pot accedir des de qualsevol lloc amb connexió a Internet.

El proveïdor desenvolupa, manté, actualitza i hostatja l'aplicació i per tant és responsable de gestionar el contingut de programari i dades.
- Els usuaris només han d'iniciar sessió i, sense haver d'instal·lar-hi res, només el fan servir.
- El problema és que **no es té el control** i això pot ser un problema per a determinats negocis.

És el servei cloud més comú i molt usat per empreses per que:
- és **fàcil** de desplegar, usar, gestionar i escalar.
- facilita la col·laboració entre equips.

Alguns **exemples de SaaS** són:
- Google Workspace
- Dropbox
- Salesforce.

---

### PaaS

**PaaS (Platform as a Service)** proporciona un entorn de desenvolupament ja preparat pel desenvolupament de manera que els desenvolupadors poden centrar-se en escriure i executar codi per a crear aplicacions personalitzades. Es fa servir la web per a construir les aplicacions.

S'**integra serveis, motors de bases de dades, etc. per fer desenvolupament**, testeig i desplegament d'apps.
- El **client és parcialment responsable de les dades i les aplicacions** però no de l'emmagatzematge, xarxa, servidors ni sistema operatiu.
- Els clients PaaS **només tenen control sobre el que construeixen** però no sobre el sistema operatiu o el maquinari subjacent.
- Per tant **no tenen control sobre el rendiment** que pugui tenir sobre el programari.
- Si que controlen altres aspectes com anti-malware o el control d'accés.

Alguns **exemples de PaaS**:
- Heroku
- Apache Stratos
- OpenShift.

---

### IaaS

**IaaS (Infrastructure as a Service)** és una plataforma que hostatja la infraestructura de maquinari. El proveïdor ofereix la xarxa, l'emmagatzematge, la virtualització. No cal invertir en un CPD d'alt nivell per obtenir els mateixos avantatges.

El **client és responsable de la integritat de les dades, programari, middleware (programari intermediari), aplicacions i sistema operatiu**. Accedeixen mitjançant un panell de control o API sense haver de fer-ho físicament.

Permeten **molta flexibilitat**:
- Es pot comprar recursos de computació **sota demanda**, en lloc d'haver de comprar el propi maquinari.
- Milloren l'**eficiència**
- Milloren l'**escalabilitat** (es pot créixer o decréixer segons necessitats).
- Milloren la **redundància i seguretat**.
- Es manté el **control sobre la infraestructura**.
- Deleguen la instal·lació, gestió i manteniment.
- Poden fer estalviar diners, temps i esforços.

Els **clients han d'assumir la responsabilitat de**:
- gestió d'Accés
- xifrat
- protecció del tràfic de xarxa, etc.

La IaaS s'utiltiza per exemple per a:
- desplegar aplicacions web
- operar un CRM (Customer Relationship Management)
- realitzar anàlisi de Big Data
- emmagatzemar dades
- backups i plans de Disaster Recovery, etc.


Alguns exemples de IaaS:
- Azure
- Stackscale
- AWS
- VMware.

---

<img src="imgs/1-forense-cloud-29f49c20.png" width="600" align="center" />

Comparativa de models Cloud

---

<img src="imgs/1-forense-cloud-7444855c.png" width="600" align="center" />

Símil amb diferents formes de menjar una pizza

---

## Tipus de Cloud

A més dels tipus de serveis, també podem diferenciar diferents **tipus de Cloud**.

### Cloud Públic

Cloud **totalment extern i accessible via Internet**. És compartit entre diferents empreses.

Es té **poc control sobre la seguretat de les dades**, i no és recomanable per empreses que gestionen dades sensibles o amb restriccions sobre la seva gestió.

---

### Cloud Privat

L'**empresa instal·la la seva infraestructura Cloud** i és responsable de tota la gestió de maquinari, dades, xarxa, etc.

Normalment més car però **millor per empreses amb dades sensibles** o restringides.

És el **millor tipus per a anàlisi forense** ja que es té accés a totes les dades i infraestructura. Es pot aplicar la metodologia tradicional en forense.

---

### Cloud Híbrid

En aquest model les **dades sensibles es guarden en el Cloud privat i la resta en el cloud públic**.

Favorable per empreses que no volen invertir en la seva pròpia infraestructura però volen mantenir la privacitat de les dades i la propietat de les dades.

Permet estalviar diners i mantenir les dades sensibles protegides. Aquest model també dificulta la labor forense.

---

Basat en l'estudi "Cloud Audit and Forensics" de Cloud Security Alliance España e ISMS Forum Spain - https://www.ismsforum.es/ficheros/descargas/cloudauditforensics2018v41544463021.pdf

## Supervisió del servei Cloud

ÉS difícil establir un nivell adequat de supervisió ja que les tècniques i estratègies utilitzades normalment es tornen ineficaces o incompletes en l'entorn Cloud.

Factors a tenir en compte
- **Connectivitat**: Es tracta de tenir accés fiable a Internet i altres xarxes. Riscos: disponibilitat, velocitat d'accés.
- **Serveis i gestió de xarxa**: Administració i supervisió així com  balanceig de càrrega. Riscos: disponibilitat, transmissions segures, nivell d'accés.
- **Serveis i gestió de la computació**: Gestionar el nucli, processadors, memòria i administració del sistema operatiu. Risc: Disponibilitat.
- **Emmagatzematge de dades**: Gestió de l'espai en disc. Riscos: seguretat de les dades, recuperació, disponibilitat.
- **Seguretat**: Que inclou tant física com lògica.

---

## Reptes de l'anàlisi forense en Cloud

Donat que les tecnologies al núvol tenen unes característiques intrínseques com ara
- **elasticitat**: S'adapten a les necessitats del client.
- **ubiqüitat**: Accessibles des de qualsevol lloc.
- **abstracció**: segons el nivell de cloud, no es pot veure res de la infraestructura.
- **volatilitat**: Les dades es reciclen molt ràpidament.
- **compartició de recursos**: Es pot usar recursos de diferents servidors.

cal enfrontar un sèrie de reptes:

<img src="imgs/01-forense-cloud-95abda5d.png" width="600" align="center" />

---

### 1. Reptes legals

- Possibles **canvis de jurisdicció** segons on s'ubica el proveïdor o el CPD.

- **Manca de col·laboració del CSP** (Communications Service Provider) o proveïdor, si no està establert per contracte.

- Difícil mantenir la **cadena de custòdia**, ja que hi ha més actors en la adquisició i preservació i per tant més opcions de que les evidències es comprometin.

---

### 2. Reptes organitzatius

- Falta de govern de les dades, ja que al núvol augmenta la **dificultat per a establir la propietat i responsabilitat de les dades**.

- **No existeixen procediments específics per a anàlisi forense en cloud**, quan hauria de basar-se en Estàndards. Això tenint en compte que els riscos són més grans:
  - Deslocalització de les dades
  - Volatitlitat de l'entorn
  - Dificultat per mantenir la cadena de custòdia

- **Falta d'aspectes forenses als contractes i SLA's** (Service Level Agreement), quan la participació del CSP és importantíssima.

- **Col·laboració difícil** entre personal de l'empresa i el proveïdor.

---

### 3. Reptes tècnics

- **Problemes en la identificació de les evidències**: Cal identificar quines evidències cal adquirir, però aquestes estan distribuïdes, en equips virtuals i molt volàtils. És difícil l'extracció física i inclús la lògica, depenent del model i tipus de cloud.

- **Arquitectures en el núvol**: Quan més abstracta i fàcil de fer servir pels usuaris, més difícil és la tasca forense. De més amigable amb el forense a menys tenim: (+) IaaS-PaaS-SaaS (-)

- **Ús de tecnologies serverless**: El client carrega codi al núvol i aquest s'adapta, de manera que és més difícil extreure les dades forenses.

- **Volatilitat de les dades**: Discos virtualitzats implica més facilitat per a recuperar les dades per exemple esborrades.

- **Múltiples formats de logs i plataformes**: Al núvol hi ha moltíssimes plataformes i formats de logs a gestionar.

- **Validació de les evidències adquirides**: és molt complicat extreure evidències sense que en el procés es corrompin, per exemple al capturar imatges de memòria.

- **Eines insuficients**: Les que es fan servir normalment en forense convencional poden no servir.

- **Recuperació de dades esborrades difícil**: Molt complicada al núvol per la volatilitat.

- **Grans quantitats de dades implicades**: El núvol escala i pot arribar a ser molt extens, amb múltiples servidors i moltes dades que fan l'**extracció i anàlisi molt complicats**.

- **Velocitats de transferència lentes:** En ser tantes dades i mitjans de transmissió en xarxa, són lents.

- **Sincronització temporal de fonts d'informació**: Les màquines no són controlades per una sola entitat i fa que la Sincronització pugui no ser correcta cosa que **dificulta reconstruir les seqüències temporals**.

- **Falta de coneixement especialitzat**: pocs tècnics amb coneixements especialitzats en núvol.

---

## Estratègies d'anàlisi forense en cloud

Les fases de l'anàlisi forense cloud continuen sent **les mateixes que a l'anàlisi tradicional**, però adaptades a les característiques de volatilitat, elasticitat, ubiqüitat, abstracció i compartició de recursos.

### Requeriments per l'anàlisi forense

1. **Planificació**: S'ha de definir els mitjans materials i els procediments.

2. **Coneixement de l'entorn**: Cal entendre com es desplega i configura l'entorn i cal tenir formació específica:
  - Xarxa
  - sistema
  - seguretat, etc.

3. **Compliment legal**: Cal afegir a la legislació i la normativa el fet que participen més actors (proveïdors).

4. **Rigorositat**: Cal ser metòdic i sistemàtic, encara més al núvol per les seves característiques.

5. **Eficiència**: Cal adaptar els recursos a la labor ja que pot ser molt més extensa.

---

### Fases del procés forense en cloud

<img src="imgs/1-forense-cloud-94ffc425.png" width="700" align="center" />

#### 1. Preparació de l'entorn

Cal **aïllar el sistema en el possible**, per evitar volatilització. Cal fer servir eines per a extreure la informació inalterada.
- Serà més difícil instal·lar eines.
- Més difícil extreure dades sense alteracions.
- És lent extreure les dades.
- És impossible aïllar totalment ja que l'investigador ha de poder accedir remotament.

**Cloud Forensics Readiness**: Consisteix en haver preparat l'entorn abans, cosa imprescindible. Es tracta d'una activitat proactiva d'anticipació als incidents.

Cal tenir en compte tres dimensions:

---

##### A: Aspectes regulatoris

- **Marc legal** (constitució espamyola, codi penal, llei d'enjudiciament civil, RGPD i LOPD, LSSI).

- **Clàusules contractuals**
	- determinar si es pot recopilar informació, finalitat, etc.
	- Jurisdicció aplicable, segurament serà la del servidor al núvol
	- Obligació de comunicar en cas d'incident.
	- Períodes de retenció normalment de logs (temps que es guarda), per poder fer la investigació.

---

##### B: Aspectes organitzacionals

- Comptar amb el **recolzament de la direcció**, sobretot amb recursos

- **Definir l'estratègia de forense** per preparar l'entorn

- **Definir rols i responsabilitats**, assignant a personal preparat. També les que corresponguin al proveïdor, per contracte.

- **Procediments d'actuació documentats i detallats**, a nivell procedimental i tècnic.

- **Formació i conscienciació** tant dels investigadors com del personal, per no comprometre la investigació i algú fracassi.

---

##### C: Aspectes tècnics

El sistema ha de generar la informació per poder portar a terme l'anàlisi posterior i aquesta informació ha d'estar protegida. Això depèn molt del servei. Cal desplegar les eines i configurar-les:
- **SaaS o PaaS** donen poca informació que no depengui del proveïdor.
- **IaaS** permet més control i dona molta més informació.

Els **logs i el registre d'esdeveniments, així com els registres d'auditoria** sóm les fonts principals d'informació pel forense.
- Linux, Windows i z/OS (sistema del núvol de IBM) permeten generar i recuperar aquesta informació.

Per saber quins logs i informació cal generar i recol·lectar, cal un enfoc en nivells:

1. **Nivell de Compliment**: Requisits legals o reguladors que:
  - o bé indiquen quins registres cal guardar
  - o bé com s'han de guardar sense determinar exactament quins.

2. **Nivell de Processos del negoci**: Per poder reclamar el possible incompliment dels SLAs pactats caldrà guardar logs que es faran servir com a proves en cas d'incident.

3. **Nivell Operacional**: Qualsevol cosa que impacta en el funcionament.
  - **Incidències** que afecten un component específic, tant errors del sistema o per amenaces.
  - **Parades, inicis i reinicis** de components que poden indicar incidències que cal investigar.
  - **Canvis de configuració** d'algun component que pugui afectar a d'altres per desconeixement de com funcionen els altres, o bé per algun malware que ho hagi provocat.
  - **Informació en memòria**: És una font important d'informació en anàlisi forense.

4. **Nivell de seguretat**: Cal definir **quina informació es guarda en cada capa** i amb quin **nivell de verbositat**, per no saturar el sistema on es guarda per l'alt volum de dades generades. És recomanable usar sistemes SIEM que correlacionen els logs. Tot això implica molta feina prèvia. Es farà en dos àmbits:
  - **Informació generada per eines de seguretat**
    - anti-malware
    - IDS/IDPS
    - firewalls
    - analitzadors de protocols, etc.

    Cal analitzar la informació generada de forma periòdica i s'ha de poder exportar per l'anàlisi forense.

  - **Informació sobre l'activitat dels usuaris** respecte la seguretat. Aquesta informació és bàsicament els registres de
    - identificació
    - autenticació
    - autorització.

---

#### 2. Identificació i adquisició d'evidències

En el cas de **PaaS i SaaS**, caldrà demanar al proveïdor l'extracció d'evidències o al menys col·laboració.

En el cas de **IaaS** el client podrà extreure més evidències considerant:

- **Identificar evidències**: Usuaris, xarxa i infraestructura  

- **Ordre de volatilitat**:
  - RAM
  - Estat del sistema
    - Connexions obertes
    - Llistat de fitxers oberts
    - Processos en execució
  - Memòria persistent, molt més volàtil al núvol per ser equips virtuals.

- **Ordre d'extracció**:
  - RAM
  - Dades de triatge
  - Metadades del client al proveïdor
  - Disc dur del sistema

---

##### Extracció d'evidències:

- **Eines d'extracció remotes**, mitjançant agent desplegat prèviament o connexió remota. Independent del proveïdor i fàcil i ràpid en l'adquisició.
- **Eines que no permeten accés remot**, treballen en local, per obtenir imatge del sistema. Aquest sistema és més costós però permet usar eines forenses "tradicionals" i que copien amb més facilitat per que no depenen d'una connexió directa.  
  - Recomanable **fer snapshots de sistemes virtuals** en entorns IaaS si està disponible, per ser el que **menys alteren la informació**.
  - Crear **discs virtuals secundaris** per bolcar la informació.

---

##### Carregar evidències al sistema local

Cal baixar les evidències a un equip local per l'anàlisi.

- Millora les garanties de **disponibilitat i integritat**.

- Cal afegir les **marques de temps** (timestamps) per poder establir la seqüència temporal.

- És recomanable usar una **font externa de sincronització** pel sistema de logs.

- Cal usar **protocols de xifrat, checksums o hashes** per garantir confidencialitat i integritat.

- Convé usar **compressió** per estalviar ample de banda i augmentar la velocitat de transmissió. Cal fer servir canals diferents a la resta del tràfic dels usuaris.

---

En entorns **PaaS i SaaS** existeixen eines per obtenir la informació directament dels serveis:
- si es tenen les credencials necessàries
- són limitades i dependents del sistema en concret
- cal analitzar prèviament si servirà la informació proporcionada.

---

##### Cadena de custòdia

Cal garantir la integritat si volem que siguin admeses en judicis.
- Mai no sabem si acabarem en judici i per tant **totes les adquisicions i gestions han de ser admissibles a un judici**.

- Per assegurar la cadena de custodia de informació accessible per l'usuari que és volàtil, cal **fer servir contenidors i sistemes de precinte** per garantir que el dispositiu físic es manté íntegre.

- Com que hi ha un proveïdor que no controlem (procés i personal), la cadena de custòdia pot quedar compromesa en aquest punt. Cal intentar **obtenir tota la informació de la capa de client** deixant de banda, si es pot, al proveïdor**.

- A més el fet que la infraestructura estigui compartida posa restriccions legals a la còpia física de discos.

- La **presència d'un notari** en el procés d'adquisició és molt recomanable. També ho és lliurar un document previ al notari explicat molt clarament i amb nivell NO tècnic el procediment que es seguirà.

- Una altra estratègia pot ser tenir **registres duplicats en un equip local** que es pugui protegir adequadament.

- Fer servir sempre **hashing i xifrat** per garantir la cadena de custòdia tot i que participi el proveïdor en algun lloc.

- Cal **mantenir un registre amb totes les passes seguides** durant l'anàlisi i el personal involucrat, igual al que fem en un entorn tradicional

---

#### 3. Preservació d'evidències

Obviament, **continuar mantenir la cadena de custòdia** i només permetre accés a persones autoritzades:

- Si tenim les evidències al núvol, fer servir una interfície segura d'accés pels només investigadors.
- Si les tenim en local, implementar identificació, autenticació i autorització al sistema local.

Cal prendre les següents mesures en el moment de que disposem de la informació en local:
1. **Assegurar-se que el SIEM, és compatible** amb els registres generats, si ho fem servir.

2. **Pre-processar la informació** si el volum de registres d'auditoria és molt alt i/o de diferents fonts:
	  - per contextualitzar la informació
	  - donar-li un format comú
	  - agrupar si és necessari
	  - eliminar informació no rellevant.

3. Asegurar que el sistema d'emmagatzematge s'adapta i es pot ampliar segons les necessitats i que es vigila per no quedar-se sense espai.

4. La **informació ha de ser fàcilment accessible** i recuperable.

5. Poder **arxivar les evidències al llarg del temps**, fent un backup per anàlisis futurs.

6. Assegurar que **només el personal autoritzat pot accedir a les evidències**.

7. **Cal conservar la integritat de totes les evidències**. S'ha d'evitar poder modificar o eliminar la informació quan s'accedeix. També inclús si la informació està al sistema d'origen.

8. Cal **registrar TOTS els accessos a les evidències**, tant logs com imatges de memòria.
- És important saber qui ha accedit per deixar rastre de les consultes.
- important tant si és accés legítim com si no ho és i poder detectar abusos de privilegis.

9. És molt recomanable **guardar les còpies de les evidències originals en una localització diferent**.
- Sempre que es **treballi es farà amb una còpia de l'evidència**, mai amb l'original.
- Evitarem problemes per errors com muntar imatge en mode R/W en lloc de RO.
- També millorem la disponibilitat si no tenim accés remot al servidor que conté els originals.

---

#### 4. Anàlisi i creació de l'informe

Es pot fer amb un enfoc multinivell:

##### 4.1. Inspecció inicial / Triatge

Fase **inicial, superficial i ràpida**. Normalment amb eines automàtiques.
- Categoritzar el tipus d'incident.
- Entendre l'impacte sobre l'organització.
- Decidir el nivell d'aprofundiment en l'anàlisi i quines seran les fonts d'evidències.
- Es fa sobre l'entorn afectat i amb menys informació que si es decideix després aprofundir.

---

##### 4.2. Anàlisi forense preliminar

Obtenir informació per obtenir una **comprensió inicial de l'incident** a investigar.
- Recopilar informació obtinguda en el triatge.
- Buscar les evidències de l'incident, coses sospitoses.
- Si no hi ha prou informació per obtenir indicis, deixar aquí l'anàlisi o continuar investigant per assegurar que és un fals positiu.

---

##### 4.3. Anàlisi forense en profunditat

Si s'ha trobat indicis caldrà un anàlisi en profunditat:
- determinar seqüència temporal
- raons, causants, culpables i conseqüències   

Les eines a fer servir poden ser molt específiques atenent al tipus de cloud.

**Existeixen serveis que permeten centralitzar les evidències** en un sistema d'emmagatzematge centralitzat:
- **FaaS** (Forensics-as-a-Service) o **DFaaS** (Digital-Forensics-as-a-Service)
- ofereixen interfícies de connexió remota per facilitar el treball col·laboratiu.
- Alguns tenen eines forenses instal·lades per poder fer anàlisi.

REFS: https://cisomag.eccouncil.org/a-beginners-guide-to-forensics-as-a-service-faas/

---

#### 5. Presentació de l'informe (Report)

Es presenta l'informe al públic objectiu
- Cal tenir present al proveïdor.
- Cal **evitar conclusions rotundes** si no tenim les evidències prou sòlides.
- Cal **diferenciar els destinataris** de les conclusions a l'informe. Si és el proveïdor, pot ser que no hagi de fer res si no està obligat per contracte.
- En cas d'emprendre accions legals, assegurar-se si hi ha restriccions al contracte que no permetin fer-ho.
- Separar la part que va destinada al proveïdor, per assegurar la confidencialitat de les dades del client.

---

## Referències

- Cloud Audit and Forensics - https://www.ismsforum.es/ficheros/descargas/cloudauditforensics2018v41544463021.pdf

- ENISA - Exploring Cloud Incidents - https://www.enisa.europa.eu/publications/exploring-cloud-incidents/@@download/fullReport

- Guide to Digital Forensics Incident Response in the Cloud - https://www.intezer.com/blog/cloud-security/guide-to-digital-forensics-incident-response-in-the-cloud/

- Cloud Forensics and the Digital Crime Scene - https://www.appdirect.com/blog/cloud-forensics-and-the-digital-crime-scene

- El núvol no existeix, és l'ordinador d'un altre
  - https://medium.com/@storjproject/there-is-no-cloud-it-s-just-someone-else-s-computer-6ecc37cdcfe5
  - https://www.linkedin.com/pulse/cloud-really-just-someone-elses-computer-marcelo-oliveira/

- Forensic investigation environment strategies in the AWS Cloud - https://aws.amazon.com/blogs/security/forensic-investigation-environment-strategies-in-the-aws-cloud/

- ENISA Cloud Service Scheme - https://www.enisa.europa.eu/publications/eucs-cloud-service-scheme

- Exercicis - https://www.enisa.europa.eu/topics/trainings-for-cybersecurity-specialists/online-training-material/operational#incident-handling-in-the-cloud
