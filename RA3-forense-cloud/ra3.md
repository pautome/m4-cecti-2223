3. Realitza anàlisis forenses en Cloud, aplicant metodologies establertes, actualitzades i reconegudes.

- Criteris d'avaluació:
  1. Desenvolupa una estratègia d'anàlisi forense en Cloud, assegurant la disponibilitat dels recursos i capacitats necessaris un cop passat l'incident.
  2. Aconsegueix identificar les causes, l'abast i l'impacte real causat per l'incident.
  3. Realitza les fases de l'anàlisi forense en Cloud.
  4. Identifica les característiques intrínseques del núvol (elasticitat, ubiqüitat, abstracció, volatilitat i compartició de recursos).
  5. Acompleix els requeriments legals en vigor, RGPD (Reglament general de protecció de dades) i directiva NIS (Directiva de la UE sobre seguretat de xarxes i sistemes d'informació) o les que eventualment puguin substituir-les.
  6. Presenta i exposa les conclusions de l'anàlisi forense realitzat.

  **Continguts**

  3. Realització d'anàlisis forenses en Cloud:
      1. Núvol privat i núvol públic o híbrid.
      2. Reptes legals, organitzatius i tècnics particulars d'una anàlisi en Cloud.
      3. Estratègies d'anàlisi forense en Cloud.
      4. Realitzar les fases rellevants de l'anàlisi forense en Cloud.
      5. Utilitzar eines d'anàlisi en Cloud (Cellebrite UFED Cloud Analizer, Cloud Trail, Frost, OWADE, ...).
